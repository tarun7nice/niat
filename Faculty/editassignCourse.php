<?php
ob_start();
include("autoload.php");
include("check_session.php");

$us	=	new	Auth();
$rec= $us->getUserInfo($fuserId);


$obj	=	new Faculty();

$insId = $_GET['insId'];
$couId = $_GET['couId'];

$tmp = $obj->fetchRanks(); //fetching Ranks


$insDet = $obj->fetchInstructorByID($insId);
$couDet = $obj->fetchcourseById($couId);


//inserting Instructor

	if(isset($_POST["addButton"])){
		
		$insId = $_GET['insid'];
		$couId = $_GET['couid'];
		
		if($obj->editassignCourse($_POST,$insId,$couId)){
		$msg = "Inserted Successfully";
		header("Location:listAssigned.php?sucess=$msg");
		exit;
		}else{
		$msg = $auth->getError();	
		header("Location:listAssigned.php?msg=$msg");
		exit;
		}

 
	}

 
 //deleting Instructor 
 
 if($_GET["action"]=="del"){
		$vid		=	$_GET["vid"];
		$tt	=	$obj->deleteInstructor($vid);				
		header("Location:editassignCourse.php?msg=Deleted successfully");
		exit;
      }
	





$msg	=$_GET["msg"];
$msg1	=$_GET["msg1"];
	
$webpageTitle	=	"Edit Assign Subjects to Instructors";
?>




<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php include("top.php"); ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

  
  	
<script type="text/javascript">

function add_sorted (parent_node, new_node) {
   var children = parent_node.getElementsByTagName('OPTION');
   var flag=true;
   
   for (var i = 0; i < children.length; i++) {
	 
       if (new_node.text == children[i].text) {
	      flag=false;
       }
       
   }
   
   if( children.length<1)
   parent_node.appendChild(new_node);
   else
   {
	   if(flag)
	   {
	   		parent_node.appendChild(new_node);
		   	}
	   }   
  
}
function move_right () {
   var left = document.getElementById('left');
   var right = document.getElementById('right');

   var Options = left.getElementsByTagName('OPTION');
   for (var i = 0; i < Options.length; i++) {
       if (Options[i].selected) {
     
       var optionsNew= new Option(Options[i].text, Options[i].value, false, Options[i].isSelected);
      
          // optionsNew.setAttribute("group_id", Options[i].parentNode.id);
           add_sorted(right, optionsNew);
		    left.remove(i);
         //  i = -1;
       }
   }
}

function move_left () {
   var left = document.getElementById('left');
   var right = document.getElementById('right');

   var Options = right.getElementsByTagName('OPTION');
   for (var i = 0; i < Options.length; i++) {
       if (Options[i].selected) {
            var optionsNew= new Option(Options[i].text, Options[i].value, false, Options[i].isSelected);
          // var group_id = Options[i].getAttribute("group_id");
           var move_to = left;
           right.remove(i);
           add_sorted(move_to, optionsNew);
		   // i = -1;
       }
   }
}


function Selectall(ff)
{
	for (var i = 0; i < ff.length; i++) { 
    ff.options[i].selected=true;    	
    }
 }
 

 
 function fetchSubjects(str){
	var instructor = document.getElementById("instructor").value;
	//document.getElementById("dd").style.display="block";
	var xmlhttp;    
	
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  {// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			var data=xmlhttp.responseText;
			
			  //   data=eval(data);
								
		/*	for(var count=0;count<data.length;count++)
				{
					var op=new Option(data[count].name,data[count].id);
					//document.getElementById('left').appendChild(op);//=xmlhttp.responseText;
					
					
				//	$('#left').multiSelect('addOption', { value: data[count].id, text: data[count].name });
					
				}	*/
				
									
								 
	
			 document.getElementById('rr').innerHTML=data;
		}
	//	$('#left').multiSelect();
	  }

	xmlhttp.open("GET","getSubjectEdit.php?courseId="+str+"&instructor="+instructor,true);
	xmlhttp.send();
	
	
	
	}
</script> 




  

		
    </head>
    <body class="skin-blue"  onLoad="startTime()">
	<?php include("head.php"); ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <?php include("sidemenu.php"); ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
			
			<section class="content-header">
			<div id="txt" class="alert alert-info"></div>
			
			 <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li><i class="fa fa-bookmark-o"></i> Masters</li>
                        <li class="active">Edit Assign Subjects</li>
                    </ol>
                    <h1><small>Edit Assign Subjects</small></h1>
                   
                </section>
               <!-- Main content -->
                <section class="content">    
				
				
				          

                  <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-success">
					
							<?php
							if($msg)
							{
							?>    
							<div class="alert alert-success alert-dismissable"> <?php echo $msg; ?><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
							<?php
							}
							?>    
							
							<?php
							if($msg1)
							{
							?>    
							<div class="alert alert-danger alert-dismissable"> <?php echo $msg1; ?><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
							<?php
							}
							?>    
                              
                              
                                <!-- form start -->
                                <form name="myform" role="form" method="post" action="editassignCourse.php?insid=<?php echo $_GET['insId']; ?>&couid=<?php echo $_GET['couId']; ?>" id="myform" onSubmit="Selectall(document.myform.right);">
                                    <div class="col-md-12 box-body">
									
										
										<div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Instructor </label>
											<select id="instructor" name="instructor" class="form-control">
												<option value="">---Select Instrucutor---</option>
												<?php
												$ins = $obj->fetchInstructor();
												$insSize = count($ins);
												$i = 0;
												while($i<$insSize)
												{
												 $rankId = $ins[$i]["instructor_rank"];
                                                 $rankName = $obj->fetchRankById($rankId);
												?>
												<option value="<?php echo $ins[$i]["instructor_id"]; ?>" <?php if($insDet[0]["instructor_id"]==$ins[$i]["instructor_id"]) { ?> selected="selected"<?php } ?>><?php echo $ins[$i]["instructor_name"]; ?>-&nbsp;<?php echo $rankName[0]["ranks"];?>-&nbsp;<?php echo $ins[$i]["personalno"];?></option>
												<?php
												$i++;
												}
												?>
											</select>
                                        </div>
										
										<div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Course </label>
											<select id="course" name="course" class="form-control" onChange="fetchSubjects(this.value)" >
											<option value="">---Select Course---</option>
											<?php
											$course = $obj->fetchMIP();
											$courseSize = count($course);
											$j=0;
											while($j<$courseSize)
											{
											?>
											<option value="<?php echo $course[$j]["course_id"]; ?>" <?php if($couDet[0]["course_id"]==$course[$j]["course_id"]) { ?> selected="selected"<?php } ?>><?php echo $course[$j]["course_name"]; ?></option>
											<?php
											$j++;
											}
											?>
											</select>
										</div>	
										
										<div class="col-md-6 form-group">
										
										
										<div id="rr">
										
										<?php
										$subjects = $obj->fetchSubjectInfoEdit($couId,$insId);
										$size = count($subjects);
										
										
										$assignsubDet = $obj->fetchwtpCouEdit($insId);
										
										
									//	$gh = count($subjects)+1;
										
									//	for($g=0;$g<count($assignsubDet);$g++){
										
										
										
										
								//		$subId = $assignsubDet[$g]["subjectId"];		
								//		$subDet = $obj->fetchSubject($subId);									
									    
										
									//	$subjects[$gh]["sub_id"] = $subId;
									//	$subjects[$gh]["subject_name"] = $subDet[0]["subject_name"];
										
									//	$gh++;
									//	}
										
										
									
										
										?>
										
										
										<div class="multi_left_big">							
										
										<select multiple id='left' name="left[]" class="form-control" size="11">
										
										
										<?php 
										
										for($f=0;$f<count($subjects);$f++) {
										?>
										
										<option value="<?php echo $subjects[$f]["sub_id"];?>"><?php echo $subjects[$f]["subject_name"];?></option>
										
										<?php 
										 }?>
										
										</select>
										
										</div>
										
										
										
									  <div class="form-group">
										<div class="col-sm-offset-2 col-sm-10">
										<input type="button" name="#" id="#" value=">>>>" onClick='move_right();'/>
										<input type="button" name="#" id="#" value="<<<<" onClick='move_left();'/>
										
										</div>
										</div>
										
										<div class="clearfix"></div>
										</div>
										
										</div>
										
										
										<div class="col-md-6 form-group" id="dd">
										<select multiple name="right[]" id="right" class="form-control" size="11" >	
										<?php 
										
										$assignsubDet = $obj->fetchwtpCouEdit($insId);
										
									     for($g=0;$g<count($assignsubDet);$g++){
										
											$subId = $assignsubDet[$g]["subjectId"];		
											$subDet = $obj->fetchSubject($subId);	?>
											
										<option value="<?php echo $assignsubDet[$g]["subjectId"];?>"><?php echo $subDet[0]["subject_name"];?></option>
										<?php }?>							
										</select>
										</div>
                                        
                                      </div><!-- /.box-body -->
									
									
                                    <div class="box-footer">
                                        <button type="submit" name="addButton" class="btn btn-success">Update <i class="fa fa-check"></i></button>
                                    </div>
                                </form>
                            </div><!-- /.box -->
							
							</div>
							
							
							
							</div>
							

                </section><!-- /.content -->
				
				
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        <script src="js/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
		<!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>
		
		
	<!-- <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"> -->

	<!-- <script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>	 -->
	<link rel="stylesheet" href="../jquery-ui-1.11.2.custom/jquery-ui.css">

	<script src="../jquery-ui-1.11.2.custom/jquery-ui.js"></script>


	   <!-- <script type="text/javascript">
        CKEDITOR.replace(txtaddress);
        </script> -->
	
		
			<script type="text/javascript">
            $(function() {
                $('#example1').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>
		
		
		<script type="text/javascript">
		
		function askDelete(){
			if(confirm("Do you want to delete this item ? click OK to continue, CANCEL to exit")){
				return true;
			}else{
				return false;
			}
		}

		
		</script>
		
		
		  
		
<script>
function startTime() {
    var today=new Date();
    var h=today.getHours();
    var m=today.getMinutes();
    var s=today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt').innerHTML = h+":"+m+":"+s;
    var t = setTimeout(function(){startTime()},500);
}

function checkTime(i) {
    if (i<10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}
</script>	


<link href="css/multi-select.css" media="screen" rel="stylesheet" type="text/css">
	

  <script src="js/jquery.multi-select.js" type="text/javascript"></script>
  
  
    <script type="text/javascript">
  
 // $('#left').multiSelect();
  
  </script>	
        
    </body>
</html>