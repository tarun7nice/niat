<?php
ob_start();
include("autoload.php");
include("check_session.php");
$us	=	new	Auth();
$rec= $us->getUserInfo($fuserId);


$name = $_GET["name"];


$obj	=	new Faculty();


$tmp = $obj->fetchCourseDetailsForSubStatusById($name); //fetching Course details



$courseId = $tmp[0]["course_id"];


$subjectList = $obj->fetchSubListFOrCourse($courseId);

$size = count($subjectList);


if(count($subjectList)>0)
{


?>


 <table id="example1" class="table table-bordered table-hover">
										<thead>
                                            <tr>
                                                <th>Sl No.</th>
                                                <th>Subject</th>
												<th>Total Sessions</th>
												<th>Sessions Taken</th>
												<th>Balance Sessions</th>
												<th>Progress</th>
												<!--<th>Stream</th>-->
												
                                            </tr>
                                        </thead>
                                        <tbody>
										   <?php 
							                $i	=	0;	
// 							                $colorArray = array("#F7464A","#FF5A5E","#46BFBD","#5AD3D1");
							                $labelArray = array();
							                $valArray = array();
							                while($i < $size) {	
											
											$wtpSessTaken = $obj->fetchwtpSessTaken($subjectList[$i]["sub_id"]);
											
											$nodays =$obj->fetchwtpSessTakenDays($subjectList[$i]["sub_id"]);
											
											
											
											$completedSess =  ($wtpSessTaken[0]["noofsess"]*2);
											
											$labelArray[$i] = $subjectList[$i]["subject_name"];
											$valArray[$i] = round($completedSess/$subjectList[$i]["sessions"] * 100);
							                ?>						    
                                            <tr>
                                                <td><?php echo $i+'1';?></td>
                                                <td><?php echo $subjectList[$i]["subject_name"];?> </td>												
												<td><?php echo $subjectList[$i]["sessions"];?></td>
												
												
												<td><?php echo $completedSess;?></td>
												
												
												<td><?php echo $subjectList[$i]["sessions"]-$completedSess;?></td>
												<td><div class="progressbar" data-value="<?php echo round($completedSess/$subjectList[$i]["sessions"] * 100);?>">
												<div class="progress-label">
												<?php echo round($completedSess/$subjectList[$i]["sessions"] * 100);?>%
												</div></div>
												</td>
												
												<?php /*?><td><?php echo $subjectList[$i]["stream"];?></td><?php */?>
		
                                            </tr>
											<?php
											$i++;
											}
											?>
                                            
                                                                                     
                                        </tbody>
													
	 </table>												
													
<?php }?>

		<!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

	<script type="text/javascript">
            $(function() {
                $('#example1').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
            $('#canvas-holder').empty();
            $('#canvas-holder').append('<canvas id="myChart" height="400" width="800"></canvas>');
            var ctx = document.getElementById("myChart").getContext("2d");
            var dataArray = {
            	    labels: <?php echo json_encode($labelArray); ?>,
            	    datasets: [

            	        {
            	            label: "Subject progress",
            	            fillColor: "rgba(151,187,205,0.5)",
            	            strokeColor: "rgba(151,187,205,0.8)",
            	            highlightFill: "rgba(151,187,205,0.75)",
            	            highlightStroke: "rgba(151,187,205,1)",
            	            data: <?php echo json_encode($valArray); ?>
            	        }
            	    ]
            	};
//             chartBar.destroy();
            chartBar = new Chart(ctx).Bar(dataArray, {
            	scaleOverride: true,
            	scaleSteps: Math.ceil((100)/10),
            	scaleStepWidth: 10,
            	scaleStartValue: 0
            	});
            
            
        </script>
		





