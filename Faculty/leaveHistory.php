<?php
ob_start();
include("autoload.php");
include("check_session.php");

$us	=	new	Auth();
$rec= $us->getUserInfo($fuserId);


$obj	=	new Faculty();


$instruc = $obj->fetchInstructor(); //fetching CourseInstructor

 
 //deleting Leave App Form 
 
 if($_GET["action"]=="del"){
		$vid		=	$_GET["vid"];
		$tt	=	$obj->deleteLeaveAppForm($vid);				
		header("Location:leaveHistory.php?msg=Deleted successfully");
		exit;
      }
	


//fetching Leave App Form 
	
$tmplist	=	$obj->fetchLeaveAppForm();	
$size	=	count($tmplist);


$msg	=$_GET["msg"];
$msg1	=$_GET["msg1"];
	
$webpageTitle	=	"Leave History";
?>




<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php include("top.php"); ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
	

		
    </head>
    <body class="skin-blue" onLoad="startTime()">
	<?php include("head.php"); ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <?php include("sidemenu.php"); ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
			
			<section class="content-header">
			<div id="txt" class="alert alert-info"></div>
			
			 <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li><i class="fa fa-bookmark-o"></i> Masters</li>
                        <li class="active">Manage Leave Application Form</li>
                    </ol>
                    <h1><small>Leave History></h1>
                   
                </section>
               <!-- Main content -->
                <section class="content">    
				
				
				          

                  <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-success">
					
							<?php
							if($msg)
							{
							?>    
							<div class="alert alert-success alert-dismissable"> <?php echo $msg; ?><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
							<?php
							}
							?>    
							
							<?php
							if($msg1)
							{
							?>    
							<div class="alert alert-danger alert-dismissable"> <?php echo $msg1; ?><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
							<?php
							}
							?>    
                              
                              
                               
									
								
							</div>
							
							
							
							</div>
							
							
							
							
							
							
							
							<div class="box">
                                <div class="box-header">     
							                            
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
								
								<div class="col-md-4 form-group">
                                            <label for="exampleInputEmail1">Start Date* </label>
                                            <input type="text" class="form-control" id="startdateFilter" name="startdateFilter" required>
                                        </div>
										
										
										<div class="col-md-4 form-group">
                                            <label for="exampleInputEmail1">End Date *</label>
                                            <input type="text" class="form-control" id="enddateFilter" name="enddateFilter" required>
                                        </div>
										
										<div class="col-md-4 form-group">
										<br>
                                            <button type="button" name="dateFilter" onClick="filterData()" class="btn btn-success">Filter <i class="fa fa-filter"></i></button>
                                        </div>
								
                                    <table id="example1" class="table table-bordered table-hover fetchData">
                                        <thead>
                                            <tr>
                                                <th>Sl No.</th>
                                                <th>Instructor</th>
												<th>Start Date</th>
												<th>End Date</th>	
												<th>Reason</th>											
												<th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										   <?php 
							                $i	=	0;										
							                while($i < $size) {		
											$instru = $obj->fetchInstructorByID($tmplist[$i]["instructor_id"]);  
											$rankName = $obj->fetchRankById($instru[0]["instructor_rank"]);
							                ?>						    
                                            <tr onclick="showGraph(<?php echo $tmplist[$i]["instructor_id"];?>,'<?php echo $rankName[0]["ranks"]." ".$instru[0]["instructor_name"];?>')">
                                                <td><?php echo $i+'1';?></td>
                                                <td><?php echo $rankName[0]["ranks"]." ".$instru[0]["instructor_name"];?> </td>												
												<td><?php echo date('d-m-Y', strtotime($tmplist[$i]["start_date"]));?> </td>
												<td><?php echo date('d-m-Y', strtotime($tmplist[$i]["end_date"]));?> </td>
												<td><?php echo $tmplist[$i]["reason"];?> </td>
									 
                                                <td><a href="editLeaveAppForm.php?id=<?php echo $tmplist[$i]["app_id"];?>" class="btn btn-info">Edit <i class="fa fa-edit"></i></a>&nbsp;<a href="leaveHistory.php?vid=<?php echo $tmplist[$i]["app_id"];?>&action=del" onClick="return askDelete();" class="btn btn-danger">Delete <i class="fa fa-trash-o"></i></a></td>
                                            </tr>
											<?php
											$i++;
											}
											?>
                                            
                                                                                     
                                        </tbody>
                                        
                                    </table>
                                </div><!-- /.box-body -->
                            </div>
							
							
							
							
							
							
							
						<div id="dialog" title="Leave History Graph" style="display: hidden;width:600px;height:400px;">
						
</div>	

                </section><!-- /.content -->
				
				<!-- data table--->
				
				
				
				
				<!--- /.data table -->
				
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        <script src="js/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
		<!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>
		<!-- Chart.js -->
	<script src="js/Chart.js"></script>
			   
		
	<!-- <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"> -->

	<!-- <script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>	 -->
	<link rel="stylesheet" href="../jquery-ui-1.11.2.custom/jquery-ui.css">

	<script src="../jquery-ui-1.11.2.custom/jquery-ui.js"></script>	


	   <!-- <script type="text/javascript">
        CKEDITOR.replace(txtaddress);
        </script> -->
	
		
			<script type="text/javascript">
            $(function() {
                $('#example1').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
//             $( "#dialog" ).dialog({
// 				  minWidth: 630
// 			});
            function showGraph(ins,insName){

            	$.ajax(
                        {
                            url :'ajaxfetchLeaveHistory.php?ins='+ins,
                            type: "POST",
                            async: true,
                            success:function(result) 
                            {	
                                var result = jQuery.parseJSON( result);	
// 							alert(result[0]);
							$('#dialog').empty();
				            $('#dialog').append('<canvas id="myChartLeave" height="400" width="600"></canvas>');
							var ctx = document.getElementById("myChartLeave").getContext("2d");
							
							var dataBarGraph = {
						    labels: result[0],
						    datasets: [

						        {
						            label: "Leave History",
						            fillColor: "rgba(151,187,205,0.5)",
						            strokeColor: "rgba(151,187,205,0.8)",
						            highlightFill: "rgba(151,187,205,0.75)",
						            highlightStroke: "rgba(151,187,205,1)",
						            data: result[1] 
						        }
						    ]
						};
// 							if ($("#dialog").dialog( "isOpen" )===true) {
// 								$( "#dialog" ).dialog( "close" );
// 							}
// 							
							chartBar = new Chart(ctx).Bar(dataBarGraph);
							$( "#dialog" ).attr("title",insName);
							$( "#dialog" ).dialog({
								  minWidth: 630,
								  title: insName
							});
                            }
              
        });
                
            }
        </script>
		
		
		<script type="text/javascript">
		
		function askDelete(){
			if(confirm("Do you want to delete this item ? click OK to continue, CANCEL to exit")){
				return true;
			}else{
				return false;
			}
		}


      		  
  $(function() {
    $( "#startdate" ).datepicker({ dateFormat: 'dd-mm-yy' });  
    $( "#enddate" ).datepicker({ dateFormat: 'dd-mm-yy' });  
	$( "#startdateFilter" ).datepicker({ dateFormat: 'dd-mm-yy' });  
    $( "#enddateFilter" ).datepicker({ dateFormat: 'dd-mm-yy' }); 
  });
  
  
  
   		 $("#enddate").change(function () {
		 
		 
			var startDate = document.getElementById("startdate").value;
			var endDate = document.getElementById("enddate").value;
			

			var startDate = startDate.split("-").reverse().join("-");
			
			var endDate = endDate.split("-").reverse().join("-");
			
		
			startDate = new Date(startDate);

			endDate = new Date(endDate);
			
			if (startDate >endDate) {
			alert("End date should be greater than Start date");
			document.getElementById("enddate").value = "";
			}
			
		});
		
		
		 $("#startdate").change(function () {
			var startDate = document.getElementById("startdate").value;
			var endDate = document.getElementById("enddate").value;
			//alert(Date.parse(startDate));
			
		 	var startDate = startDate.split("-").reverse().join("-");
			
			var endDate = endDate.split("-").reverse().join("-");
			
		
			startDate = new Date(startDate);

			endDate = new Date(endDate);
			
			
			
			if (startDate > endDate) {
			alert("End date should be greater than Start date");
			document.getElementById("enddate").value = "";
			}
			
		});
		
		
		 $("#enddateFilter").change(function () {
		 
		 
			var startDate = document.getElementById("startdateFilter").value;
			var endDate = document.getElementById("enddateFilter").value;
			

			var startDate = startDate.split("-").reverse().join("-");
			
			var endDate = endDate.split("-").reverse().join("-");
			
		
			startDate = new Date(startDate);

			endDate = new Date(endDate);
			
			if (startDate >= endDate) {
			alert("End date should be greater than Start date");
			document.getElementById("enddateFilter").value = "";
			}
			
		});
		
		
		 $("#startdateFilter").change(function () {
			var startDate = document.getElementById("startdateFilter").value;
			var endDate = document.getElementById("enddateFilter").value;
			//alert(Date.parse(startDate));
			
		 	var startDate = startDate.split("-").reverse().join("-");
			
			var endDate = endDate.split("-").reverse().join("-");
			
		
			startDate = new Date(startDate);

			endDate = new Date(endDate);
			
			
			
			if (startDate >= endDate) {
			alert("End date should be greater than Start date");
			document.getElementById("enddateFilter").value = "";
			}
			
		});
		
		function filterData()
		{
			
			var startdateFilter = document.getElementById('startdateFilter').value;
			var enddateFilter = document.getElementById('enddateFilter').value;
			if(startdateFilter!=""&&enddateFilter!="")
			{
			$.ajax(
                            {
                                url :'ajaxfetchLeaveList.php?sd='+startdateFilter+'&ed='+enddateFilter,
                                type: "POST",
                                async: true,
                                success:function(data) 
                                {		
								//alert(data);	
                               $(".fetchData").html(data);
							 //  document.getElementById("artId").innerHTML=data;
                                }
                  
            });
			
			}
			

		}
  
 
  </script>
		
<script>
function startTime() {
    var today=new Date();
    var h=today.getHours();
    var m=today.getMinutes();
    var s=today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt').innerHTML = h+":"+m+":"+s;
    var t = setTimeout(function(){startTime()},500);
}

function checkTime(i) {
    if (i<10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}
</script>
		
        
    </body>
</html>