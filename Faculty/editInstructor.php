<?php
ob_start();
include("autoload.php");
include("check_session.php");

$us	=	new	Auth();
$rec= $us->getUserInfo($fuserId);


$obj	=	new Faculty();

$tmp = $obj->fetchRanks(); //fetching Ranks

$fac = $obj->fetchFacultybyDept();

$id = $_GET["id"];


//updating Instructor

	if(isset($_POST["addButton"])){
	 
			if($obj->editInstructor($_POST,$id)){							
				$msg = "Updated Successfully !";	
			   header("Location:listInstructor.php?msg=$msg");		
			   exit;						
			}else{			
				$msg1 = $obj->getError();
				header("Location:editInstructor.php?id=$id&msg1=$msg1");		
			   exit;						
			}		
		
	}


//fetching Instructor By Id
	
$tmplist	=	$obj->fetchInstructorByID($id);	

$rankId = $tmplist[0]["instructor_rank"];
$rankName = $obj->fetchRankById($rankId);

// Fetch Department

$dep = $obj->getDepartment();
$depCount = count($dep);

$msg	=$_GET["msg"];
$msg1	=$_GET["msg1"];
	
$webpageTitle	=	"Edit Instructor";
?>




<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php include("top.php"); ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
	
<!-- Script to fecth subcontractor ends -->
<script type="text/javascript">

function editcheckPersonalNum(str,id)
{


if(str=="")
{
document.getElementById("txtpersonalNo").innerHTML="";
return;
}
 if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else { // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
      document.getElementById("txtpersonalNo").innerHTML=xmlhttp.responseText;
	  if(xmlhttp.responseText!="")
	  {
	  	document.getElementById("personalNo").value="";
	  }
    }
	
  }
  xmlhttp.open("GET","editcheckpersonalnumber.php?q="+str+'&id='+id,true);
  xmlhttp.send();
}
</script>

<!-- Script to fecth subcontractor ends -->	

		
    </head>
    <body class="skin-blue" onLoad="startTime()">
	<?php include("head.php"); ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <?php include("sidemenu.php"); ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
			
			<section class="content-header">
			
			<div id="txt" class="alert alert-info"></div>
			 <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li><li><i class="fa fa-bookmark-o"></i> Masters</li>
                        <li class="active">Manage Instructor</li>
                    </ol>
                    <h1><small>Edit Instructor</small></h1>
                   
                </section>
               <!-- Main content -->
                <section class="content">    
				
				
				          

                  <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-success">
					
							<?php
							if($msg)
							{
							?>    
							<div class="alert alert-success alert-dismissable"> <?php echo $msg; ?><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
							<?php
							}
							?>    
							
							<?php
							if($msg1)
							{
							?>    
							<div class="alert alert-danger alert-dismissable"> <?php echo $msg1; ?><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
							<?php
							}
							?>    
                              
                              
                                <!-- form start -->
                                <form name="myform" id="myform" role="form" method="post" action="editInstructor.php?id=<?php echo $id;?>" enctype="multipart/form-data">
                                    <div class="col-md-12 box-body">
									
									
									     
										
										 <div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Name *</label>
                                            <input type="text" class="form-control" id="Name" name="Name" placeholder="Name" required value="<?php echo $tmplist[0]["instructor_name"];?>" style="text-transform:uppercase;">
                                        </div>
										
										
										<div class="col-md-6 form-group">
										<label for="inputPassword">Rank *</label>
										
											
											<input type="text" class="form-control" id="ranks" name="ranks" placeholder="Rank" value="<?php echo $rankName[0]["ranks"];?>" style="text-transform:uppercase;" required>
											
										</div>
							
										
										<div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Grade </label>
											<select id="grade" name="grade" class="form-control" >
												<option value="">Select Grade</option>
												<option value="A" <?php if($tmplist[0]["grade"]=="A") {?> selected="selected" <?php }?>>A</option>
												<option value="B" <?php if($tmplist[0]["grade"]=="B") {?> selected="selected" <?php }?>>B</option>
												<option value="C" <?php if($tmplist[0]["grade"]=="C") {?> selected="selected" <?php }?>> C</option>
											</select>
                                        </div>
										
										<div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Personal No. </label><span class="badge pull-right bg-green" id="txtpersonalNo"></span>
                                            <input type="text" class="form-control" id="personalNo" name="personalNo" value="<?php echo $tmplist[0]["personalno"];?>" onChange="editcheckPersonalNum(this.value,<?php echo $id; ?>)" >
                                        </div>
										
										
										<div class="col-md-6 form-group">
										<label for="inputPassword">Faculty </label>
										<select id="faculty" name="faculty" class="form-control" >
												<option value="">Select</option>
													<?php for($r=0;$r<count($dep);$r++){?>
													<option value="<?php echo $dep[$r]["dep_id"];?>" <?php if($dep[$r]["dep_id"]==$tmplist[0]["faculty"]) { ?> selected="selected" <?php }?> ><?php echo $dep[$r]["department_code"];?></option>
													<?php }?>
												
											</select>
										
										 <!--<select id="faculty" name="faculty" class="form-control" >
												<option value="">Select</option>
												
													<?php for($r=0;$r<count($facultyList);$r++){?>
													<option value="<?php echo $facultyList[$r]["staff_id"];?>"><?php echo $facultyList[$r]["staff_name"];?> (<?php echo $facultyList[$r]["department_code"]; ?>)</option>
													<?php }?>
												
											</select>-->
										</div>
										
										<div class="col-md-6 form-group">
										<label for="inputPassword">Aircarft Speciality</label>
										 <select id="aircraft" name="aircraft" class="form-control" >
												<option value="">Select</option>
												<option value="Tu-144M" <?php if($tmplist[0]["aircraft"]=="Tu-144M") {?> selected="selected" <?php }?>>Tu-144M</option>
												<option value="IL-38SD" <?php if($tmplist[0]["aircraft"]=="IL-38SD") {?> selected="selected" <?php }?>>IL-38SD</option>
												<option value="Chetak" <?php if($tmplist[0]["aircraft"]=="Chetak") {?> selected="selected" <?php }?>> Chetak</option>
												<option value="Km-31" <?php if($tmplist[0]["aircraft"]=="Km-31") {?> selected="selected" <?php }?>>Km-31</option>
												<option value="Kv-28" <?php if($tmplist[0]["aircraft"]=="Kv-28") {?> selected="selected" <?php }?>>Kv-28</option>
												<option value="Ka-25" <?php if($tmplist[0]["aircraft"]=="Ka-25") {?> selected="selected" <?php }?>>Ka-25</option>
												<option value="Mig-29K" <?php if($tmplist[0]["aircraft"]=="Mig-29K") {?> selected="selected" <?php }?>>Mig-29K</option>
												<option value="SeaKing" <?php if($tmplist[0]["aircraft"]=="SeaKing") {?> selected="selected" <?php }?>>SeaKing</option>
												<option value="UH-3H" <?php if($tmplist[0]["aircraft"]=="UH-3H") {?> selected="selected" <?php }?>>UH-3H</option>
												<option value="P-8I" <?php if($tmplist[0]["aircraft"]=="P-8I") {?> selected="selected" <?php }?>>P-8I</option>
												<option value="SeaHarrier" <?php if($tmplist[0]["aircraft"]=="SeaHarrier") {?> selected="selected" <?php }?>>Sea Harrier</option>
												<option value="UAV" <?php if($tmplist[0]["aircraft"]=="UAV") {?> selected="selected" <?php }?>>UAV</option>
                                                <option value="ALH" <?php if($tmplist[0]["aircraft"]=="ALH") {?> selected="selected" <?php }?>>ALH</option>
                                                <option value="AJT" <?php if($tmplist[0]["aircraft"]=="AJT") {?> selected="selected" <?php }?>>AJT</option>
                                                <option value="DORNIER" <?php if($tmplist[0]["aircraft"]=="DORNIER") {?> selected="selected" <?php }?>>DORNIER</option>
											</select>
										</div>
										
										<div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Mobile </label>
                                             <input type="text" class="form-control" id="mobile" name="mobile" value="<?php echo $tmplist[0]["mobile"];?>" onKeyPress="return isNumberKey(event)" onChange="Validate()">
                                        </div>
										
										<div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Address </label>
                                            <textarea class="form-control" name="address" id="address"><?php echo $tmplist[0]["address"];?></textarea>
                                        </div>
										
										
                                        
                                      </div><!-- /.box-body -->
									
									
                                    <div class="box-footer">
                                        <button type="submit" name="addButton" class="btn btn-success">Update <i class="fa fa-check"></i></button>
                                    </div>
                                </form>
                            </div><!-- /.box -->
							
							</div>
							
							
							
							</div>
							
							
							

                </section><!-- /.content -->
				
				<!-- data table--->
				
				
				
				
				<!--- /.data table -->
				
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        <script src="js/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
		<!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>
		
		
	<!-- <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"> -->

	<!-- <script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>	 -->
	<link rel="stylesheet" href="../jquery-ui-1.11.2.custom/jquery-ui.css">

	<script src="../jquery-ui-1.11.2.custom/jquery-ui.js"></script>


	   <!-- <script type="text/javascript">
        CKEDITOR.replace(txtaddress);
        </script> -->
	
		
			<script type="text/javascript">
            $(function() {
                $('#example1').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>

		  <script>
		  
		  function isAlphaNumeric(e){ // Alphanumeric only
            var k;
            document.all ? k=e.keycode : k=e.which;
            return((k>47 && k<58)||(k>64 && k<91)||(k>96 && k<123)||k==0);
         }
		  
  $(function() {
    $( "#grade" ).datepicker({ dateFormat: 'dd-mm-yy' });  
  });

   
   $(document).ready(function(){
		  var rank = document.getElementById('ranks').value;
			$("#ranks").autocomplete({
				source:'getRankNames.php?rank='+rank,
				minLength:1
			});
		});

  function Validate() {
       var mobile = document.getElementById("mobile").value;
       var pattern = /^\d{10}$/;
       if (pattern.test(mobile)) {
           //alert("Your mobile number : " + mobile);
           return true;
       }
       alert("It is not valid mobile number.input 10 digits number!");
       return false;
   }

  $(function(){
 
$('#ranks').keyup(function()
{
var yourInput = $(this).val();
re = /[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
var isSplChar = re.test(yourInput);
if(isSplChar)
{
var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",<>\{\}\[\]\\\/]/gi, '');
$(this).val(no_spl_char);
}
});
 
});

   $(function(){
 
$('#Name').keyup(function()
{
var yourInput = $(this).val();
re = /[`~!@#$%^&*()_|+\-=?;:'",<>\{\}\[\]\\\/]/gi;
var isSplChar = re.test(yourInput);
if(isSplChar)
{
var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",<>\{\}\[\]\\\/]/gi, '');
$(this).val(no_spl_char);
}
});
 
});

function AllowAlphabet(e)
{
 isIE = document.all ? 1 : 0
 keyEntry = !isIE ? e.which : event.keyCode;
 if (((keyEntry >= '65') && (keyEntry <= '90')) || ((keyEntry >= '97') && (keyEntry <= '122')) || (keyEntry == '46') || (keyEntry == '32') || keyEntry == '45')
    return true;
 else
{
   alert('Please Enter Only Character values.');
   return false;
     }
} 

$(document).ready(function () {
	 $("#Name").keydown(function (e) {
	       
	        if (e.keyCode) {
	            keyEntry = e.keyCode;
	        } else if (e.which) {
	            keyEntry = e.which;
	        }
	 if (((keyEntry >= '65') && (keyEntry <= '90')) || ((keyEntry >= '97') && (keyEntry <= '122')) 
			 || (keyEntry == '46') || (keyEntry == '32') || keyEntry == '45'
				 || (keyEntry == '8') || keyEntry == '9' || (keyEntry == '37') || keyEntry == '39')
	    return true;
	 else
	{
	   alert('Please Enter Only Character values.');
	   return false;
	     }

	});

	 $("#personalNo").keydown(function (e) {
	     
	     if (e.keyCode) {
	         keyEntry = e.keyCode;
	     } else if (e.which) {
	         keyEntry = e.which;
	     }
	if (((keyEntry >= '48') && (keyEntry <= '57')) ||((keyEntry >= '65') && (keyEntry <= '90')) || ((keyEntry >= '97') && (keyEntry <= '122')) 
			 || (keyEntry == '46') || (keyEntry == '32') || keyEntry == '45'
				 || (keyEntry == '8') || keyEntry == '9' || (keyEntry == '37') || keyEntry == '39')
	 return true;
	else
	{
	// alert('Please Enter Only Character values.');
	return false;
	  }

	});

	 $("#personalNo").keypress(function (e) {
	     
	     if (e.keyCode) {
	         keyEntry = e.keyCode;
	     } else if (e.which) {
	         keyEntry = e.which;
	     }
	if (((keyEntry >= '48') && (keyEntry <= '57')) ||((keyEntry >= '65') && (keyEntry <= '90')) || ((keyEntry >= '97') && (keyEntry <= '122')) 
			 || (keyEntry == '46') || (keyEntry == '32') || keyEntry == '45'
				 || (keyEntry == '8') || keyEntry == '9' || (keyEntry == '37') || keyEntry == '39')
	 return true;
	else
	{
	// alert('Please Enter Only Character values.');
	return false;
	  }

	});

	 $( "#myform" ).submit(function( event ) {
		 var correct = true;
		 if( !$("#Name").val() ) {
			 $("#Name").addClass('warning');
			 correct = false;
	   }
		 else{
			 $("#Name").removeClass('warning');
		 }
		 if( !$("#ranks").val() ) {
			 $("#ranks").addClass('warning');
			 correct = false;
	   }
		 else{
			 $("#ranks").removeClass('warning');
		 }
		 if( !$("#personalNo").val() ) {
			 $("#personalNo").addClass('warning');
			 correct = false;
	   }
		 else{
			 $("#personalNo").removeClass('warning');
		 }
		   if(!correct)
		 event.preventDefault();
		 });

	}); 
 
  </script>
		
<script>
function startTime() {
    var today=new Date();
    var h=today.getHours();
    var m=today.getMinutes();
    var s=today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt').innerHTML = h+":"+m+":"+s;
    var t = setTimeout(function(){startTime()},500);
}

function checkTime(i) {
    if (i<10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}
</script>	
		
        
    </body>
</html>