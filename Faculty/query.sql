SELECT mip.course_name, mip.course_no, mip.start_date, mip.end_date, mip.strength, 
FLOOR(DATEDIFF(mip.end_date, mip.start_date)/7) as full_week,
FLOOR(DATEDIFF(wtp.end_date, mip.start_date)/7) as current_week,
class.class_room,
coff.instructor_name as course_off, coff_rank.ranks as course_off_rank, cins.instructor_name as course_ins, cins_rank.ranks as course_ins_rank
FROM tbl_wtp as wtp 
inner join tbl_mip as mip on mip.course_id = wtp.course_id 
left join tbl_class_room as class on wtp.classroomid = class.class_room_id
left join tbl_instructor as coff on coff.instructor_id = mip.course_officer
left join tbl_instructor_ranks as coff_rank on coff.instructor_rank = coff_rank.id
left join tbl_instructor as cins on cins.instructor_id = mip.course_instructor
left join tbl_instructor_ranks as cins_rank on cins.instructor_rank = cins_rank.id
where wtp.start_date <= ? and wtp.end_date >= ?


SELECT wtp.session, ins_rank.ranks, ins.Instructor_name, sub.subject_name FROM tbl_wtp_per_date as wtp 
left join tbl_instructor as ins on ins.instructor_id = wtp.Instructor_Id
left join tbl_instructor_ranks as ins_rank on ins.instructor_rank = ins_rank.id
left join tbl_course_subjects as sub on sub.sub_id = wtp.subject_id
where wtp_id = 3 and day = "Monday"


SELECT DATE_SUB(NOW(), INTERVAL 1 YEAR),
DATE_ADD(LAST_DAY(DATE_SUB(NOW(), INTERVAL 1 YEAR)), INTERVAL 1 DAY)
