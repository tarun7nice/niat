<?php
ob_start();
include("autoload.php");
include("check_session.php");
$us	=	new	Auth();
$rec= $us->getUserInfo($fuserId);

$obj	=	new Faculty();
$sd = $_GET["sd"];
$ed = $_GET["ed"];

$sd = date('Y-m-d', strtotime($sd));
$ed = date('Y-m-d', strtotime($ed));

$data = $obj->fetchLeavebyDate($sd,$ed);
$size	=	count($data);
?>

                                        <thead>
                                            <tr>
                                                <th>Sl No.</th>
                                                <th>Instructor</th>
												<th>Start Date</th>
												<th>End Date</th>	
												<th>Reason</th>											
												<th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										   <?php 
							                $i	=	0;										
							                while($i < $size) {				
							                ?>						    
                                            <tr>
                                                <td><?php echo $i+'1';?></td>
                                                <td><?php $instru = $obj->fetchInstructorByID($data[$i]["instructor_id"]);  echo $instru[0]["instructor_name"];?> </td>												
												<td><?php echo date('d-m-Y', strtotime($data[$i]["start_date"]));?> </td>
												<td><?php echo date('d-m-Y', strtotime($data[$i]["end_date"]));?> </td>
												<td><?php echo $data[$i]["reason"];?> </td>
									 
                                                <td><a href="editLeaveAppForm.php?id=<?php echo $data[$i]["app_id"];?>" class="btn btn-info">Edit <i class="fa fa-edit"></i></a>&nbsp;<a href="addLeaveAppForm.php?vid=<?php echo $data[$i]["app_id"];?>&action=del" onClick="return askDelete();" class="btn btn-danger">Delete <i class="fa fa-trash-o"></i></a></td>
                                            </tr>
											<?php
											$i++;
											}
											?>
                                            
                                                                                     
                                        </tbody>