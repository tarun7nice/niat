<?php
ob_start();
include("autoload.php");
include("check_session.php");

$us	=	new	Auth();
$rec= $us->getUserInfo($fuserId);


$obj	=	new Faculty();

$id = $_GET["id"];


$instruc = $obj->fetchInstructor(); //fetching CourseInstructor


//updating Leave App Form

	if(isset($_POST["addButton"])){
	 
			if($obj->editLeaveAppForm($_POST,$id)){							
				$msg = "Updated Successfully !";	
			   header("Location:addLeaveAppForm.php?msg=$msg");		
			   exit;						
			}else{			
				$msg1 = $obj->getError();
				header("Location:editLeaveAppForm.php?id=$id&msg1=$msg1");		
			   exit;						
			}		
		
	}



//fetching Leave App Form 
	
$tmplist	=	$obj->fetchLeaveAppFormByID($id);	
$size	=	count($tmplist);


$msg	=$_GET["msg"];
$msg1	=$_GET["msg1"];
	
$webpageTitle	=	"Edit Leave Application Form";
?>




<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php include("top.php"); ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
	

		
    </head>
    <body class="skin-blue" onLoad="startTime()">
	<?php include("head.php"); ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <?php include("sidemenu.php"); ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
			
			<section class="content-header">
			
			<div id="txt" class="alert alert-info"></div>
			 <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li><li><i class="fa fa-bookmark-o"></i> Masters</li>
                        <li class="active">Manage Leave Application Form</li>
                    </ol>
                    <h1><small>Edit Leave Application Form</small></h1>
                   
                </section>
               <!-- Main content -->
                <section class="content">    
				
				
				          

                  <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-success">
					
							<?php
							if($msg)
							{
							?>    
							<div class="alert alert-success alert-dismissable"> <?php echo $msg; ?><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
							<?php
							}
							?>    
							
							<?php
							if($msg1)
							{
							?>    
							<div class="alert alert-danger alert-dismissable"> <?php echo $msg1; ?><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
							<?php
							}
							?>    
                              
                              
                                <!-- form start -->
                                <form name="myform" role="form" method="post" action="editLeaveAppForm.php?id=<?php echo $id;?>" enctype="multipart/form-data">
                                    <div class="col-md-12 box-body">
									
									
									     
										<div class="col-md-6 form-group">
										<label for="inputPassword">Instructor *</label>
										 <select id="courseInstructor" name="courseInstructor" class="form-control" required >
												<option value="">Select</option>
													<?php for($r=0;$r<count($instruc);$r++){
													$rankId = $instruc[$r]["instructor_rank"];
                                                    $rankName = $obj->fetchRankById($rankId);
														?>
													<option value="<?php echo $instruc[$r]["instructor_id"];?>" <?php if($instruc[$r]["instructor_id"]==$tmplist[0]["instructor_id"]){?> selected="selected" <?php }?>><?php echo $instruc[$r]["instructor_name"];?>-&nbsp;<?php echo $rankName[$r]["ranks"];?>-&nbsp;<?php echo $instruc[$r]["personalno"];?></option>
													<?php }?>
												
											</select>
										</div>
										
											
										<div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Start Date </label>
                                            <input type="text" class="form-control" id="startdate" name="startdate" value="<?php echo date('d-m-Y', strtotime($tmplist[0]["start_date"]));?>">
                                        </div>
										
										
										<div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">End Date </label>
                                            <input type="text" class="form-control" id="enddate" name="enddate"  value="<?php echo date('d-m-Y', strtotime($tmplist[0]["end_date"]));?>">
                                        </div>
										
										
								         <div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Reason</label>
                                            <textarea class="form-control" id="reason" name="reason" rows="1" placeholder="Reason"><?php echo $tmplist[0]["reason"];?></textarea>
                                        </div>
										
										
                                        
                                      </div><!-- /.box-body -->
									
									
                                    <div class="box-footer">
                                        <button type="submit" name="addButton" class="btn btn-success">Save <i class="fa fa-check"></i></button>
                                    </div>
                                </form>
                            </div><!-- /.box -->
							
							</div>
							
							
							
							</div>
							
							

                </section><!-- /.content -->
				
				<!-- data table--->
				
				
				
				
				<!--- /.data table -->
				
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        <script src="js/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
		<!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>
		
		
	<!-- <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"> -->

	<!-- <script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>	 -->
	<link rel="stylesheet" href="../jquery-ui-1.11.2.custom/jquery-ui.css">

	<script src="../jquery-ui-1.11.2.custom/jquery-ui.js"></script>


	   <!-- <script type="text/javascript">
        CKEDITOR.replace(txtaddress);
        </script> -->
	
		
			<script type="text/javascript">
            $(function() {
                $('#example1').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>
		
		
	<script type="text/javascript">
	
  $(function() {
    $( "#startdate" ).datepicker({ dateFormat: 'dd-mm-yy' });  
    $( "#enddate" ).datepicker({ dateFormat: 'dd-mm-yy' });  
  });
  
  
  
   		 $("#enddate").change(function () {
			var startDate = document.getElementById("startdate").value;
			var endDate = document.getElementById("enddate").value;
			
			
			var startDate = startDate.split("-").reverse().join("-");
			
			var endDate = endDate.split("-").reverse().join("-");
			
		
			startDate = new Date(startDate);

			endDate = new Date(endDate);
			
						
			//alert(Date.parse(startDate));
			if (startDate > endDate) {
			alert("End date should be greater than Start date");
			document.getElementById("enddate").value = "";
			}
			
		});
		
		
		 $("#startdate").change(function () {
			var startDate = document.getElementById("startdate").value;
			var endDate = document.getElementById("enddate").value;		
			
			var startDate = startDate.split("-").reverse().join("-");
			
			var endDate = endDate.split("-").reverse().join("-");
			
		
			startDate = new Date(startDate);

			endDate = new Date(endDate);
			
			
			if (startDate > endDate) {
			alert("End date should be greater than Start date");
			document.getElementById("enddate").value = "";
			}			
			
		});

  
 
  </script>
		
<script>
function startTime() {
    var today=new Date();
    var h=today.getHours();
    var m=today.getMinutes();
    var s=today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt').innerHTML = h+":"+m+":"+s;
    var t = setTimeout(function(){startTime()},500);
}

function checkTime(i) {
    if (i<10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}
</script>
		
        
    </body>
</html>