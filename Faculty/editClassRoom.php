<?php
ob_start();
include("autoload.php");
include("check_session.php");

$us	=	new	Auth();
$rec= $us->getUserInfo($fuserId);


$obj	=	new Faculty();

$id= $_GET["id"]; //class room id

//updating Class Room

	if(isset($_POST["addButton"])){
	 
			if($obj->editClassRoom($_POST,$id)){							
				$msg = "Updated Successfully !";	
			   header("Location:addClassRoom.php?msg=$msg");		
			   exit;						
			}else{			
				$msg1 = $obj->getError();
				header("Location:addClassRoom.php?msg1=$msg1");		
			   exit;						
			}		
		
	}



//fetching ClassRoom 
	
$tmplist	=	$obj->fetchClassRoomByID($id);	
$size	=	count($tmplist);


$msg	=$_GET["msg"];
$msg1	=$_GET["msg1"];
	
$webpageTitle	=	"Manage Class Room";
?>




<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php include("top.php"); ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
	

		
    </head>
    <body class="skin-blue" onLoad="startTime()">
	<?php include("head.php"); ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <?php include("sidemenu.php"); ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
			
			<section class="content-header">
			
			<div id="txt" class="alert alert-info"></div>
			 <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li><i class="fa fa-bookmark-o"></i> Masters</li>
                        <li class="active">Manage Class Room</li>
                    </ol>
                    <h1><small>Edit Class Room</small></h1>
                   
                </section>
               <!-- Main content -->
                <section class="content">    
				
				
				          

                  <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-success">
					
							<?php
							if($msg)
							{
							?>    
							<div class="alert alert-success alert-dismissable"> <?php echo $msg; ?><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
							<?php
							}
							?>    
							
							<?php
							if($msg1)
							{
							?>    
							<div class="alert alert-danger alert-dismissable"> <?php echo $msg1; ?><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
							<?php
							}
							?>    
                              
                              
                                <!-- form start -->
                                <form name="myform" role="form" method="post" action="editClassRoom.php?id=<?php echo $id;?>" enctype="multipart/form-data">
                                    <div class="col-md-12 box-body">
									
									
									     
										
										 <div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Class Room *</label>
                                            <input type="text" class="form-control" id="Name" name="Name" placeholder="Name" required style="text-transform:uppercase;" value="<?php echo $tmplist[0]["class_room"];?>" >
                                        </div>
							
										
										<div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Strength* </label>
                                            <input type="text" class="form-control" id="strength" name="strength" placeholder="Strength" required onKeyPress="return isNumberKey(event)" onChange="return checkStrength()" value="<?php echo $tmplist[0]["strength"];?>">
                                        </div>
										
										
										<div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Facilities </label>
                                            <input type="text" class="form-control" id="facilities" name="facilities"  placeholder="Facilities"value="<?php echo $tmplist[0]["facilities"];?>">
                                        </div>
										
										
										
                                        
                                      </div><!-- /.box-body -->
									
									
                                    <div class="box-footer">
                                        <button type="submit" name="addButton" class="btn btn-success">Save <i class="fa fa-check"></i></button>
                                    </div>
                                </form>
                            </div><!-- /.box -->
							
							</div>
							
							
							
							</div>
							
							
							

                </section><!-- /.content -->
				
				<!-- data table--->
				
				
				
				
				<!--- /.data table -->
				
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        <script src="js/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
		<!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>
		
		
	<!-- <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"> -->

	<!-- <script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>	 -->
	<link rel="stylesheet" href="../jquery-ui-1.11.2.custom/jquery-ui.css">

	<script src="../jquery-ui-1.11.2.custom/jquery-ui.js"></script>


	   <!-- <script type="text/javascript">
        CKEDITOR.replace(txtaddress);
        </script> -->
	
		
			<script type="text/javascript">
            $(function() {
                $('#example1').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>
		
		
		<script type="text/javascript">

	function checkStrength()
	{
		if(document.getElementById('strength').value==0)
		{
			alert("Stregth cannot be 0");
			document.getElementById('strength').value="";
			return false;
		}
		else
		{
			return true;
		}
	}
	
	
	
      function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
	{
		alert("Please enter valid number");
        return false;
	}
	else
	{
    return true;
	}
}    


	/*function editcheckclassName(newclassname,classname)
	{
	//alert('rrr');
	alert(classname);
	$.ajax(
                            {
                                url :'editajaxcheckclassname.php?name='+classname+'&new='+newclassname,
                                type: "POST",
                                async: true,
                                success:function(data) 
                                {	
								if(data!=="")
								{
									alert("Class name already exist");
									document.getElementById('Name').value="";
                                }
								}
								
	});
	}
*/
 
  </script>
		
<script>
function startTime() {
    var today=new Date();
    var h=today.getHours();
    var m=today.getMinutes();
    var s=today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt').innerHTML = h+":"+m+":"+s;
    var t = setTimeout(function(){startTime()},500);
}

function checkTime(i) {
    if (i<10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}
</script>
		
        
    </body>
</html>