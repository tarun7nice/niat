<?php
ob_start ();
include ("autoload.php");
include ("check_session.php");

$us = new Auth ();
$rec = $us->getUserInfo ( $fuserId );

$webpageTitle = "Reports";

$obj = new Faculty ();
$courseList = $obj->fetchMIPPercentage ();
$size	=	count($courseList);

// sort in descending order based on percentage
usort($courseList, function($a, $b) {
	return $b['PERCENT'] - $a['PERCENT'];
});

$allCourseLabels = array();
$allCourseData = array();

$i = 0;
while($i < $size) {
$allCourseLabels[$i] =  $courseList[$i]["COURSE"];
$allCourseData[$i] =  $courseList[$i]["PERCENT"];
$i++;
}


$topTenCourseLabels = array();
$topTenCourseData = array();

$i = 0;
$ubound = 0;
if($size > 10)
	$ubound = 10;
else $ubound = $size;

while($i < $ubound) {
	$topTenCourseLabels[$i] =  $courseList[$i]["COURSE"];
	$topTenCourseData[$i] =  $courseList[$i]["PERCENT"];
	$i++;
}

// sort in ascending order based on percentage
usort($courseList, function($a, $b) {
	return $a['PERCENT'] - $b['PERCENT'];
});
$belowTenCourseLabels = array();
$belowTenCourseData = array();

$i = 0;
while($i < $ubound) {
	$belowTenCourseLabels[$i] =  $courseList[$i]["COURSE"];
	$belowTenCourseData[$i] =  $courseList[$i]["PERCENT"];
	$i++;
}
?>



<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
       <?php include("top.php"); ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
</head>
<body class="skin-blue">
      <?php include("head.php"); ?>

        <div class="wrapper row-offcanvas row-offcanvas-left">
		
		<?php include("sidemenu.php"); ?>
           

            <!-- Right side column. Contains the navbar and content of the page -->
		<aside class="right-side">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>Reports</h1>
				<ol class="breadcrumb">
					<li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li class="active">Reports</li>
				</ol>
			</section>
			<!-- Main content -->
			<section class="content">
				<div class="row">
				<h4>Status of All Courses</h4>
				<canvas id="myChartAllCourse" height="400"></canvas>
				</div>
				<div class="row">
				<h4>Status of Top Ten Courses</h4>
				<canvas id="myChartTopTenCourse" height="400"></canvas>
				</div>
				<div class="row">
				<h4>Status of Bottom Ten Courses</h4>
				<canvas id="myChartBelowTenCourse" height="400"></canvas>
				</div>

			</section>
			<!-- /.content -->
		</aside>
		<!-- /.right-side -->
	</div>
	<!-- ./wrapper -->

	<!-- add new calendar event modal -->


	<!-- jQuery 2.0.2 -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery UI 1.10.3 -->
	<script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
	<!-- Morris.js charts 
        <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="js/plugins/morris/morris.min.js" type="text/javascript"></script>
-->
	<!-- Sparkline -->
	<script src="js/plugins/sparkline/jquery.sparkline.min.js"
		type="text/javascript"></script>
	<!-- jvectormap -->
	<script src="js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"
		type="text/javascript"></script>
	<script src="js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"
		type="text/javascript"></script>
	<!-- fullCalendar -->
	<script src="js/plugins/fullcalendar/fullcalendar.min.js"
		type="text/javascript"></script>
	<!-- jQuery Knob Chart -->
	<script src="js/plugins/jqueryKnob/jquery.knob.js"
		type="text/javascript"></script>
	<!-- daterangepicker -->
	<script src="js/plugins/daterangepicker/daterangepicker.js"
		type="text/javascript"></script>
	<!-- Bootstrap WYSIHTML5 -->
	<script
		src="js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"
		type="text/javascript"></script>
	<!-- iCheck -->
	<script src="js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>

	<!-- AdminLTE App -->
	<script src="js/AdminLTE/app.js" type="text/javascript"></script>

	<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
	<script src="js/AdminLTE/dashboard.js" type="text/javascript"></script>

	<!-- Chart.js -->
	<script src="js/Chart.js"></script>
	<script type="text/javascript">
	//var courseData = <?php echo json_encode($courseList); ?>;
	var ctxAllCourse = document.getElementById("myChartAllCourse").getContext("2d");
	var ctxTopTen = document.getElementById("myChartTopTenCourse").getContext("2d");
	var ctxBelowTen = document.getElementById("myChartBelowTenCourse").getContext("2d");
	
	var dataAllCourse = {
    labels: <?php echo json_encode($allCourseLabels); ?>,
    datasets: [

        {
            label: "My Second dataset",
            fillColor: "rgba(151,187,205,0.5)",
            strokeColor: "rgba(151,187,205,0.8)",
            highlightFill: "rgba(151,187,205,0.75)",
            highlightStroke: "rgba(151,187,205,1)",
            data: <?php echo json_encode($allCourseData); ?>
        }
    ]
};

	var dataTopTenCourse = {
		    labels: <?php echo json_encode($topTenCourseLabels); ?>,
		    datasets: [

		        {
		            label: "My Second dataset",
		            fillColor: "rgba(000,187,205,0.5)",
		            strokeColor: "rgba(000,187,205,0.8)",
		            highlightFill: "rgba(000,187,205,0.75)",
		            highlightStroke: "rgba(000,187,205,1)",
		            data: <?php echo json_encode($topTenCourseData); ?>
		        }
		    ]
		};
	var dataBelowTenCourse = {
		    labels: <?php echo json_encode($belowTenCourseLabels); ?>,
		    datasets: [

		        {
		            label: "My Second dataset",
		            fillColor: "rgba(151,187,000,0.5)",
		            strokeColor: "rgba(151,187,000,0.8)",
		            highlightFill: "rgba(151,187,000,0.75)",
		            highlightStroke: "rgba(151,187,000,1)",
		            data: <?php echo json_encode($belowTenCourseData); ?>
		        }
		    ]
		};
var myBarChartAllCourse = new Chart(ctxAllCourse).Bar(dataAllCourse);
var myBarChartTopTen = new Chart(ctxTopTen).Bar(dataTopTenCourse);
var myBarChartBelowTen = new Chart(ctxBelowTen).Bar(dataBelowTenCourse);
	</script>

</body>
</html>
