<?php
ob_start();
include("autoload.php");
include("check_session.php");

$us	=	new	Auth();
$rec= $us->getUserInfo($fuserId);


$obj	=	new Faculty();

$tmp = $obj->fetchCourseOfficer(); //fetching CourseOfficer


$instruc = $obj->fetchCourseInstructor(); //fetching CourseOfficer


$name= $_GET["name"];


//inserting Instructor

	if(isset($_POST["addButton"])){
	 
			if($obj->addMIP($_POST,$fuserId)){							
				$msg = "Added Successfully !";	
			   header("Location:addMIP.php?msg=$msg");		
			   exit;						
			}else{			
				$msg1 = $obj->getError();
				header("Location:addMIP.php?msg1=$msg1");		
			   exit;						
			}		
		
	}

 

//fetching Instructor 
	
$tmplist	=	$obj->fetchInstructor();	
$size	=	count($tmplist);


// Fetch Department

$dep = $obj->getDepartment();
$depCount = count($dep);


$msg	=$_GET["msg"];
$msg1	=$_GET["msg1"];
	
$webpageTitle	=	"Manage MIP";
?>




<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php include("top.php"); ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

<script>
function startTime() {
    var today=new Date();
    var h=today.getHours();
    var m=today.getMinutes();
    var s=today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt').innerHTML = h+":"+m+":"+s;
    var t = setTimeout(function(){startTime()},500);
}

function checkTime(i) {
    if (i<10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}
</script>	

	
	
	<script language="javascript">

function removeDetails(spanId){ //removes fields	
	var spn=document.getElementById(spanId);	
	spn.remove();
	}	
	
	
var counter = 1;	



//var limit = 3;
function addInput(divName){


    /* if (counter == limit)  {
          alert("You have reached the limit of adding " + counter + " inputs");
     }
     else {*/
          var newdiv = document.createElement('div');
		  
		  var subj = "subject"+counter;
		  var ses = "session"+counter;
		  var pha = "phase"+counter;		  
		  var stre = "stream"+counter;

          newdiv.innerHTML = "<span id='span"+counter+"'><div class='col-md-3 form-group'><label for='exampleInputEmail1'>Subject * </label><input type='text' class='form-control' id="+subj+" name="+subj+" style='text-transform:uppercase;' required></div><div class='col-md-3 form-group'><label for='exampleInputEmail1'>Sessions *</label><input type='text' class='form-control' id="+ses+" name="+ses+" onKeyPress='return isNumberKey(event)' required onChange='return checkSession("+counter+")' ></div><div class='col-md-3 form-group'><label for='exampleInputEmail1'>Phase </label><select id="+pha+" name="+pha+" class='form-control'><option value=''>Select</option><option value='Phase1'>Phase1</option><option value='Phase2'>Phase2</option></select></div><div class='col-md-3 form-group'><label for='exampleInputEmail1'>Stream </label><select id="+stre+" name="+stre+" class='form-control'><option value=''>Select</option><option value='A*'>A*</option><option value='A'>A</option><option value='B'>B</option><option value='C'>C</option><option value='D'>D</option><option value='E'>E</option></select></div><img class='btn btn-danger' src='img/delete.png' onclick=removeDetails('span"+counter+"');></img><span>";
		  

          document.getElementById(divName).appendChild(newdiv);	

          counter++;
          document.getElementById('counter').value=counter;
		  
		  
		  

}


</script>
	
	
	
	
	
	
		
    </head>
    <body class="skin-blue" onLoad="startTime()">
	<?php include("head.php"); ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <?php include("sidemenu.php"); ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
			
			<section class="content-header">
			
			<div id="txt" class="alert alert-info"></div>
			
			 <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li><li><i class="fa fa-bookmark-o"></i> Masters</li>
                        <li class="active">Manage MIP</li>
                    </ol>
                    <h1><small>Manage MIP</small></h1>
                   
                </section>
               <!-- Main content -->
                <section class="content">    
				
				
				          

                  <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-success">
					
							<?php
							if($msg)
							{
							?>    
							<div class="alert alert-success alert-dismissable"> <?php echo $msg; ?><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
							<?php
							}
							?>    
							
							<?php
							if($msg1)
							{
							?>    
							<div class="alert alert-danger alert-dismissable"> <?php echo $msg1; ?><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
							<?php
							}
							?>    
                              
                              
                                <!-- form start -->
                                <form name="myform" role="form" method="post" action="addMIP.php" enctype="multipart/form-data" onSubmit="return checkDates();">
                                    <div class="col-md-12 box-body">
									
									
									     
										
										 <div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Course Name *</label>
                                            <input type="text" class="form-control" id="Name" name="Name" style="text-transform:uppercase;" placeholder="Course Name" required value="<?php echo $name;?>">
                                        </div>
										
										 <div class="col-md-6 form-group" style="margin-top:24px;">
										  <label for="exampleInputEmail1"></label>
										 <button type="button" class="btn btn-info" onClick="fetchAlreCourse();">Check</button>
										</div>
										
										<div style="clear: both;"></div>
										
										
										<span id="editCrse">
										
										
										 <div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Course No *</label>
                                            <input type="text" class="form-control" id="courseno" name="courseno" placeholder="Course NO" required>
                                        </div>
										
										
										<div class="col-md-6 form-group">
										<label for="inputPassword">Course Officer *</label>
										 <select id="courseofficer" name="courseofficer" class="form-control" required >
												<option value="">Select</option>
													<?php for($r=0;$r<count($tmp);$r++){
													  $rankId = $tmp[$r]["instructor_rank"];
                                                      $rankName = $obj->fetchRankById($rankId);
													
													?>
													<option value="<?php echo $tmp[$r]["instructor_id"];?>"><?php echo $tmp[$r]["instructor_name"];?>-&nbsp;<?php echo $rankName[0]["ranks"];?>-&nbsp;<?php echo $tmp[$r]["personalno"];?></option>
													<?php }?>
												
											</select>
										</div>
										
										
										
										<div class="col-md-6 form-group">
										<label for="inputPassword"> Instructor *</label>
										 <select id="courseInstructor" name="courseInstructor" class="form-control" required >
												<option value="">Select</option>
													<?php for($r=0;$r<count($instruc);$r++){
													
													  $rankId = $instruc[$r]["instructor_rank"];
                                                      $rankName = $obj->fetchRankById($rankId);
													?>
													<option value="<?php echo $instruc[$r]["instructor_id"];?>"><?php echo $instruc[$r]["instructor_name"];?>-&nbsp;<?php echo $rankName[0]["ranks"];?>-&nbsp;<?php echo $instruc[$r]["personalno"];?></option>
													<?php }?>
												
											</select>
										</div>
										
										
										
										 <div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Strength *</label>
                                            <input type="text" class="form-control" id="strength" name="strength" placeholder="Strength" onKeyPress="return isNumberKey(event)" onChange="return checkStrength()" required>
                                        </div>
										
							
										
										<div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Start Date *</label>
                                            <input type="text" class="form-control" id="startdate" name="startdate" required>
                                        </div>
										
										
										<div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">End Date *</label>
                                            <input type="text" class="form-control" id="enddate" name="enddate" required>
                                        </div>
										
										
										<div class="col-md-6 form-group">
										<label for="inputPassword">Faculty Responsible*</label>
										 <select id="faculty" name="faculty" class="form-control" required>
												<option value="">Select</option>
												
													<?php for($r=0;$r<count($dep);$r++){?>
													<option value="<?php echo $dep[$r]["dep_id"];?>"><?php echo $dep[$r]["department_code"]; ?></option>
													<?php }?>
												
											</select>
										</div>
										
										
										<div style="clear:both;"></div>
										
										
										
										
										
										<div class="col-md-3 form-group">
                                            <label for="exampleInputEmail1">Subject *</label>
                                            <input type="text" class="form-control" id="subject0" name="subject0" style="text-transform:uppercase;"  required>
                                        </div>
										
										
										<div class="col-md-3 form-group">
                                            <label for="exampleInputEmail1">Sessions *</label>
                                            <input type="text" class="form-control" id="session0" name="session0" onKeyPress="return isNumberKey(event)" onChange="return checkSession(0)" required>
                                        </div>
										
										
										<div class="col-md-3 form-group">
                                            <label for="exampleInputEmail1">Phase </label>                                         
										   <select id="phase0" name="phase0" class="form-control">
												<option value="">Select</option>
												<option value="Phase1">Phase1</option>
												<option value="Phase2">Phase2</option>	
												
											</select>
                                        </div>
										
										
										<div class="col-md-3 form-group">
                                            <label for="exampleInputEmail1">Stream </label>
                                            <select id="stream0" name="stream0" class="form-control">
												<option value="">Select</option>
												<option value="A*">A*</option>
												<option value="A">A</option>
												<option value="B">B</option>	
												<option value="C">C</option>
												<option value="D">D</option>	
												<option value="E">E</option>	
											</select>
                                        </div>
										
										
										
										 <input type="hidden" name="counter" id="counter" value="1" />
										
										
										<div class="box-footer">
                                        <button type="button" name="addButton" class="btn btn-info" onClick="addInput('dynamicInput');" >Add Subject <i class="fa fa-check"></i></button>
                                     </div>
										
										 </span>	
										
										
										
										
										<div class="col-md-12 form-group" id='dynamicInput'>
										
										</div>
										
										
										
									
									 
										
                                        
                                      </div><!-- /.box-body -->
									
									
                                    <div class="box-footer">
                                        <button type="submit" name="addButton" class="btn btn-success">Save <i class="fa fa-check"></i></button>
                                    </div>
                                </form>
                            </div><!-- /.box -->
							
							</div>
							
							
							
							</div>
							
							
							

                </section><!-- /.content -->
				
				<!-- data table--->
				
				
				
				
				<!--- /.data table -->
				
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        <script src="js/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
		<!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>
		
<!-- 			   
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">

<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>	

-->
<link rel="stylesheet" href="../jquery-ui-1.11.2.custom/jquery-ui.css">

<script src="../jquery-ui-1.11.2.custom/jquery-ui.js"></script>	


	   <!-- <script type="text/javascript">
        CKEDITOR.replace(txtaddress);
        </script> -->
	
		
			<script type="text/javascript">
            $(function() {
                $('#example1').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>

		  <script>
		  
  $(function() {
    $( "#startdate" ).datepicker({ dateFormat: 'dd-mm-yy' });  
    $( "#enddate" ).datepicker({ dateFormat: 'dd-mm-yy' }); 
	
  });
  
  
 
		$(document).keyup(function(){
		  var name = document.getElementById('Name').value;
			$("#Name").autocomplete({
				source:'getCoursesNames.php?name='+name,
				minLength:1
			});
		});
		
				
				
		function fetchAlreCourse(){
		
		 var name = document.getElementById('Name').value;

		 $.ajax(
					{
						url :'ajaxfetchAlreCourse.php?name='+name,
						type: "POST",
						async: false,
						success:function(data) 
						{
						$("#editCrse").html(data);							
							
						}
		  
		});
			
		}
			
      

      function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
	{
		alert("Please enter valid number");
        return false;
	}
	else
	{
    return true;
	}
}    

	function checkStrength()
	{
		if(document.getElementById('strength').value==0)
		{
			alert("Strength cannot be 0");
			document.getElementById('strength').value="";
			return false;
		}
		else
		{
			return true;
		}
	}

   
   function checkSession(val)
   {
   		if(document.getElementById('session'+val).value==0)
		{
			alert("Session cannot be 0");
			document.getElementById('session'+val).value=""
			return false;
		}
		else
		{
			return true;
		}
   }
   

   

   		 $("#enddate").change(function () {
			var startDate = document.getElementById("startdate").value;
			var endDate = document.getElementById("enddate").value;
			
			var startDate = startDate.split("-").reverse().join("-");
			
			var endDate = endDate.split("-").reverse().join("-");
			
		
			startDate = new Date(startDate);

			endDate = new Date(endDate);
			
			if (startDate >= endDate) {
			alert("End date should be greater than Start date");
			document.getElementById("enddate").value = "";
			}
			
		});
		
		$("#startdate").change(function () {
			var startDate = document.getElementById("startdate").value;
			var endDate = document.getElementById("enddate").value;
			//alert(Date.parse(startDate));
			
		 	var startDate = startDate.split("-").reverse().join("-");
			
			var endDate = endDate.split("-").reverse().join("-");
			
		
			startDate = new Date(startDate);

			endDate = new Date(endDate);
			
			
			
			if (startDate >= endDate) {
			alert("End date should be greater than Start date");
			document.getElementById("enddate").value = "";
			}
			
		});	
		
		
		<?php if($name!=""){?>
		
		fetchAlreCourse();
		
		<?php }?>
			
		

 
  </script>
		

		
        
    </body>
</html>
