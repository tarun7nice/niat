<?php
ob_start();
include("autoload.php");
include("check_session.php");
$us	=	new	Auth();
$rec= $us->getUserInfo($fuserId);


$name = $_GET["name"];


$obj	=	new Faculty();


$tmp = $obj->fetchCourseOfficer(); //fetching CourseOfficer



$instruc = $obj->fetchCourseInstructor(); //fetching CourseOfficer

$courseInfo = $obj->fetchCourseInfo($name);


$subInfo = $obj->fetchSubjectInfo($courseInfo[0]["course_id"]);

// Fetch Department

$dep = $obj->getDepartment();
$depCount = count($dep);


?>


 										<div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Course No *</label>
                                            <input type="text" class="form-control" id="courseno" name="courseno" placeholder="Course NO" required value="<?php echo $courseInfo[0]["course_no"];?>" required> 
                                        </div>
										
										
										<div class="col-md-6 form-group">
										<label for="inputPassword">Course Officer *</label>
										 <select id="courseofficer" name="courseofficer" class="form-control" required>
												<option value="">Select</option>
													<?php for($r=0;$r<count($tmp);$r++){
													  $rankId = $tmp[$r]["instructor_rank"];
                                                      $rankName = $obj->fetchRankById($rankId);
													
													?>
													<option value="<?php echo $tmp[$r]["instructor_id"];?>" <?php if($tmp[$r]["instructor_id"]==$courseInfo[0]["course_officer"]){?> selected="selected" <?php }?> ><?php echo $tmp[$r]["instructor_name"];?>-&nbsp;<?php echo $rankName[0]["ranks"];?>-&nbsp;<?php echo $tmp[$r]["personalno"];?></option>
													<?php }?>
												
											</select>
										</div>
										
										
										
										<div class="col-md-6 form-group">
										<label for="inputPassword">Course Instructor *</label>
										 <select id="courseInstructor" name="courseInstructor" class="form-control" required>
												<option value="">Select</option>
													<?php for($r=0;$r<count($instruc);$r++){
													 $rankId = $instruc[$r]["instructor_rank"];
                                                      $rankName = $obj->fetchRankById($rankId);
													  ?>
													<option value="<?php echo $instruc[$r]["instructor_id"];?>"  <?php if($instruc[$r]["instructor_id"]==$courseInfo[0]["course_instructor"]){?> selected="selected" <?php }?>><?php echo $instruc[$r]["instructor_name"];?>-&nbsp;<?php echo $rankName[0]["ranks"];?>-&nbsp;<?php echo $instruc[$r]["personalno"];?></option>
													<?php }?>
												
											</select>
										</div>
										
										
										
										 <div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Strength *</label>
                                            <input type="text" class="form-control" id="strength" name="strength" placeholder="Strength" required value="<?php echo $courseInfo[0]["strength"];?>">
                                        </div>
										
							
										
										<div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Start Date *</label>
											<input type="text" class="form-control" id="startdate" name="startdate" value="<?php echo date('d-m-Y', strtotime($courseInfo[0]["start_date"])); ?>" required>
                                        </div>
										
										
										<div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">End Date *</label>
                                            <input type="text" class="form-control" id="enddate" name="enddate" value="<?php echo date('d-m-Y', strtotime($courseInfo[0]["end_date"]));?>" required>
                                        </div>
										
										
										
										<div class="col-md-6 form-group">
										<label for="inputPassword">Faculty Responsible*</label>
										 <select id="faculty" name="faculty" class="form-control" >
												<option value="">Select</option>
												
													<?php for($r=0;$r<count($dep);$r++){?>
													<option value="<?php echo $dep[$r]["dep_id"];?>" <?php if($courseInfo[0]["faculty_id"]==$dep[$r]["dep_id"]) {?> selected="selected" <?php }?>><?php echo $dep[$r]["department_code"]; ?></option>
													<?php }?>
												
											</select>
										</div>
										
										<div style="clear:both;"></div>
										
										
										
										<?php for($h=0;$h<count($subInfo);$h++){?>
										
										
										<span id="span<?php echo $h; ?>">
										
										
										<input type="hidden" class="form-control" id="subjectId<?php echo $h;?>" name="subjectId<?php echo $h;?>" value="<?php echo $subInfo[$h]["sub_id"];?>" style="text-transform:uppercase;" required="required">
										
										
										<div class="col-md-2 form-group">
                                            <label for="exampleInputEmail1">Subject *</label>
                                            <input type="text" class="form-control" id="subject<?php echo $h;?>" name="subject<?php echo $h;?>" value="<?php echo $subInfo[$h]["subject_name"];?>" style="text-transform:uppercase;" required="required">
                                        </div>
										
										
										<div class="col-md-2 form-group">
                                            <label for="exampleInputEmail1">Sessions *</label>
                                            <input type="text" class="form-control" id="session<?php echo $h;?>" name="session<?php echo $h;?>" value="<?php echo $subInfo[$h]["sessions"];?>" onKeyPress="return isNumberKey(event)" required>
                                        </div>
										
										
										<div class="col-md-2 form-group">
                                            <label for="exampleInputEmail1">Phase </label>                                         
										   <select id="phase<?php echo $h;?>" name="phase<?php echo $h;?>" class="form-control" >
												<option value="">Select</option>
												<option value="Phase1" <?php if($subInfo[$h]["phase"]=="Phase1") {?> selected="selected" <?php }?>>Phase1</option>
												<option value="Phase2" <?php if($subInfo[$h]["phase"]=="Phase2") {?> selected="selected" <?php }?>>Phase2</option>	
												
											</select>
                                        </div>
										
										
										<div class="col-md-3 form-group">
                                            <label for="exampleInputEmail1">Stream </label>
                                            <select id="stream<?php echo $h;?>" name="stream<?php echo $h;?>" class="form-control" >
												<option value="">Select</option>
												<option value="A*" <?php if($subInfo[$h]["stream"]=="A*") {?> selected="selected" <?php }?>>A*</option>
												<option value="A" <?php if($subInfo[$h]["stream"]=="A") {?> selected="selected" <?php }?>>A</option>
												<option value="B" <?php if($subInfo[$h]["stream"]=="B") {?> selected="selected" <?php }?>>B</option>	
												<option value="C" <?php if($subInfo[$h]["stream"]=="C") {?> selected="selected" <?php }?>>C</option>
												<option value="D" <?php if($subInfo[$h]["stream"]=="D") {?> selected="selected" <?php }?>>D</option>	
												<option value="E" <?php if($subInfo[$h]["stream"]=="E") {?> selected="selected" <?php }?>>E</option>	
											</select>
                                        </div>
										
										<div class="col-md-2 form-group">
										<?php 
										$subId = $subInfo[$h]["sub_id"];
										$subExist = $obj->fetchsubfromList($subId);
										if(count($subExist)==0)
										{
										?>
										<a href="#" onClick="deleteSubject(<?php echo $subInfo[$h]["sub_id"]; ?>,<?php echo $h; ?>)" ><img class="btn btn-danger" src="img/delete.png"  /></a>
										<?php
										}
										?>
										</div>
										</span>
										<div style="clear:both;"></div>
										<?php }?>
										
								       <input type="hidden" name="counter" id="counter" value="<?php echo $h;?>" />
									   
									   <br />
									   	<div>
                                        <button type="button" name="addButton" class="btn btn-info" onClick="addInput('dynamicInput');" >Add Subject <i class="fa fa-check"></i></button>
                                     </div>
									   
									   
									   
									   
									   
									   
	<script language="javascript">

    function deleteSubject(subject,counter)
	{
	//alert('rrr');
	$.ajax(
                            {
                                url :'ajaxdeleteSubject.php?subId='+subject,
                                type: "POST",
                                async: true,
                                success:function(data) 
                                {		
								//alert(data);
								var spn=document.getElementById('span'+counter);	
								spn.remove();
								
								//$("#deleteAlert").html(data);								
								
									
								//$("#prodCart").html(data);
								//document.getElementById("deletesuccess").innerHTML=data;
								
                               //$("#fadeOut").fadeout('slow');
							 //  document.getElementById("artId").innerHTML=data;
                                }
								
	});
	}

function removeDetails(spanId){ //removes fields	
	var spn=document.getElementById(spanId);	
	spn.remove();
	}	
	
	
var counter = document.getElementById('counter').value;	



//var limit = 3;
function addInput(divName){


    /* if (counter == limit)  {
          alert("You have reached the limit of adding " + counter + " inputs");
     }
     else {*/
          var newdiv = document.createElement('div');
		  
		  var subj = "subject"+counter;
		  var ses = "session"+counter;
		  var pha = "phase"+counter;		  
		  var stre = "stream"+counter;

          newdiv.innerHTML = "<span id='span"+counter+"'><div class='col-md-3 form-group'><label for='exampleInputEmail1'>Subject * </label><input type='text' class='form-control' id="+subj+" name="+subj+" style='text-transform:uppercase;' required></div><div class='col-md-3 form-group'><label for='exampleInputEmail1'>Sessions * </label><input type='text' class='form-control' id="+ses+" name="+ses+" onKeyPress='return isNumberKey(event)' required onChange='return checkSession("+counter+")'></div><div class='col-md-3 form-group'><label for='exampleInputEmail1'>Phase </label><select id="+pha+" name="+pha+" class='form-control'><option value=''>Select</option><option value='Phase1'>Phase1</option><option value='Phase2'>Phase2</option></select></div><div class='col-md-3 form-group'><label for='exampleInputEmail1'>Stream </label><select id="+stre+" name="+stre+" class='form-control'><option value=''>Select</option><option value='A*'>A*</option><option value='A'>A</option><option value='B'>B</option><option value='C'>C</option><option value='D'>D</option><option value='E'>E</option></select></div><img class='btn btn-danger' src='img/delete.png' onclick=removeDetails('span"+counter+"');></img><span>";
		  

          document.getElementById(divName).appendChild(newdiv);	

          counter++;
          document.getElementById('counter').value=counter;
		  
		  
		  

}
$(function() {
    $( "#startdate" ).datepicker({ dateFormat: 'dd-mm-yy' });  
    $( "#enddate" ).datepicker({ dateFormat: 'dd-mm-yy' });  
  });
  
   $("#enddate").change(function () {
			var startDate = document.getElementById("startdate").value;
			var endDate = document.getElementById("enddate").value;
			
			var startDate = startDate.split("-").reverse().join("-");
			
			var endDate = endDate.split("-").reverse().join("-");
			
		
			startDate = new Date(startDate);

			endDate = new Date(endDate);
			
			if (startDate >= endDate) {
			alert("End date should be greater than Start date");
			document.getElementById("enddate").value = "";
			}
			
		});
		
		$("#startdate").change(function () {
			var startDate = document.getElementById("startdate").value;
			var endDate = document.getElementById("enddate").value;
			//alert(Date.parse(startDate));
			
		 	var startDate = startDate.split("-").reverse().join("-");
			
			var endDate = endDate.split("-").reverse().join("-");
			
		
			startDate = new Date(startDate);

			endDate = new Date(endDate);
			
			
			
			if (startDate >= endDate) {
			alert("End date should be greater than Start date");
			document.getElementById("enddate").value = "";
			}
			
		});

</script>
									   
									   
										