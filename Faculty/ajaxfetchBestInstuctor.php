<?php
ob_start();
include("autoload.php");
include("check_session.php");

$us	=	new	Auth();
$rec= $us->getUserInfo($fuserId);

$obj	=	new Faculty();


$startdate = $_GET["startdate"];
$enddate = $_GET["enddate"];
$faculty = $_GET["faculty"];


if($startdate=="" && $enddate=="" && $faculty==""){
$tmplist	=	$obj->fetchBestInstructor();	
}else if($startdate!="" && $enddate!="" && $faculty==""){
$tmplist	=	$obj->fetchBestInstructorWithDates($startdate,$enddate);	
}else if($startdate!="" && $enddate!="" && $faculty!=""){
$tmplist	=	$obj->fetchBestInstructorWithDatesAndFaculty($startdate,$enddate,$faculty);	
}else if($faculty!=""){
	$tmplist	=	$obj->fetchBestInstructorWithFaculty($faculty);
}


$size = count($tmplist);


if(count($tmplist)>0)
{


?>


 <table id="example1" class="table table-bordered table-hover">
										<thead>
                                            <tr>
                                                <th>Sl No.</th>
                                                <th>Instructor</th>
												<th>No Of Sessions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										   <?php 
							                $i	=	0;										
							                while($i < $size) {	
								
							                ?>						    
                                            <tr>
                                                <td><?php echo $i+'1';?></td>
                                                <td><?php  $rankName = $obj->fetchRankById($tmplist[$i]["instructor_rank"]); echo $rankName[0]["ranks"]." ". $tmplist[$i]["instructor_name"];?> </td>												
												<td><?php echo $tmplist[$i]["totSession"];?></td>
										<?php /*?>		<td><?php echo date('d-m-Y', strtotime($tmplist[$i]["start_date"]));?> </td>
									 			<td><?php echo date('d-m-Y', strtotime($tmplist[$i]["end_date"]));?> </td><?php */?>
																
                                        
                                            </tr>
											<?php
											$i++;
											}
											?>
                                            
                                                                                     
                                        </tbody>
													
	 </table>												
													
<?php }?>

		<!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

	<script type="text/javascript">
            $(function() {
                $('#example1').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>
		