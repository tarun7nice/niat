<?php
ob_start();
include("autoload.php");
include("check_session.php");
$us	=	new	Auth();
$rec= $us->getUserInfo($fuserId);

$obj	=	new Faculty();
$ins = $_GET["ins"];
$data = $obj->fetchLeaveHistorybyId($ins);
$size	=	count($data);
// echo json_encode($data);



$leaveData = array();
for ($i = 0; $i < $size; $i++)
{
	$leaveData = array_merge($leaveData, createDateRangeArray($data[$i]["start_date"],$data[$i]["end_date"]));
	
}
$leaveData = array_values(array_unique($leaveData));
$futureDate = date('Y-m-t', strtotime('+6 months'));
$backDate=date('Y-m-01', strtotime('-6 months'));

// echo $futureDate;
// echo $backDate;

$showLeaveData = array();
for ($i = 0; $i < count($leaveData); $i++)
{

	$leaveDate = date("Y-m-d",strtotime($leaveData[$i]));
	if($leaveDate >= $backDate and $leaveDate <= $futureDate){
		array_push($showLeaveData,$leaveData[$i]);
	}

}

$graphData = array();
while($backDate <= $futureDate){
	$backDateYear = date('Y',strtotime($backDate));
	$backDateMonth = date('M',strtotime($backDate));
// 	echo "<br>".$backDateYear.$backDateMonth;
	if(!array_key_exists($backDateMonth." ".$backDateYear,$graphData)){
// 		$graphData[$backDateYear] = array();
		$graphData[$backDateMonth." ".$backDateYear] = 0;
	}
// 	if(!array_key_exists($backDateMonth,$graphData[$backDateYear])){
// 		$graphData[$backDateYear][$backDateMonth] = 0;
// 	}
	$backDate=date('Y-m-d', strtotime($backDate.'+1 months'));
	
}

for ($i = 0; $i < count($showLeaveData); $i++)
{

	$backDateYear = date('Y',strtotime($showLeaveData[$i]));
	$backDateMonth = date('M',strtotime($showLeaveData[$i]));
// 	$graphData[$backDateYear][$backDateMonth] += 1;
	$graphData[$backDateMonth." ".$backDateYear] += 1;

}
$finalData[0]= array_keys($graphData);
$finalData[1]= array_values($graphData);
echo json_encode($finalData);
function createDateRangeArray($strDateFrom,$strDateTo)
{
	// takes two dates formatted as YYYY-MM-DD and creates an
	// inclusive array of the dates between the from and to dates.

	// could test validity of dates here but I'm already doing
	// that in the main script

	$aryRange=array();

	$iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
	$iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

	if ($iDateTo>=$iDateFrom)
	{
		array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
		while ($iDateFrom<$iDateTo)
		{
			$iDateFrom+=86400; // add 24 hours
			array_push($aryRange,date('Y-m-d',$iDateFrom));
		}
	}
	return $aryRange;
}
?>
