<?php
ob_start ();
include ("autoload.php");
include ("check_session.php");

$us = new Auth ();
$rec = $us->getUserInfo ( $fuserId );

$obj = new Faculty ();

$tmp = $obj->fetchRanks (); // fetching Ranks

//inserting Instructor

if(isset($_POST["addButton"])){

	if($obj->addInstructor($_POST)){
		$msg = "Added Successfully !";
		header("Location:addInstructor.php?msg=$msg");
		exit;
	}else{
		$msg1 = $obj->getError();
		header("Location:addInstructor.php?msg1=$msg1");
		exit;
	}

}

                           
// deleting Instructor

if ($_GET ["action"] == "del") {
	$vid = $_GET ["vid"];
	$tt = $obj->deleteInstructor ( $vid );
	header ( "Location:listInstructor.php?msg=Deleted successfully" );
	exit ();
}

// fetching Instructor

$tmplist = $obj->fetchInstructor ();
$size = count ( $tmplist );

// Fetch Faculty

$facultyList = $obj->fetchFacultybyDept ();
$facultyCount = count ( $facultyList );

// Fetch Department

$dep = $obj->getDepartment ();
$depCount = count ( $dep );

$msg = $_GET ["msg"];
$msg1 = $_GET ["msg1"];

$webpageTitle = "Manage Instructor";
?>




<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
        <?php include("top.php"); ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

<!-- Script to fecth subcontractor ends -->
<script type="text/javascript">

function checkPersonalNum(str)
{


if(str=="")
{
document.getElementById("txtpersonalNo").innerHTML="";
return;
}
 if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else { // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
      document.getElementById("txtpersonalNo").innerHTML=xmlhttp.responseText;
	  if(xmlhttp.responseText!="")
	  {
	  	document.getElementById("personalNo").value="";
	  }
    }
	
  }
  xmlhttp.open("GET","checkpersonalnumber.php?q="+str,true);
  xmlhttp.send();
}
</script>

<!-- Script to fecth subcontractor ends -->


</head>
<body class="skin-blue" onLoad="startTime()">
	<?php include("head.php"); ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
		<!-- Left side column. contains the logo and sidebar -->
            <?php include("sidemenu.php"); ?>

            <!-- Right side column. Contains the navbar and content of the page -->
		<aside class="right-side">

			<section class="content-header">
				<div id="txt" class="alert alert-info"></div>

				<ol class="breadcrumb">
					<li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><i class="fa fa-bookmark-o"></i> Masters</li>
					<li class="active">Manage Instructor</li>
				</ol>
				<h1>
					<small>Add Instructor</small>
				</h1>

			</section>
			<!-- Main content -->
			<section class="content">




				<div class="row">
					<!-- left column -->
					<div class="col-md-12">
						<!-- general form elements -->
						<div class="box box-success">
					
							<?php
							if ($msg) {
								?>    
							<div class="alert alert-success alert-dismissable"> <?php echo $msg; ?><button
									type="button" class="close" data-dismiss="alert"
									aria-hidden="true">×</button>
							</div>
							<?php
							}
							?>    
							
							<?php
							if ($msg1) {
								?>    
							<div class="alert alert-danger alert-dismissable"> <?php echo $msg1; ?><button
									type="button" class="close" data-dismiss="alert"
									aria-hidden="true">×</button>
							</div>
							<?php
							}
							?>    
                              
                              
                                <!-- form start -->
							<form name="myform" role="form" method="post" id="myform"
								action="addInstructor.php" enctype="multipart/form-data">
								<div class="col-md-12 box-body">




									<div class="col-md-6 form-group">
										<label for="exampleInputEmail1">Name *</label> <input
											type="text" class="form-control" id="Name" name="Name"
											placeholder="Name" style="text-transform: uppercase;"
											required>
										<!--onKeyPress="return AllowAlphabet(event)" -->
									</div>


									<div class="col-md-6 form-group">
										<label for="inputPassword">Rank *</label>
										<!--<select id="rank" name="rank" class="form-control" >
												<option value="">Select</option>
													<?php for($r=0;$r<count($tmp);$r++){?>
													<option value="<?php echo $tmp[$r]["id"];?>"><?php echo $tmp[$r]["ranks"];?></option>
													<?php }?>
												
											</select>-->
										<input type="text" class="form-control" id="rank" name="rank"
											placeholder="Rank" style="text-transform: uppercase;"
											required>
									</div>



									<div class="col-md-6 form-group">
										<label for="exampleInputEmail1">Grade </label> <select
											id="grade" name="grade" class="form-control">
											<option value="">Select Grade</option>
											<option value="A">A</option>
											<option value="B">B</option>
											<option value="C">C</option>
										</select>
									</div>

									<div class="col-md-6 form-group">
										<label for="exampleInputEmail1">Personal No. *</label><span
											class="badge pull-right bg-green" id="txtpersonalNo"></span>
										<input type="text" class="form-control" id="personalNo"
											name="personalNo"
											onChange="checkPersonalNum(this.value)" required>
									</div>

									<div class="col-md-6 form-group">
										<label for="inputPassword">Faculty </label> <select
											id="faculty" name="faculty" class="form-control">
											<option value="">Select</option>
												
													<?php for($r=0;$r<count($dep);$r++){?>
													<option value="<?php echo $dep[$r]["dep_id"];?>"><?php echo $dep[$r]["department_code"]; ?></option>
													<?php }?>
												
											</select>
									</div>

									<div class="col-md-6 form-group">
										<label for="inputPassword">Aircarft Speciality</label> <select
											id="aircraft" name="aircraft" class="form-control">
											<option value="">Select</option>
											<option value="Tu-142M">Tu-142M</option>
											<option value="IL-38SD">IL-38SD</option>
											<option value="Chetak">Chetak</option>
											<option value="Km-31">Km-31</option>
											<option value="Kv-28">Kv-28</option>
											<option value="Ka-25">Ka-25</option>
											<option value="Mig-29K">Mig-29K</option>
											<option value="SeaKing">SeaKing</option>
											<option value="UH-3H">UH-3H</option>
											<option value="P-8I">P-8I</option>
											<option value="SeaHarrier">Sea Harrier</option>
											<option value="UAV">UAV</option>
											<option value="ALH">ALH</option>
											<option value="AJT">AJT</option>
											<option value="DORNIER">DORNIER</option>
										</select>
									</div>

									<div class="col-md-6 form-group">
										<label for="exampleInputEmail1">Mobile </label> <input
											type="text" class="form-control" id="mobile" name="mobile"
											onKeyPress="return isNumberKey(event)" onChange="Validate()">
									</div>

									<div class="col-md-6 form-group">
										<label for="exampleInputEmail1">Address </label>
										<textarea class="form-control" name="address" id="address"></textarea>
									</div>


								</div>
								<!-- /.box-body -->


								<div class="box-footer">
									<button type="submit" name="addButton" class="btn btn-success">
										Save <i class="fa fa-check"></i>
									</button>
								</div>
							</form>
						</div>
						<!-- /.box -->

					</div>



				</div>

			</section>
			<!-- /.content -->

			<!-- data table--->




			<!--- /.data table -->

		</aside>
		<!-- /.right-side -->
	</div>
	<!-- ./wrapper -->

	<!-- add new calendar event modal -->


	<!-- jQuery 2.0.2 -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery UI 1.10.3 -->
	<script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
	<!-- DATA TABES SCRIPT -->
	<script src="js/plugins/datatables/jquery.dataTables.js"
		type="text/javascript"></script>
	<script src="js/plugins/datatables/dataTables.bootstrap.js"
		type="text/javascript"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
	<script src="js/AdminLTE/app.js" type="text/javascript"></script>

<!--
	<link rel="stylesheet"
		href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">

	<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
-->
<link rel="stylesheet" href="../jquery-ui-1.11.2.custom/jquery-ui.css">

<script src="../jquery-ui-1.11.2.custom/jquery-ui.js"></script>	


	<!-- <script type="text/javascript">
        CKEDITOR.replace(txtaddress);
        </script> -->


	<script type="text/javascript">
            $(function() {
                $('#example1').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>


	<script type="text/javascript">
		
		function askDelete(){
			if(confirm("Do you want to delete this item ? click OK to continue, CANCEL to exit")){
				return true;
			}else{
				return false;
			}
		}

		
		</script>


	<script>
		  
  $(function() {
    $( "#grade" ).datepicker({ dateFormat: 'dd-mm-yy' });  
  });


    
	$(document).ready(function(){
		  var rank = document.getElementById('rank').value;
			$("#rank").autocomplete({
				
				source:'getRankNames.php?rank='+rank,
				minLength:1
			});
		});
		
	
    function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
	{
		alert("Please enter valid number");
        return false;
	}
	else
	{
    return true;
	}
}


// function isAlphaNumeric(e){ // Alphanumeric only
//             var k;
//             document.all ? k=e.keycode : k=e.which;
//             return((k>47 && k<58)||(k>64 && k<91)||(k>96 && k<123)||k==0);
//          }
	
	
	 function Validate() {
       var mobile = document.getElementById("mobile").value;
       var pattern = /^\d{10}$/;
       if (pattern.test(mobile)) {
          // alert("Your mobile number : " + mobile);
           return true;
       }
       alert("It is not valid mobile number.input 10 digits number!");
       return false;
   }
   
   $(function(){
 
$('#rank').keyup(function()
{
var yourInput = $(this).val();
re = /[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/0-9]/gi;
var isSplChar = re.test(yourInput);
if(isSplChar)
{
var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/0-9]/gi, '');
$(this).val(no_spl_char);
}
});
 
});

   $(function(){
 
$('#Name').keyup(function()
{
var yourInput = $(this).val();
re = /[`~!@#$%^&*()_|+\-=?;:'",<>\{\}\[\]\\\/]/gi;
var isSplChar = re.test(yourInput);
if(isSplChar)
{
var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",<>\{\}\[\]\\\/]/gi, '');
$(this).val(no_spl_char);
}
});
 
});
 
$(document).ready(function () {
 $("#Name").keydown(function (e) {
       
        if (e.keyCode) {
            keyEntry = e.keyCode;
        } else if (e.which) {
            keyEntry = e.which;
        }
 if (((keyEntry >= '65') && (keyEntry <= '90')) || ((keyEntry >= '97') && (keyEntry <= '122')) 
		 || (keyEntry == '46') || (keyEntry == '32') || keyEntry == '45'
			 || (keyEntry == '8') || keyEntry == '9' || (keyEntry == '37') || keyEntry == '39')
    return true;
 else
{
   alert('Please Enter Only Character values.');
   return false;
     }

});

 $("#personalNo").keydown(function (e) {
     
     if (e.keyCode) {
         keyEntry = e.keyCode;
     } else if (e.which) {
         keyEntry = e.which;
     }
if (((keyEntry >= '48') && (keyEntry <= '57')) ||((keyEntry >= '65') && (keyEntry <= '90')) || ((keyEntry >= '97') && (keyEntry <= '122')) 
		 || (keyEntry == '46') || (keyEntry == '32') || keyEntry == '45'
			 || (keyEntry == '8') || keyEntry == '9' || (keyEntry == '37') || keyEntry == '39')
 return true;
else
{
// alert('Please Enter Only Character values.');
return false;
  }

});

 $("#personalNo").keypress(function (e) {
     
     if (e.keyCode) {
         keyEntry = e.keyCode;
     } else if (e.which) {
         keyEntry = e.which;
     }
if (((keyEntry >= '48') && (keyEntry <= '57')) ||((keyEntry >= '65') && (keyEntry <= '90')) || ((keyEntry >= '97') && (keyEntry <= '122')) 
		 || (keyEntry == '46') || (keyEntry == '32') || keyEntry == '45'
			 || (keyEntry == '8') || keyEntry == '9' || (keyEntry == '37') || keyEntry == '39')
 return true;
else
{
// alert('Please Enter Only Character values.');
return false;
  }

});

 $( "#myform" ).submit(function( event ) {
	 var correct = true;
	 if( !$("#Name").val() ) {
		 $("#Name").addClass('warning');
		 correct = false;
   }
	 else{
		 $("#Name").removeClass('warning');
	 }
	 if( !$("#rank").val() ) {
		 $("#rank").addClass('warning');
		 correct = false;
   }
	 else{
		 $("#rank").removeClass('warning');
	 }
	 if( !$("#personalNo").val() ) {
		 $("#personalNo").addClass('warning');
		 correct = false;
   }
	 else{
		 $("#personalNo").removeClass('warning');
	 }
	   if(!correct)
	 event.preventDefault();
	 });

}); 
 
  </script>

	<script>
function startTime() {
    var today=new Date();
    var h=today.getHours();
    var m=today.getMinutes();
    var s=today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt').innerHTML = h+":"+m+":"+s;
    var t = setTimeout(function(){startTime()},500);
}

function checkTime(i) {
    if (i<10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}

function fetchData()
{
	var airc = document.getElementById('aircraftfilter').value;
	$.ajax(
                            {
                                url :'ajaxfetchInslist.php?ac='+airc,
                                type: "POST",
                                async: true,
                                success:function(data) 
                                {		
								//alert(data);	
                               $(".fetchData").html(data);
							 //  document.getElementById("artId").innerHTML=data;
                                }
                  
            });
			
}
</script>


</body>
</html>
