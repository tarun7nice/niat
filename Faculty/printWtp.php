<?php
ob_start();
include("autoload.php");
include("check_session.php");

$us	=	new	Auth();
$rec= $us->getUserInfo($fuserId);


$obj	=	new Faculty();

$msg	=$_GET["msg"];
$msg1	=$_GET["msg1"];
	
$webpageTitle	=	"Print WTP";
?>




<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php include("top.php"); ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
	

		
    </head>
    <body class="skin-blue" onLoad="startTime()">
	<?php include("head.php"); ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <?php include("sidemenu.php"); ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
			
			<section class="content-header">
			<div id="txt" class="alert alert-info"></div>
			
			 <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li><li><i class="fa fa-bookmark-o"></i> Masters</li>
                        <li class="active">Manage WTP</li>
                    </ol>
                    <h1><small>Print WTP</small></h1>
                   
                </section>
               <!-- Main content -->
                <section class="content">   
							
							<div class="box">
                                <div class="box-header">     
							                            
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                <div class="row">
                                 <div class="col-md-6 form-group">
                                Please Select any day of a week for which you want to print WTP.<br>
                                <span style="font-style: italics; color:red">Format: yyyy-mm-dd</span>
                                </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 form-group">
											<label for="startdate">Select Date *</label> <input
												type="text" class="form-control" id="weekdate"
												name="weekdate" required>
												<div style="margin-top: 5px;">
												<button type="button" onclick="generate_report()" name="addButton" class="btn btn-success">Generate Report<i class="fa fa-check"></i></button>
												</div>
												</div>
									 
												</div>
                                </div><!-- /.box-body -->
                            </div>
							
							
							
							
							
							
							
							

                </section><!-- /.content -->
				
				<!-- data table--->
				
				
				
				
				<!--- /.data table -->
				
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        <script src="js/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
		<!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>
		
			   
		
	<!-- <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"> -->

	<!-- <script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>	 -->
	<link rel="stylesheet" href="../jquery-ui-1.11.2.custom/jquery-ui.css">

	<script src="../jquery-ui-1.11.2.custom/jquery-ui.js"></script>

	
		  <script>
		  
  $(function() {
    $( "#weekdate" ).datepicker({ dateFormat: 'yy-mm-dd' });  
  });
  
  var isValidDate = function (value, userFormat) {
	  var

	  userFormat = userFormat || 'yyyy-mm-dd', // default format

	  delimiter = /[^mdy]/.exec(userFormat)[0],
	  theFormat = userFormat.split(delimiter),
	  theDate = value.split(delimiter),

	  isDate = function (date, format) {
	    var m, d, y
	    for (var i = 0, len = format.length; i < len; i++) {
	      if (/m/.test(format[i])) m = date[i]
	      if (/d/.test(format[i])) d = date[i]
	      if (/y/.test(format[i])) y = date[i]
	    }
	    return (
	      m > 0 && m < 13 &&
	      y && y.length === 4 &&
	      d > 0 && d <= (new Date(y, m, 0)).getDate()
	    )
	  }

	  return isDate(theDate, theFormat)

	}

  function generate_report(){
	  var weekDate = $("#weekdate").val();
	  console.log(weekDate);
   if(isValidDate(weekDate)){
	   var url = "wtpReport.php?week_date="+weekDate
	   var win = window.open(url, '_blank');
	   win.focus();
   }
   else
	   {console.log("invalid date");
   		alert("Please Select Valid Date");
	   }
  }

 
  </script>
		

	<script>
function startTime() {
    var today=new Date();
    var h=today.getHours();
    var m=today.getMinutes();
    var s=today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt').innerHTML = h+":"+m+":"+s;
    var t = setTimeout(function(){startTime()},500);
}

function checkTime(i) {
    if (i<10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}
</script>	

<script language="javascript">
function buttonAction(func,obj){
//document.getElementById('pro_Menu').style.display = "block";
return func(obj);
}

function hideURL(obj){

return hs.htmlExpand(obj, { objectType: 'iframe', contentId: 'controlbar5' , align: 'center'} );
}
</script>



<script type="text/javascript" src="javascript/highslide-with-html.js"></script>
<link rel="stylesheet" type="text/css" href="javascript/highslide.css" />
<script type="text/javascript">
    hs.graphicsDir = 'javascript/graphics/';
    hs.outlineType = 'rounded-white';
	if(location.hash) {
	var tabid = location.hash.substr(1);
	$('a[href="#'+tabid+'"]').click();
	} 
</script>
        
    </body>
</html>