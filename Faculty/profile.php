<?php 
ob_start();
include("autoload.php");
include("check_session.php");


$us	=	new	Auth();
$rec= $us->getUserInfo($fuserId);

$register	=	new Session();
			

if(isset($_POST["submitAccount"])){
	$or	=	new Auth();
	$or	->	setUserId($fuserId);	
	$or	->	updateStaffAccount($_POST,$fuserId);
	$msg1	=	"";
	if($msg1	=	$or->getError()){
	}else{
		 $msg1	=	"Account details updated successfully !";
		 session_start();
		 $register->setSession("USER_LOGINED_NAME",$_POST["txtname"]);
	}
	
	header("Location:profile.php?msg1=$msg1");
	exit;

}


 // Update password for logged user 

if(isset($_POST["submitPassword"])){
		
	$or	=	new Auth();
	$or->updatePassword($_POST,$fuserId);
	$msg2	=	"";
	if($msg2	=	$or->getError()){
	}else{
		 $msg2	=	"Password Updated Successfully";
	}
	header("Location:profile.php?msg2=$msg2");
	exit;
}


$msg1=	$_GET["msg1"];
$msg2=	$_GET["msg2"];


$webpageTitle	=	"Dashboard";

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php include("top.php"); ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
	<?php include("head.php"); ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <?php include("sidemenu.php"); ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                
                <!-- Main content -->
                <section class="content">   
				
				<?php
				if($msg1)
				{
				?>    
				<label class="control-label" for="inputSuccess"> <?php echo $msg1; ?></label>
				<?php
				}
				else
				if($msg2)
				{
				?>  
				<label class="control-label" for="inputSuccess"> <?php echo $msg2; ?></label>
				<?php
				}
				?>       

                  <div class="row">
                        <!-- left column -->
						
						
						
						<div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-success">
                              
                                <!-- form start -->
                                <form name="myform" role="form" method="post" action="profile.php" >
                                    <div class="col-md-12 box-body">
										
										 <div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Name *</label>
                                            <input type="text" class="form-control" id="txtUsername" name="txtUsername" value="<?php echo $rec[0]["staff_name"];?>"  required>
                                        </div>
										                                        
                                    </div><!-- /.box-body -->
									
									
                                    <div class="box-footer">
                                        <button type="submit" name="submitAccount" class="btn btn-success">Save <i class="fa fa-check"></i></button>
                                    </div>
                                </form>
                            </div><!-- /.box -->
							
							</div>
							
							
							
							
							<div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-success">
                              
                                <!-- form start -->
                                <form name="myform1" role="form" method="post" action="profile.php" >
                                    <div class="col-md-12 box-body">
										
										 <div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Old Password *</label>
                                            <input type="password" class="form-control" id="txtCurrentPassword" name="txtCurrentPassword"  required>
                                        </div>
										
										
										<div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">New Password *</label>
                                            <input type="password" class="form-control" id="txtNewPassword" name="txtNewPassword"  required>
                                        </div>
										                                        
                                    </div><!-- /.box-body -->
									
									
									
									
									
									
									
                                    <div class="box-footer">
                                        <button type="submit" name="submitPassword" class="btn btn-success">Save <i class="fa fa-check"></i></button>
                                    </div>
                                </form>
                            </div><!-- /.box -->
							
							</div>
                        
							
							
							</div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        <script src="js/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>
        
    </body>
</html>