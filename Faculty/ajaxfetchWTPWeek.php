<?php
ob_start();
include("autoload.php");
include("check_session.php");

$us	=	new	Auth();
$rec= $us->getUserInfo($fuserId);
$depId = $rec[0]["dep_id"];

$obj	=	new Faculty();


$fetchweek = $_GET["week"];
// echo $fetchweek;
$date = new DateTime ();
$week = $date->format ( "W" );
$month = $date->format ( "m" );
$year = $date->format ( "Y" );
if($month == 12 && $week == 1){
	$year = $year + 1;}
if($fetchweek == "next")
{	
// 	echo "i m here";
$weekStartDate =  date('Y-m-d',strtotime($year.'W'.$week . ' + 7 days'));
$weekEndDate =  date('Y-m-d',strtotime($year.'W'.$week . ' + 12 days'));
}
else {
	$weekStartDate =  date('Y-m-d',strtotime($year.'W'.$week));
	$weekEndDate =  date('Y-m-d',strtotime($year.'W'.$week . ' + 5 days'));	
}
// echo $weekStartDate;
// echo "<br>";
// echo $weekEndDate;

$tmplist =	$obj->fetchMIPwithWTP($weekStartDate, $weekEndDate,$depId);
// echo json_encode($tmplist);
$size	=	count($tmplist);


$size = count($tmplist);


if(count($tmplist)>0)
{


?>
<div style="text-align:center;font-weight:bold;font-size:20px">
<?php 
echo date('d-M-Y',strtotime($weekStartDate));
echo " TO ";
echo date('d-M-Y',strtotime($weekEndDate));
?>
</div>
 <table id="example1" class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Sl No.</th>
                                                <th>Course Name</th>
                                                <th>Course No</th>
                                                <th>Faculty</th>
												<th>Course Start Date</th>
												<th>Course End Date</th>	
												<th>Weeks</th>									
												<th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										   <?php 
							                $i	=	0;										
							                while($i < $size) {	
											//$facultyId = $tmplist[$i]["faculty_id"]	;
										//	$dep = $obj->fetchDepid($facultyId);
										//	$depId = $dep[0]["dep_id"];
										// 	$courseDet = $obj->fetchcourseById($tmplist[$i]["course_id"]);
											
											//$facultyName = $obj->fetchFacultybyId($facultyId);
							                ?>						    
                                            <tr style="color:#ffffff;background-color:
                                            <?php if (empty($tmplist[$i]["wtp_id"])) echo "#DA2C43"; else echo "#41924B";?>
                                            ">
                                                <td><?php echo $i+'1';?></td>
                                                <td><?php echo $tmplist[$i]["course_name"];?> </td>	
                                                <td><?php echo $tmplist[$i]["course_no"];?> </td>
                                                <td><?php echo $tmplist[$i]["department_code"];?> </td>		
												<td><?php echo date('d-m-Y', strtotime($tmplist[$i]["start_date"]));?> </td>
									 			<td><?php echo date('d-m-Y', strtotime($tmplist[$i]["end_date"]));?> </td>
									 			<td><?php echo $tmplist[$i]["current_week"]."/",$tmplist[$i]["full_week"];?> </td>
												<?php if (empty($tmplist[$i]["wtp_id"])) {?>
												 <td> 
												 <a href="addWTPCourse.php?course_id=<?php echo $tmplist[$i]["course_id"];?>
												 &date=<?php echo date('Y-m-d',strtotime($weekStartDate));?>"
												 class="btn btn-info">Add WTP <i class="fa fa-edit"></i></a>&nbsp;</td>
												<?php } else {?>
                                                <td><a target="_top" onClick="return hideURL(this);" href="viewWTP.php?id=<?php echo $tmplist[$i]["wtp_id"];?>" class="btn btn-primary">View <i class="fa fa-eye"></i></a>&nbsp;
                                                
                                                <a href="editWTP.php?id=<?php echo $tmplist[$i]["wtp_id"];?>" class="btn btn-info">Edit <i class="fa fa-edit"></i></a>&nbsp;
												
												<a href="listWtp.php?id=<?php echo $tmplist[$i]["wtp_id"];?>&action=del" onClick="return askDelete();" class="btn btn-danger">Delete <i class="fa fa-trash-o"></i></a></td>
                                            </tr>
											<?php
							                }
											$i++;
											}
											?>
                                            
                                                                                     
                                        </tbody>
                                        
                                    </table>												
													
<?php }?>

		<!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

	<script type="text/javascript">
            $(function() {
                $('#example1').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>
		