<?php
ob_start();
include("autoload.php");
include("check_session.php");

$us	=	new	Auth();
$rec= $us->getUserInfo($fuserId);

$obj	=	new Faculty();
$course = $_GET["course"];

$courseList = $obj->fetchCourseDetailedInfo($course);


$size = count($courseList);


$officer = $obj->fetchInstructorByID($courseList[0]["course_officer"]);

$rankId = $officer[0]["instructor_rank"];
$rankName = $obj->fetchRankById($rankId);



$instructor = $obj->fetchInstructorByID($courseList[0]["course_instructor"]);

$instrucrankId = $instructor[0]["instructor_rank"];
$instrucrankName = $obj->fetchRankById($instrucrankId);

$strength = $courseList[0]["strength"];

$classroomDet = $obj->fetchClassroombyStrength($strength);
$roomCount = count($classroomDet);


if(count($courseList)>0)
{

?>                                       
                                         <div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Course No  </label>
                                            <input type="text" class="form-control" id="courseNo" name="courseNo" readonly="true" value="<?php echo $courseList[0]["course_no"];?> ">
                                        </div>
										
				
				
		                                  <div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Course Officer </label>
                                            
											
											 <input type="text" class="form-control" id="courseOfficer" name="courseOfficer" readonly="true" value="<?php echo $officer[0]["instructor_name"];?>-&nbsp;<?php echo $rankName[0]["ranks"];?>-&nbsp;<?php echo $officer[0]["personalno"];?> ">
											
                                        </div>
										
										
										
										  <div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Course Instructor *</label>
                                            
											
											 <input type="text" class="form-control" id="courseInstructor" name="courseInstructor" readonly="true" value="<?php echo $instructor[0]["instructor_name"];?>-&nbsp;<?php echo $instrucrankName[0]["ranks"];?>-&nbsp;<?php echo $instructor[0]["personalno"];?> ">
											
                                        </div>
										
										
										
										
										  
                                         <div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Strength  </label>
                                            <input type="text" class="form-control" id="strength" name="strength" readonly="true" value="<?php echo $courseList[0]["strength"];?> ">
                                        </div>
                                        
										
										
										<input type="hidden" name="coName" id="coName" value="<?php echo $courseList[0]["course_name"];?>"  />
										
										
										
										
<?php


 $ddate = $courseList[0]["start_date"];
$date = new DateTime($ddate);
$tdate = date("Y-m-d");
$toddate = new DateTime($tdate);
$diff = $toddate->diff($date)->format("%a");
// $week = $date->format("W");
//echo "Weeknummer: $week";	



										
// $tdate = date("Y-m-d");
// $toddate = new DateTime($tdate);
// $nowweek = $toddate->format("W");
//echo "thisWeeknummer: $nowweek";												
				
$runWeek = 	ceil($diff/7);
		
			 ?>
		
		        
		                                 <div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Running Week  </label>
                                            <input type="text" class="form-control" id="week" name="week" readonly="true" 
                                            value="<?php //echo $nowweek - $week;
			 										echo $runWeek;
			 								?> ">
                                        </div>
                                        
                                        <div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Start Date *</label>
                                            <input type="text" class="form-control" id="startdate" name="startdate" required onChange="fetchendDate(this.value);">
                                        </div>
										
										
										<div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">End Date *</label>
                                            <input type="text" class="form-control" id="enddate" name="enddate" required readonly>
                                        </div>
										
		
		
        
        <div class="col-md-6 form-group">
            <label for="exampleInputEmail1">Class Room * </label>
            <select id="classroom" name="classroom" class="form-control" disabled="disabled" required onchange="checkClasroom()">
                <option value="">Select</option>
                    <?php for($c=0;$c<count($classroomDet);$c++){?>
                    <option value="<?php echo $classroomDet[$c]["class_room_id"];?>"><?php echo $classroomDet[$c]["class_room"];?>-&nbsp;<?php echo $classroomDet[$c]["strength"];?></option>
                    <?php }?>												
            </select>
        </div>
		
        
        <div class="col-md-6 form-group">
        <label for="exampleInputEmail1">Facility  </label>
        <input type="text" class="form-control" id="facility" name="facility" readonly="true" >
    </div>
		
		
		
		
		
				
													
<?php }?>

<script type="text/javascript">

  $(function() {
	    $( "#startdate" ).datepicker({ 
		       
	        firstDay: 1,
	        onSelect: function(date){
	         var d = new Date(date);
	            var index = d.getDay();
	            if(index == 0) {
	             d.setDate(d.getDate() - 6);
	            }
	            else if(index == 1) {
	             d.setDate(d.getDate());
	            }
	            else if(index != 1 && index > 0) {
	              d.setDate(d.getDate() - (index - 1));
	            }
	            var value = ("0"+d.getDate()).slice(-2)+"-"+("0"+(parseInt(d.getMonth())+1)).slice(-2)+"-"+d.getFullYear();
	            $(this).val(value);
	            fetchendDate(value);
	            }


	         }); 
//     $( "#enddate" ).datepicker({ dateFormat: 'dd-mm-yy' }); 
	
  });

function fetchendDate(stdate){
		
			var stdate = stdate.split("-").reverse().join("-");
			stdate = new Date(stdate);
			
			stdate.setDate(stdate.getDate() + 5);
		   
		   eddate = (stdate.getDate() + '-' + (stdate.getMonth() + 1) + '-' +  stdate.getFullYear());	
		   
		   document.getElementById('enddate').value=eddate;
		   document.getElementById('classroom').disabled=false;
		
		}

</script>


