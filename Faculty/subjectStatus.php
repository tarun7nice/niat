<?php
ob_start();
include("autoload.php");
include("check_session_public.php");

$us	=	new	Auth();
$rec= $us->getUserInfo($fuserId);


$obj	=	new Faculty();

$tmp = $obj->fetchRanks(); //fetching Ranks


 

	
// Fecthing Assigned course

$assignC = $obj->fetchallAssignCourse();
$assignCount = count($assignC);


//fetching Instructor 
	
$tmplist	=	$obj->fetchInstructor();	
$size	=	count($tmplist);

// Fetch Faculty

$facultyList = $obj->fetchFacultybyDept();
$facultyCount = count($facultyList);

// Fetch Department

$dep = $obj->getDepartment();
$depCount = count($dep);


$msg	=$_GET["msg"];
$msg1	=$_GET["msg1"];
	
$webpageTitle	=	"WTP Listing";
?>




<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
		
        <?php 
		
		include("top.php"); 
		
		?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
         .ui-progressbar {
position: relative;
}
         .progress-label {
position: absolute;
left: 50%;
top: 4px;
text-shadow: 1px 1px 0 #fff;
}
        </style>

    </head>
    <body class="skin-blue"  onLoad="startTime()">
	<?php 
	if(!empty($fuserId)){
	include("head.php"); 
	}
	?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <?php 
			if(!empty($fuserId)){
			include("sidemenu.php"); 
			}
			?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
			
			<section class="content-header">
			<div id="txt" class="alert alert-info"></div>
                    <h1><small>Subject Status</small></h1>
                   
                </section>
               <!-- Main content -->
                <section class="content">    
				
				
				          

                  <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-success">
					
							<?php
							if($msg)
							{
							?>    
							<div class="alert alert-success alert-dismissable"> <?php echo $msg; ?><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
							<?php
							}
							?>    
							
							<?php
							if($msg1)
							{
							?>    
							<div class="alert alert-danger alert-dismissable"> <?php echo $msg1; ?><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
							<?php
							}
							?>    
                              
                              
                                <!-- form start -->
                                <form name="myform" role="form" method="post" action="subjectStatus.php" id="myform">
                                    <div class="col-md-12 box-body">
									
										
																		
									
									
								        <div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Course </label>
											<select id="Name" name="Name" class="form-control" onChange="fetchCourseNo();" >
											<option value="">---Select Course---</option>
											<?php
											$course = $obj->fetchMIP();
											$courseSize = count($course);
											$j=0;
											while($j<$courseSize)
											{
											?>
											<option value="<?php echo $course[$j]["course_id"]; ?>"><?php echo $course[$j]["course_name"]; ?></option>
											<?php
											$j++;
											}
											?>
											</select>
										</div>	
										

										
										<div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Course No</label>
											<input type="text" class="form-control" id="courseNo" name="courseNo" style="text-transform:uppercase;" placeholder="Course No"  readonly="true">
											
										</div>	
										
										
										
										
										 <div class="col-md-6 form-group" style="margin-top:24px;">
										 <button type="button" class="btn btn-success" onClick="fetchSubjectStatusCourse();">View</button>
										</div>
                                        
                                      </div><!-- /.box-body -->
									
									
                                    <!--<div class="box-footer">
                                        <button type="submit" name="addButton" class="btn btn-success">Save <i class="fa fa-check"></i></button>
                                    </div>-->
                                </form>
                            </div><!-- /.box -->
							
							</div>
							
							
							
							</div>
						<div id="listAssigned">
							
							<div class="box" >
                                <div class="box-header">     
							                            
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive" id="ex1">
                                    
                                </div><!-- /.box-body -->
                                	<div id="canvas-holder">
				
				</div>
                            </div>
							
							</div>
							
				</section><!-- /.content -->
				
				
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        <script src="js/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
		<!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>
		
			   
	<!-- <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"> -->

	<!-- <script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>	 -->
	<link rel="stylesheet" href="../jquery-ui-1.11.2.custom/jquery-ui.css">

	<script src="../jquery-ui-1.11.2.custom/jquery-ui.js"></script>
<!-- Chart.js -->
	<script src="js/Chart.js"></script>


	   <!-- <script type="text/javascript">
        CKEDITOR.replace(txtaddress);
        </script> -->
	
		
			<script type="text/javascript">
            $(function() {
                $('#example1').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
            
        </script>
		
		
		<script type="text/javascript">

		
		/*$(document).keyup(function(){
		  var name = document.getElementById('Name').value;
			$("#Name").autocomplete({
				source:'getCoursesNames.php?name='+name,
				minLength:1
			});
		});*/
		
		
		
		
		
		function fetchCourseNo(){
		
		 var name = document.getElementById('Name').value;
		
		 $.ajax(
					{
						url :'ajaxfetchCourseNoTraineeWTP.php?name='+name,
						type: "POST",
						async: false,
						success:function(data) 
						{
						$("#courseNo").val(data);							
							
						}
		  
		});
			
		}
		
		
		function fetchSubjectStatusCourse(){
		
		 var name = document.getElementById('Name').value;
		
		 $.ajax(
					{
						url :'ajaxSubjectStatusCourse.php?name='+name,
						type: "POST",
						async: false,
						success:function(data) 
						{
						$("#ex1").html(data);	
						$("div.progressbar").each (function () {
						    var element = this;

						   $(element).progressbar({
						        value: parseInt($(element).attr("data-value"))
						    });
						});					
							
						}
		  
		});
			
		}

		</script>
		
		
		  
		
<script>
function startTime() {
    var today=new Date();
    var h=today.getHours();
    var m=today.getMinutes();
    var s=today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt').innerHTML = h+":"+m+":"+s;
    var t = setTimeout(function(){startTime()},500);
}

function checkTime(i) {
    if (i<10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}
</script>	
		
        
    </body>
</html>