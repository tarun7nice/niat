<?php
ob_start();
include("autoload.php");
include("check_session.php");

$us	=	new	Auth();
$rec= $us->getUserInfo($fuserId);


$obj	=	new Faculty();



//fetching Best Instructor 
	
$tmplist	=	$obj->fetchBestInstructor();	
$size	=	count($tmplist);



// Fetch Department

$dep = $obj->getDepartment();
$depCount = count($dep);


$msg	=$_GET["msg"];
$msg1	=$_GET["msg1"];
	
$webpageTitle	=	"Best Instructor";
?>




<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php include("top.php"); ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
	

		
    </head>
    <body class="skin-blue" onLoad="startTime()">
	<?php include("head.php"); ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <?php include("sidemenu.php"); ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
			
			<section class="content-header">
			<div id="txt" class="alert alert-info"></div>
			
			 <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li><li><i class="fa fa-bookmark-o"></i> Masters</li>
                        <li class="active">Best Instructor</li>
                    </ol>
                    <h1><small>Best Instructor</small></h1>
                   
                </section>
               <!-- Main content -->
                <section class="content">   
							
							<div class="box">
                                <div class="box-header">     
								
								
								         <div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Start Date *</label>
                                            <input type="text" class="form-control" id="startdate" name="startdate" required>
                                        </div>
										
										
										<div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">End Date *</label>
                                            <input type="text" class="form-control" id="enddate" name="enddate" required onChange="fetchBestInstructor();">
                                        </div>
										
										
										<div class="col-md-6 form-group">
										<label for="inputPassword">Faculty Responsible</label>
										 <select id="faculty" name="faculty" class="form-control" onChange="fetchBestInstructor();">
												<option value="">Select</option>
												
													<?php for($r=0;$r<count($dep);$r++){?>
													<option value="<?php echo $dep[$r]["dep_id"];?>"><?php echo $dep[$r]["department_code"]; ?></option>
													<?php }?>
												
											</select>
										</div>


							                            
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive" id="exm1">
                                   
                                        
                                        
                                        
                                   
                                </div><!-- /.box-body -->
                            </div>
							
							
							
							
							
							
							
							

                </section><!-- /.content -->
				
				<!-- data table--->
				
				
				
				
				<!--- /.data table -->
				
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        <script src="js/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
		<!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>
		
			   
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">

<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>	


	   <!-- <script type="text/javascript">
        CKEDITOR.replace(txtaddress);
        </script> -->
	
		
<!--			<script type="text/javascript">
            $(function() {
                $('#example1').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>
		-->
		
		<script type="text/javascript">

		
		
   		 $("#enddate").change(function () {
			var startDate = document.getElementById("startdate").value;
			var endDate = document.getElementById("enddate").value;
			
			var startDate = startDate.split("-").reverse().join("-");
			
			var endDate = endDate.split("-").reverse().join("-");
			
		
			startDate = new Date(startDate);

			endDate = new Date(endDate);
			
			if (startDate >= endDate) {
			alert("End date should be greater than Start date");
			document.getElementById("enddate").value = "";
			}
			
		});
		
		$("#startdate").change(function () {
			var startDate = document.getElementById("startdate").value;
			var endDate = document.getElementById("enddate").value;
			//alert(Date.parse(startDate));
			
		 	var startDate = startDate.split("-").reverse().join("-");
			
			var endDate = endDate.split("-").reverse().join("-");
			
		
			startDate = new Date(startDate);

			endDate = new Date(endDate);
			
			
			
			if (startDate >= endDate) {
			alert("End date should be greater than Start date");
			document.getElementById("enddate").value = "";
			}
			
		});	
		
		
		
		
	   function fetchBestInstructor(){
		
		var startDate = document.getElementById("startdate").value;
			var endDate = document.getElementById("enddate").value;
			
				var faculty = document.getElementById("faculty").value;
				
			//alert(Date.parse(startDate));
			
		 	var startDate = startDate.split("-").reverse().join("-");
			
			var endDate = endDate.split("-").reverse().join("-");
			
		 $.ajax(
					{
						url :'ajaxfetchBestInstuctor.php?startdate='+startDate+'&enddate='+endDate+'&faculty='+faculty,
						type: "POST",
						async: false,
						success:function(data) 
						{
						
						$("#exm1").html(data);		
						
							
						}
		  
		});
			
		}		
		

  $(function() {
    $( "#startdate" ).datepicker({ dateFormat: 'yy-mm-dd' }); 
	 $( "#enddate" ).datepicker({ dateFormat: 'yy-mm-dd' });  
  });


		fetchBestInstructor();
		
 
  </script>
		

	<script>
function startTime() {
    var today=new Date();
    var h=today.getHours();
    var m=today.getMinutes();
    var s=today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt').innerHTML = h+":"+m+":"+s;
    var t = setTimeout(function(){startTime()},500);
}

function checkTime(i) {
    if (i<10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}
</script>	
        
    </body>
</html>