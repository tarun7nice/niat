<?php
ob_start ();
include ("autoload.php");
include ("check_session.php");
$us = new Auth ();
$rec = $us->getUserInfo ( $fuserId );
$obj = new Faculty ();

$week_date = $_GET ["week_date"];
$date = new DateTime ( $week_date );
// echo $date->format ( "Y-m-d" ) ;



// echo json_encode ( $wtpList );

$week = $date->format ( "W" );
$month = $date->format ( "m" );
$year = $date->format ( "Y" );
if($month == 12 && $week == 1)
	$year = $year + 1;
// echo "Weeknummer: $month";
$wtp_start_date =  date('d-M-Y',strtotime($year.'W'.$week));
$wtp_end_date =  date('d-M-Y',strtotime($year.'W'.$week . ' + 5 days'));

$wtpList = $obj->fetchWTPByDate ( date("Y-m-d",strtotime($wtp_start_date))); // fetching WTP for week
// echo $start_date;

function integerToRoman($integer)
{
	// Convert the integer into an integer (just to make sure)
	$integer = intval($integer);
	$result = '';

	// Create a lookup array that contains all of the Roman numerals.
	$lookup = array('M' => 1000,
			'CM' => 900,
			'D' => 500,
			'CD' => 400,
			'C' => 100,
			'XC' => 90,
			'L' => 50,
			'XL' => 40,
			'X' => 10,
			'IX' => 9,
			'V' => 5,
			'IV' => 4,
			'I' => 1);

	foreach($lookup as $roman => $value){
		// Determine the number of matches
		$matches = intval($integer/$value);

		// Add the same number of characters to the string
		$result .= str_repeat($roman,$matches);

		// Set the integer to be the remainder of the integer and the value
		$integer = $integer % $value;
	}

	// The Roman numeral should be built, return it
	return $result;
}

?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>NIAT | WTP Report</title>
<meta
	content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
	name='viewport'>
<!-- bootstrap 3.0.2 -->
<style type="text/css">
table {
	border-collapse: collapse;
}

 table,td,th,tr{
	border: 1px solid black;
	padding: 2px;
	font-size: 12px;
}

.text-left {
	text-align: left;
}

.text-center {
	text-align: center;
}

 .page {
        width: 210mm;
        min-height: 297mm;
        padding: 2mm;
        margin: 10mm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    }
 .quality-policy{
 text-align: center;
  font-family: arial; 
  margin-top: 10px;
 }

@page {
        size: A4;
        margin: 0;
    }
    @media print {
        html, body {
            width: 210mm;
            height: 297mm;        
        }
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
        }
         .quality-policy{
 text-align: center;
  font-family: arial; 
  margin-top: 3mm;
  font-size:4mm;
 }
 table,td,th{
	border: 1px solid black;
	padding: 2px;
	font-size: 12px;
}
    }
</style>

</head>
<body>
<?php if (count( $wtpList ) > 0 ){?>
<div class="page">
	<div style="text-align: center;">
		<img style="width: 124px;" src="img/Niat_logo_new.png">
	</div>
	<div
		style="text-align: center; font-family: arial; font-weight: bold; text-decoration: underline;">NAVAL
		INSTITUTE OF AERONAUTICAL TECHNOLOGY</div>
	<div
		style="text-align: center; font-family: arial; font-weight: bold; text-decoration: underline; margin-top: 5px;">QUALITY
		POLICY</div>
	<div class="quality-policy">
		<div>NIAT commits itself to provide Quality Aviation Technical
			Training to meet the Customer requirements of:-</div>
		<div>Highest degree of operational readiness with emphasis on flight
			safety and airworthiness of Naval</div>
		<div>Air Arm throught constant innovation, continual Improvement and
			adherence to core values.</div>
		<div>with technical changes and applied research.</div>
	</div>
	<div
		style="text-align: center; font-family: arial; 
		font-weight: bold; text-decoration: underline; margin-top: 15px;">
		WEEKLY TRAINING PROGRAMME</div>
		<div
		style="text-align: center; font-family: arial; 
		font-weight: bold; text-decoration: underline; margin-top: 5px;">
		WEEKLY COMMENCING FROM <?php echo $wtp_start_date?> TO <?php echo $wtp_end_date?></div>
	<div style="margin-top: 10px;">
		<table align="center">
			<thead>
				<tr>
					<th class="text-center">SL</th>
					<th class="text-center">ROOM NO/COURSE</th>
					<th class="text-center">STR/WEEK</th>
					<th class="text-center">COMM <br />COMP
					</th>
					<th>SESN</th>
					<th class="text-left">SUBJECTS</th>
					<th class="text-left">INSTRUCTORS</th>
					<th class="text-left">DIV CDR/COURSE OFFICER <br /> COURSE
						INSTRUCTOR
					</th>
				</tr>
			</thead>
			<tbody>
<?php

for($i = 0; $i < count ( $wtpList ); $i ++) {
	// $rankId = $instruc [$r] ["instructor_rank"];
	// $rankName = $obj->fetchRankById ( $rankId );
	$wtpInfo = $obj->fetchWTPInfoById ( $wtpList [$i] ["wtp_id"] );
	$session_wtp = "";
	$subject_wtp = "";
	$ins_wtp = "";
	for($j = 0; $j < count ( $wtpInfo ); $j ++) {
$session_wtp =  $session_wtp . integerToRoman($wtpInfo[$j]["session"])."<br><br>";
$subject_wtp =  $subject_wtp . $wtpInfo[$j]["subject_name"]."<br><br>";
$ins_wtp =  $ins_wtp . $wtpInfo[$j]["ranks"]." ".$wtpInfo[$j]["Instructor_name"]."<br><br>";
}
// 	echo json_encode ( $wtpInfo );
	?>
			<tr>
					<td class="text-center"><?php echo $i+1; ?></td>
					<td class="text-center"><?php echo $wtpList[$i]["class_room"]?>
				<br />
					<br />
				<?php echo $wtpList[$i]["course_name"]?>
				<br />
				<?php echo $wtpList[$i]["course_no"]?>
				</td>
					<td class="text-center">
				<?php echo $wtpList[$i]["strength"]?>
				<br />
					<br />
				<?php echo $wtpList[$i]["current_week"] ?>/<?php echo $wtpList[$i]["full_week"] ?>				
				</td>
					<td class="text-center">
				<?php echo $wtpList[$i]["start_date"]?>
				<br />TO<br />
				<?php echo $wtpList[$i]["end_date"]?>
				</td>
					<td class="text-center"><?php echo $session_wtp?></td>
					<td class="text-left"><?php echo $subject_wtp ?></td>
					<td class="text-left"><?php echo $ins_wtp ?></td>


					<td class="text-left">
				<?php echo $wtpList[$i]["course_off_rank"]." ".$wtpList[$i]["course_off"]?>
				<br /><br />
				<?php echo $wtpList[$i]["course_ins_rank"]." ".$wtpList[$i]["course_ins"]?>
				</td>
				</tr>
			<?php 
			}
			?>






		</tbody>

		</table>


	</div>

</div>
<script type="text/javascript">
window.print();

			</script>
<?php } else {?>
<div style="text-align: center;">
		<img style="width: 124px;" src="img/Niat_logo.png">
	</div>
	<div
		style="text-align: center; font-family: arial; font-weight: bold; text-decoration: underline;">NAVAL
		INSTITUTE OF AERONAUTICAL TECHNOLOGY</div>
		
		<div style="margin-top:20px;text-align: center; font-family: arial; font-weight: bold; text-decoration: underline; color: red; font-size: 20px;">Sorry but we don't find any WTP matching your criteria.</div>
<?php }?>
</body>
</html>