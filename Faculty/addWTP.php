<?php
ob_start();
include("autoload.php");
include("check_session.php");

$us	=	new	Auth();
$rec= $us->getUserInfo($fuserId);
$depId = $rec[0]["dep_id"];


$obj	=	new Faculty();

$tmp = $obj->fetchValidCourses($depId); //fetching Courses


//inserting Instructor

	if(isset($_POST["addButton"])){
	
	 
			if($obj->addWTP($_POST,$depId)){							
				$msg = "Added Successfully !";	
			   header("Location:addWTP.php?msg=$msg");		
			   exit;						
			}else{			
				$msg1 = $obj->getError();
				header("Location:addWTP.php?msg1=$msg1");		
			   exit;						
			}		
		
	}

 

$instruc = $obj->fetchInstructor(); //fetching CourseInstructor



$msg	=$_GET["msg"];
$msg1	=$_GET["msg1"];
	
$webpageTitle	=	"Add WTP";
?>




<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
        <?php include("top.php"); ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
<style>
.grade-A {
	background-color: #04B404;
	color: #ffffff;
}

.grade-B {
	background-color: #F7FE2E;
	color: #000000;
}

.grade-C {
	background-color: #00BFFF;
	color: #ffffff;
}

.no-select {
	
}
</style>

<script>
function startTime() {
    var today=new Date();
    var h=today.getHours();
    var m=today.getMinutes();
    var s=today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt').innerHTML = h+":"+m+":"+s;
    var t = setTimeout(function(){startTime()},500);
}

function checkTime(i) {
    if (i<10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}
</script>



</head>
<body class="skin-blue" onLoad="startTime()">
	<?php include("head.php"); ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
		<!-- Left side column. contains the logo and sidebar -->
            <?php include("sidemenu.php"); ?>

            <!-- Right side column. Contains the navbar and content of the page -->
		<aside class="right-side">

			<section class="content-header">

				<div id="txt" class="alert alert-info"></div>

				<ol class="breadcrumb">
					<li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><i class="fa fa-bookmark-o"></i> Masters</li>
					<li class="active">Manage WTP</li>
				</ol>
				<h1>
					<small>Add WTP</small>
				</h1>

			</section>
			<!-- Main content -->
			<section class="content">
				<div class="row">


					<!-- left column -->
					<div class="col-md-12">
						<!-- general form elements -->
						<div class="box box-success">
					
							<?php
							if($msg)
							{
							?>    
							<div class="alert alert-success alert-dismissable"> <?php echo $msg; ?><button
									type="button" class="close" data-dismiss="alert"
									aria-hidden="true">×</button>
							</div>
							<?php
							}
							?>    
							
							<?php
							if($msg1)
							{
							?>    
							<div class="alert alert-danger alert-dismissable"> <?php echo $msg1; ?><button
									type="button" class="close" data-dismiss="alert"
									aria-hidden="true">×</button>
							</div>
							<?php
							}
							?>    
                              
                              
                                <!-- form start -->

							<div class="col-md-12 box-body">


								<form name="myform" role="form" method="post"
									action="addWTP.php" enctype="multipart/form-data"
									onSubmit="return checkDates();">

									<div class="col-md-6 form-group">
										<label for="exampleInputEmail1">Course Name *</label> <select
											id="course" name="course" class="form-control" required
											onChange="fetchSubjects();">
											<option value="">Select</option>
													<?php for($r=0;$r<count($tmp);$r++){?>
													<option value="<?php echo $tmp[$r]["course_id"];?>"><?php echo $tmp[$r]["course_name"];?></option>
													<?php }?>												
											</select>

									</div>



									<span id="courseInf">
										<div class="col-md-6 form-group">
											<label for="exampleInputEmail1">
											Start Date 
											<span style="color:red;font-size:10px;">Select any date of the week</span>
											</label> 
											<input type="text" class="form-control" id="startdate"
												name="startdate" required="required"
												onChange="fetchendDate(this.value);">
										</div>


										<div class="col-md-6 form-group">
											<label for="exampleInputEmail1">End Date *</label> <input
												type="text" class="form-control" id="enddate" name="enddate"
												required readonly>
										</div>
									</span>


<div class="row">
									 <div style="float:right;margin-right:30px"> 
  <div style="float:left;"> <div class="grade-A" style="width:50px;height:50px"></div> Grade A </div>
  <div style="float:left; margin-left:10px;"> <div class="grade-B" style="width:50px;height:50px"></div> Grade B </div>
  <div style="float:left; margin-left:10px;"> <div class="grade-C" style="width:50px;height:50px"></div> Grade C </div>
  </div>
  </div>




									<div style="overflow: auto; clear: left;">

										<table id="example1" class="table table-bordered table-hover">
											<thead>
												<tr>
													<th>SESSIONS/DAY</th>
													<!--<th>MON</th>
												<th>TUE</th>
												<th>WED</th>
                                                <th>THU</th>
												<th>FRI</th>
												<th>SAT</th>-->
												<?php
												$j=1;
												for($k=1;$k<=6;$k++)
												{
												
												?>
												<th><?php if($k==1) { echo "MON  Check for Holiday"; } else if($k==2) { echo "TUE"; } else if($k==3) { echo "WED"; } else if($k==4) { echo "THU"; } else if($k==5) { echo "FRI"; } else if($k==6) { echo "SAT"; } ?> &nbsp;&nbsp;<input
														type="checkbox"
														name="chk<?php echo $k; ?><?php echo $j; ?>"
														id="chk<?php echo $k; ?><?php echo $j; ?>"
														onClick="weekHide(<?php echo $k; ?>,<?php echo $j; ?>)"></th>
													<!--<th>TUE &nbsp;&nbsp;<input type="checkbox" name="tueCheck" id="tueCheck" ></th>
												<th>WED &nbsp;&nbsp;<input type="checkbox" name="wedCheck" id="wedCheck" ></th>
                                                <th>THU &nbsp;&nbsp;<input type="checkbox" name="thuCheck" id="thuCheck"  ></th>
												<th>FRI &nbsp;&nbsp;<input type="checkbox" name="friCheck" id="friCheck"  ></th>
												<th>SAT &nbsp;&nbsp;<input type="checkbox" name="satCheck" id="satCheck" ></th>-->
												<?php
												$j++;
												}
												
												?>
                                            </tr>
											</thead>
											<tbody> 
										
										
										<?php for($i=1;$i<=6;$i++){?>
										
										
                                            <tr>

													<td><?php echo $i;?></td>
												
												
												
											 <?php for($j=1;$j<=6;$j++) {?>	
												
                                                <td>




														<div class="col-md-3 form-group">
															<label for="exampleInputEmail1">Subject*</label> <select
																id="subject<?php echo $i;?><?php echo $j;?>"
																name="subject<?php echo $i;?><?php echo $j;?>"
																class="form-control"
																onchange="addWhileInstChange(this.value,this.id,<?php echo $i;?>,<?php echo $j;?>);">
																<option value="">Select</option>

															</select>

														</div>

														<div style="clear: both;"></div>

														<div class="col-md-3 form-group">
															<label for="inputPassword"> Instructor*</label> <select
																id="courseInstructor<?php echo $i;?><?php echo $j;?>"
																name="courseInstructor<?php echo $i;?><?php echo $j;?>"
																class="form-control"
																onChange="checkInstructorLeaveDate(this.value,this.id,<?php echo $i;?>,<?php echo $j;?>);">
																<option value="">Select</option>


															</select>
														</div> <input type="hidden"
														name="status<?php echo $i;?><?php echo $j;?>"
														id="status<?php echo $i;?><?php echo $j;?>" value="1">


													</td>
												
												<?php }?>
												
                                               
                                            </tr>
											
											
										 <?php }?>	
											
											
										      
                                        </tbody>

										</table>

									</div>

									<div class="box-footer">
										<button type="submit" name="addButton" class="btn btn-success">
											Save <i class="fa fa-check"></i>
										</button>
	
									</div>

								</form>


							</div>
							<!-- /.box-body -->
						</div>


					</div>

				</div>

			</section>
			<!-- /.content -->

			<!-- data table--->




			<!--- /.data table -->

		</aside>
		<!-- /.right-side -->
	</div>
	<!-- ./wrapper -->

	<!-- add new calendar event modal -->


	<!-- jQuery 2.0.2 -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery UI 1.10.3 -->
	<script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
	<!-- DATA TABES SCRIPT -->
	<script src="js/plugins/datatables/jquery.dataTables.js"
		type="text/javascript"></script>
	<script src="js/plugins/datatables/dataTables.bootstrap.js"
		type="text/javascript"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
	<script src="js/AdminLTE/app.js" type="text/javascript"></script>

	<!-- <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"> -->

	<!-- <script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>	 -->
	<link rel="stylesheet" href="../jquery-ui-1.11.2.custom/jquery-ui.css">

	<script src="../jquery-ui-1.11.2.custom/jquery-ui.js"></script>


	<!-- <script type="text/javascript">
        CKEDITOR.replace(txtaddress);
        </script> -->


	<script type="text/javascript">
            $(function() {
                $('#example1').dataTable({
                    "bPaginate": false,
                    "bLengthChange": true,
                    "bFilter": false,
                    "bSort": false,
                    "bInfo": false,
                    "bAutoWidth": false
                });
            });
        </script>

	<script>
		  
  $(function() {
	    $( "#startdate" ).datepicker({ 
	       
	        firstDay: 1,
	        onSelect: function(date){
	         var d = new Date(date);
	            var index = d.getDay();
	            if(index == 0) {
	             d.setDate(d.getDate() - 6);
	            }
	            else if(index == 1) {
	             d.setDate(d.getDate());
	            }
	            else if(index != 1 && index > 0) {
	              d.setDate(d.getDate() - (index - 1));
	            }
	            var value = ("0"+d.getDate()).slice(-2)+"-"+("0"+(parseInt(d.getMonth())+1)).slice(-2)+"-"+d.getFullYear();
	            $(this).val(value);
	            fetchendDate(value);
		        }


	         });  
  });
  
  
 
		function fetchSubjects(){
		
		 var course = document.getElementById('course').value;

		 $.ajax(
					{
						url :'ajaxfetchSubjects.php?course='+course,
						type: "POST",
						async: false,
						success:function(data) 
						{
						
						
						for(var t=1;t<=6;t++){
						
						for(var f=1;f<=6;f++){
						
						$("#subject"+t+f).html(data);		
						$("#subject"+t+f).html(data);						
						$("#subject"+t+f).html(data);						
						$("#subject"+t+f).html(data);						
						$("#subject"+t+f).html(data);						
						$("#subject"+t+f).html(data);											
						
						
						}
						}
						
						
						
							
						}
		  
		});
		
		fetchCourseInfo(course);
		
			
		}		
		
		function fetchCourseInfo(course){

		 $.ajax(
					{
						url :'ajaxfetchCourseInformation.php?course='+course,
						type: "POST",
						async: false,
						success:function(data) 
						{
						
			
						$("#courseInf").html(data);		

							
						}
		  
		});
		
		
		
			
		}		
		

				function checkClasroom()
		{
		var classroom = document.getElementById('classroom').value;
		 $.ajax(
					{
						url :'ajaxfetchClassInformation.php?class='+classroom,
						type: "POST",
						async: false,
						success:function(data) 
						{
						
			
						$("#facility").val(data);		

							
						}
		  
		});
		checkclassalloc();
		
		}
		
		
		
		function checkInstructorLeaveDate(valu,fid,i,j){
		
		 var startdate = document.getElementById('startdate').value;
		 var enddate = document.getElementById('enddate').value;
          var subject =  document.getElementById('subject'+i+j).value;
          var optionClass = $( "select#"+fid+" option:selected" ).attr('class')+" form-control";

		 $.ajax(
					{
						url :'ajaxcheckInstructorLeaveDate.php?startdate='+startdate+'&enddate='+enddate+'&instructor='+valu,
						type: "POST",
						async: true,
						success:function(data) 
						{	
						
						if(data!=0){						
						data = jQuery.parseJSON(data);
						
						if(confirm("He is unavailable from " + data[0]['start_date'] +" to "+  data[0]['end_date'] +". Do you still want to plot him. If yes plot him if no then change either Subject or instructor as normal")){
						
						
						//alert(valu);
						
						
						for(var f=1;f<=6;f++){
						
						var typ = "courseInstructor"+i+f;
						
						var ret = "subject"+i+f;
						
						//$("typ select").val(val);
						
						
						$("#"+typ).val(valu);
						$("#"+typ).removeClass();
						$("#"+typ).addClass(optionClass);
						$("#"+ret).val(subject);
						
						
						}
						
						
							return true;
						}else{
						    document.getElementById(fid).value='';
						    $("#"+fid).removeClass();
							return false;
						}
						
						}
						else{
						
						
						
							for(var f=1;f<=6;f++){
						
								var typ = "courseInstructor"+i+f;
								
								var ret = "subject"+i+f;
								
								//$("typ select").val(val);
								
								
								$("#"+typ).val(valu);
						$("#"+typ).removeClass();
						$("#"+typ).addClass(optionClass);
								$("#"+ret).val(subject);
								
								
								}
						
						
						chkAssignedForAnotherCourseOnSession(startdate,enddate,i,j,valu);	
						
						}
						
						
						
						
						
						
						
					//	$("#subject1").html(data);		
					//	$("#subject2").html(data);						
					//	$("#subject3").html(data);						
					//	$("#subject4").html(data);						
					//	$("#subject5").html(data);						
					//	$("#subject6").html(data);											
							
						}
						
					
						
						
		  
		});
		
		
		
			
		}	
		
		
		
		
		
		function chkAssignedForAnotherCourseOnSession(startdate,enddate,i,j,valu){
		

		
			 $.ajax(
					{
						url :'ajaxfetchchkAssignedForAnotherCourseOnSession.php?startdate='+startdate+'&enddate='+enddate+'&instructor='+valu+'&rw='+i+'&colm='+j,
						type: "POST",
						async: false,
						success:function(data) 
						{
							
						
						if(data!=0){						
						data = jQuery.parseJSON(data);
						var already_courses = "";
						$.each(data,function(i,item){
							already_courses += item["course_name"]+"/"+item["strength"]+",";
							});
						already_courses = already_courses.substring(0, already_courses.length - 1);
						if(confirm("Instructor is already loaded with "+already_courses+". Still you want to assign this class?")){
							
							return true;
						}else{
							 for(var f=1;f<=6;f++){
									
									var typ = "courseInstructor"+i+f;
									
									$("#"+typ).val('');
									 $("#"+typ).removeClass();
									 $("#"+typ).addClass("form-control");
									
									
									}
							
						}
						
						
						
						  
						
						
						}
						
								
								
								
				}
		  
		});				
								
								
								
								
			
	   	}	
		
		
		
		
		
		
		
		
		
		function addWhileInstChange(valu,fid,i,j){
		
		
          var subject =  document.getElementById('subject'+i+j).value;
		  
		  var courseinst =  document.getElementById('courseInstructor'+i+j).value;

		
		
			 $.ajax(
					{
						url :'ajaxfetchAssignedInstructor.php?subject='+subject,
						type: "POST",
						async: false,
						success:function(data) 
						{
							
						
						
							for(var f=1;f<=6;f++){
						
								$("#courseInstructor"+i+f).html(data);		
								$("#courseInstructor"+i+f).html(data);						
								$("#courseInstructor"+i+f).html(data);						
								$("#courseInstructor"+i+f).html(data);						
								$("#courseInstructor"+i+f).html(data);						
								$("#courseInstructor"+i+f).html(data);		
						
								var typ = "courseInstructor"+i+f;
								
								var ret = "subject"+i+f;
								
								//$("typ select").val(val);
								
								
								$("#"+typ).val(courseinst);
								$("#"+ret).val(subject);
								
								
								
								
								
								}
								
								
								
								
				}
		  
		});				
								
								
								
								
			
	   	}	
		
		
		
		
				




//    		 $("#enddate").change(function () {
// 			var startDate = document.getElementById("startdate").value;
// 			var endDate = document.getElementById("enddate").value;
			
// 			var startDate = startDate.split("-").reverse().join("-");
			
// 			var endDate = endDate.split("-").reverse().join("-");
			
		
// 			startDate = new Date(startDate);

// 			endDate = new Date(endDate);
			
// 			if (startDate >= endDate) {
// 			alert("End date should be greater than Start date");
// 			document.getElementById("enddate").value = "";
// 			}
			
// 			checkclassalloc();
			
// 		});
		
// 		$("#startdate").change(function () {
// 			var startDate = document.getElementById("startdate").value;
// 			var endDate = document.getElementById("enddate").value;
// 			//alert(Date.parse(startDate));
			
// 		 	var startDate = startDate.split("-").reverse().join("-");
			
// 			var endDate = endDate.split("-").reverse().join("-");
			
		
// 			startDate = new Date(startDate);

// 			endDate = new Date(endDate);
			
			
			
// 			if (startDate >= endDate) {
// 			alert("End date should be greater than Start date");
// 			document.getElementById("enddate").value = "";
// 			}
			
// 		});	
		
		
		function fetchendDate(stdate){
		
		
	     	var stdate = stdate.split("-").reverse().join("-");
			stdate = new Date(stdate);
			
			stdate.setDate(stdate.getDate() + 5);
		   
		   eddate = (stdate.getDate() + '-' + (stdate.getMonth() + 1) + '-' +  stdate.getFullYear());	
		   
		   document.getElementById('enddate').value=eddate;
		
		}
		
		
		
		function checkclassalloc()
		{
		
			var startDate = document.getElementById("startdate").value;
			var endDate = document.getElementById("enddate").value;
		var classroom = document.getElementById('classroom').value;
		
		//var cose = document.getElementById('coName').value;
		
		var strength = document.getElementById('strength').value;
		
		if(startDate!=''&&endDate!='') {
			$.ajax(
					{
						url :'ajaxcheckClassInformation.php?class='+classroom+'&sdate='+startDate+'&edate='+endDate,
						type: "POST",
						async: false,
						success:function(data) 
						{
						
						data = jQuery.parseJSON(data);
						
						if(data!=0)
						{
						
// 						var ty = data;
							if(confirm("" + data.course_name +"/"+ data.strength +" has occupied this class. Still you want to assign this class. Yes can assign.")){
						
							return true;
						}else{
						    document.getElementById('classroom').value='';
							return false;
						}
						}	
						}
		  
		});
		}
		}
		
		
		function weekHide(val,val1)
		{
		var m = val;
		var j = val1;
		
		if(document.getElementById('chk'+m+j).checked==true)
		{
		for(i=1;i<=6;i++)
		{
		$('#subject'+i+j).hide();
		$('#courseInstructor'+i+j).hide();
		$('#status'+i+j).val(2);
		}
		}
		else
		{
		for(i=1;i<=6;i++)
		{
		$('#subject'+i+j).show();
		$('#courseInstructor'+i+j).show();
		$('#status'+i+j).val(1);
		}
		}
		}

		
		

 
  </script>




</body>
</html>