<?php
ob_start ();
include ("autoload.php");
include ("check_session.php");

$us = new Auth ();
$rec = $us->getUserInfo ( $fuserId );

$obj = new Faculty ();

$tmp = $obj->fetchRanks (); // fetching Ranks
                           
// inserting Class Room

if (isset ( $_POST ["addButton"] )) {
	
	if ($obj->addClassRoom ( $_POST )) {
		$msg = "Added Successfully !";
		header ( "Location:addClassRoom.php?msg=$msg" );
		exit ();
	} else {
		$msg1 = $obj->getError ();
		header ( "Location:addClassRoom.php?msg1=$msg1" );
		exit ();
	}
}

// deleting ClassRoom

if ($_GET ["action"] == "del") {
	$vid = $_GET ["vid"];
	$tt = $obj->deleteClassRoom ( $vid );
	header ( "Location:addClassRoom.php?msg=Deleted successfully" );
	exit ();
}

// fetching ClassRoom

$tmplist = $obj->fetchClassRoom ();
$size = count ( $tmplist );

$msg = $_GET ["msg"];
$msg1 = $_GET ["msg1"];

$webpageTitle = "Manage Class Room";
?>




<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
        <?php include("top.php"); ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->



</head>
<body class="skin-blue" onLoad="startTime()">
	<?php include("head.php"); ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
		<!-- Left side column. contains the logo and sidebar -->
            <?php include("sidemenu.php"); ?>

            <!-- Right side column. Contains the navbar and content of the page -->
		<aside class="right-side">

			<section class="content-header">

				<div id="txt" class="alert alert-info"></div>
				<ol class="breadcrumb">
					<li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><i class="fa fa-bookmark-o"></i> Masters</li>
					<li class="active">Manage Class Room</li>
				</ol>
				<h1>
					<small>Add Class Room</small>
				</h1>

			</section>
			<!-- Main content -->
			<section class="content">




				<div class="row">
					<!-- left column -->
					<div class="col-md-12">
						<!-- general form elements -->
						<div class="box box-success">
					
							<?php
							if ($msg) {
								?>    
							<div class="alert alert-success alert-dismissable"> <?php echo $msg; ?><button
									type="button" class="close" data-dismiss="alert"
									aria-hidden="true">×</button>
							</div>
							<?php
							}
							?>    
							
							<?php
							if ($msg1) {
								?>    
							<div class="alert alert-danger alert-dismissable"> <?php echo $msg1; ?><button
									type="button" class="close" data-dismiss="alert"
									aria-hidden="true">×</button>
							</div>
							<?php
							}
							?>    
                              
                              
                                <!-- form start -->
							<form name="myform" role="form" method="post"
								action="addClassRoom.php" enctype="multipart/form-data">
								<div class="col-md-12 box-body">




									<div class="col-md-6 form-group">
										<label for="exampleInputEmail1">Class Room *</label> <input
											type="text" class="form-control" id="Name" name="Name"
											placeholder="Name" required
											style="text-transform: uppercase;"
											onChange="checkclassName(this.value)">
									</div>


									<div class="col-md-6 form-group">
										<label for="exampleInputEmail1">Strength* </label> <input
											type="text" class="form-control" id="strength"
											name="strength" placeholder="Strength" required
											onKeyPress="return isNumberKey(event)"
											onChange="return checkStrength()">
									</div>


									<div class="col-md-6 form-group">
										<label for="exampleInputEmail1">Facilities </label> <input
											type="text" class="form-control" id="facilities"
											name="facilities" placeholder="Facilities">
									</div>




								</div>
								<!-- /.box-body -->


								<div class="box-footer">
									<button type="submit" name="addButton" class="btn btn-success">
										Save <i class="fa fa-check"></i>
									</button>
								</div>
							</form>
						</div>
						<!-- /.box -->

					</div>



				</div>







				<div class="box">
					<div class="box-header"></div>
					<!-- /.box-header -->
					<div class="box-body table-responsive">
						<table id="example1" class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>Sl No.</th>
									<th>Class Room</th>
									<th>Strength</th>
									<th>Facilities</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
										   <?php
													$i = 0;
													while ( $i < $size ) {
														?>						    
                                            <tr>
									<td><?php echo $i+'1';?></td>
									<td><?php echo $tmplist[$i]["class_room"];?> </td>
									<td><?php echo $tmplist[$i]["strength"];?> </td>
									<td><?php echo $tmplist[$i]["facilities"];?> </td>


									<td><a
										href="editClassRoom.php?id=<?php echo $tmplist[$i]["class_room_id"];?>"
										class="btn btn-info">Edit <i class="fa fa-edit"></i></a>&nbsp;<a
										href="addClassRoom.php?vid=<?php echo $tmplist[$i]["class_room_id"];?>&action=del"
										onClick="return askDelete();" class="btn btn-danger">Delete <i
											class="fa fa-trash-o"></i></a></td>
								</tr>
											<?php
														$i ++;
													}
													?>
                                            
                                                                                     
                                        </tbody>

						</table>
					</div>
					<!-- /.box-body -->
				</div>









			</section>
			<!-- /.content -->

			<!-- data table--->




			<!--- /.data table -->

		</aside>
		<!-- /.right-side -->
	</div>
	<!-- ./wrapper -->

	<!-- add new calendar event modal -->


	<!-- jQuery 2.0.2 -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery UI 1.10.3 -->
	<script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
	<!-- DATA TABES SCRIPT -->
	<script src="js/plugins/datatables/jquery.dataTables.js"
		type="text/javascript"></script>
	<script src="js/plugins/datatables/dataTables.bootstrap.js"
		type="text/javascript"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
	<script src="js/AdminLTE/app.js" type="text/javascript"></script>


	<!-- <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"> -->

	<!-- <script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>	 -->
	<link rel="stylesheet" href="../jquery-ui-1.11.2.custom/jquery-ui.css">

	<script src="../jquery-ui-1.11.2.custom/jquery-ui.js"></script>


	<!-- <script type="text/javascript">
        CKEDITOR.replace(txtaddress);
        </script> -->


	<script type="text/javascript">
            $(function() {
                $('#example1').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>


	<script type="text/javascript">
		
		function askDelete(){
			if(confirm("Do you want to delete this item ? click OK to continue, CANCEL to exit")){
				return true;
			}else{
				return false;
			}
		}



	function checkStrength()
	{
		if(document.getElementById('strength').value==0)
		{
			alert("Strength cannot be 0");
			document.getElementById('strength').value="";
			return false;
		}
		else
		{
			return true;
		}
	}
	
	
	
      function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
	{
		alert("Please enter valid number");
        return false;
	}
	else
	{
    return true;
	}
}    


 
  </script>

	<script>
function startTime() {
    var today=new Date();
    var h=today.getHours();
    var m=today.getMinutes();
    var s=today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt').innerHTML = h+":"+m+":"+s;
    var t = setTimeout(function(){startTime()},500);
}

function checkTime(i) {
    if (i<10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}

 function checkclassName(classname)
	{
	//alert('rrr');
	$.ajax(
                            {
                                url :'ajaxcheckclassname.php?name='+classname,
                                type: "POST",
                                async: true,
                                success:function(data) 
                                {	
								if(data!=="")
								{
									alert("Class name already exist");
									document.getElementById('Name').value="";
                                }
								}
								
	});
	}
</script>


</body>
</html>