<?php
ob_start();
include("autoload.php");

$util	=new Utilities();
$register	=	new Session();
session_start();
$msg	=	"";



 if(isset($_POST["submit"])){ 
 
  
 		$us		=	new Auth();
		if($us->checkFacultyLogin($_POST)){		
	
			$register	=	new Session();
			session_start();
			$register->setSession("USER_LOGINED_ID",$us->userId);
			$register->setSession("USER_LOGINED_NAME",$us->userName);	
			$register->setSession("USER_LOGINED_ROLE",$us->userRole);		
			header("Location:dashboard.php");
			exit;
		
		}else{
		
			$msg	=	$us->getError();
			header("Location:index.php?msg=$msg");
			exit;
		}
	
 }
 

$msg	=	$_GET["msg"];
?>

<!DOCTYPE html>
<html style="margin: 0px;">
    <head>
	
	
	<style type="text/css">
<!--
body {
	background-image: url(../mig.jpg)!important;
	background-repeat: no-repeat;
	background-position:center;
	background-attachment:fixed;
	-webkit-background-size: cover;
 	-moz-background-size: cover;
 	-o-background-size: cover;
	 background-size: cover;
}
-->
</style>


        <meta charset="UTF-8">
        <title>NIAT| Sign in</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		
		<link href="css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="js/html5shiv.js"></script>
          <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
	<div style="text-align: center">
<img style="width: 100%; height: 200px; margin: 0px auto;" src="../headerimg_new.png">
</div>
	
        <div class="form-box" id="login-box" style="margin:0px auto">
		<?php if($msg!="") { ?>
		
									 <div class="form-group has-error">
                                            <label class="control-label" for="inputError"><i class="fa fa-asterisk"></i> <?php echo $msg;  ?></label>
                                        </div>

									<?php } ?>
            <div class="header">Faculty | Sign In</div>
            <form action="index.php" method="post" name="myform" autocomplete="off">
                <div class="body bg-gray">
                    <div class="form-group">
                        <input type="text" name="txtusername" class="form-control" placeholder="User ID *"/>
                    </div>
                    <div class="form-group">
                        <input type="password" name="txtpassword" class="form-control" placeholder="Password *"/>
                    </div>
                </div>
                <div class="footer">                                                               
                    <button type="submit" name="submit" class="btn bg-olive btn-block">Sign me in</button>  
					
				<!--	 <a href="instructorWTP.php"><button type="button" name="button" class="btn bg-olive btn-block">Instructor WTP </button>  </a>
					 
					 <div style="clear:both;"></div>
					 
				    <a href="Listwtptrainee.php"> <button type="button" name="button" class="btn bg-olive btn-block" style="margin-top:10px;">Trainee WTP </button>  </a>-->
					
					<!--<p align="center"><a href="registration.php" class="text-center">Register a new Company</a></p>-->
                </div>
            </form>
        </div>

<div style="margin: 20px;" id="footer">
<div style="float: left; font-size: 20px; 
font-weight: bold; color: rgb(240, 230, 140);">
   GUIDED BY: <br>Lt Cdr V Sundararajan   </div>
<div style="float: right; font-size: 20px; 
font-weight: bold; color: rgb(240, 230, 140);">
   SUBMITTED BY: <br>Lt Priyank Kumawat
    <br>SLt Manas Mendiratta  <br>SLt Vijay 
     <br>SLt Pankaj Chaudhary</div>
</div>
        <!-- jQuery 2.0.2 -->
        <script src="js/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>        

    </body>
</html>
