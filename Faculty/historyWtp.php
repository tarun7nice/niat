<?php
ob_start();
include("autoload.php");
include("check_session.php");

$us	=	new	Auth();
$rec= $us->getUserInfo($fuserId);


$obj	=	new Faculty();

 
 //deleting Instructor 
 
 if($_GET["action"]=="del"){
		$id		=	$_GET["id"];
		$tt	=	$obj->deletelistedWTP($id);				
		header("Location:listWtp.php?msg=Deleted successfully");
		exit;
      }
	


//fetching Instructor 
	
$tmplist	=	$obj->fetchWTP();	
$size	=	count($tmplist);


$msg	=$_GET["msg"];
$msg1	=$_GET["msg1"];
	
$webpageTitle	=	"History WTP";
?>




<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php include("top.php"); ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
	

		
    </head>
    <body class="skin-blue" onLoad="startTime()">
	<?php include("head.php"); ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <?php include("sidemenu.php"); ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
			
			<section class="content-header">
			<div id="txt" class="alert alert-info"></div>
			
			 <ol class="breadcrumb">
                        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li><li><i class="fa fa-bookmark-o"></i> Masters</li>
                        <li class="active">Manage WTP</li>
                    </ol>
                    <h1><small>History WTP</small></h1>
                   
                </section>
               <!-- Main content -->
                <section class="content">   
							
							<div class="box">
                                <div class="box-header">     
							                            
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Sl No.</th>
                                                <th>Course Name</th>
												<th>Start date</th>
												<th>End date</th>										
												<th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										   <?php 
							                $i	=	0;										
							                while($i < $size) {	
											//$facultyId = $tmplist[$i]["faculty_id"]	;
										//	$dep = $obj->fetchDepid($facultyId);
										//	$depId = $dep[0]["dep_id"];
											$courseDet = $obj->fetchcourseById($tmplist[$i]["course_id"]);
											
											//$facultyName = $obj->fetchFacultybyId($facultyId);
							                ?>						    
                                            <tr>
                                                <td><?php echo $i+'1';?></td>
                                                <td><?php echo $courseDet[0]["course_name"];?> </td>	
												<td><?php echo date('d-m-Y', strtotime($tmplist[$i]["start_date"]));?> </td>
									 			<td><?php echo date('d-m-Y', strtotime($tmplist[$i]["end_date"]));?> </td>
												
                                                <td><a target="_top" onClick="return hideURL(this);" href="viewWTP.php?id=<?php echo $tmplist[$i]["wtp_id"];?>" class="btn btn-primary">View <i class="fa fa-eye"></i></a>&nbsp;
                                                
                                                <a href="editWTP.php?id=<?php echo $tmplist[$i]["wtp_id"];?>" class="btn btn-info">Edit <i class="fa fa-edit"></i></a>&nbsp;
												
												<a href="listWtp.php?id=<?php echo $tmplist[$i]["wtp_id"];?>&action=del" onClick="return askDelete();" class="btn btn-danger">Delete <i class="fa fa-trash-o"></i></a></td>
                                            </tr>
											<?php
											$i++;
											}
											?>
                                            
                                                                                     
                                        </tbody>
                                        
                                    </table>
                                </div><!-- /.box-body -->
                            </div>
							
							
							
							
							
							
							
							

                </section><!-- /.content -->
				
				<!-- data table--->
				
				
				
				
				<!--- /.data table -->
				
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        <script src="js/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
		<!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>
		
		
	<!-- <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"> -->

	<!-- <script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>	 -->
	<link rel="stylesheet" href="../jquery-ui-1.11.2.custom/jquery-ui.css">

	<script src="../jquery-ui-1.11.2.custom/jquery-ui.js"></script>


	   <!-- <script type="text/javascript">
        CKEDITOR.replace(txtaddress);
        </script> -->
	
		
			<script type="text/javascript">
            $(function() {
                $('#example1').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>
		
		
		<script type="text/javascript">
		
		function askDelete(){
			if(confirm("Do you want to delete this item ? click OK to continue, CANCEL to exit")){
				return true;
			}else{
				return false;
			}
		}

		
		</script>
		
		
		  <script>
		  
  $(function() {
    $( "#grade" ).datepicker({ dateFormat: 'yy-mm-dd' });  
  });



 
  </script>
		

	<script>
function startTime() {
    var today=new Date();
    var h=today.getHours();
    var m=today.getMinutes();
    var s=today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt').innerHTML = h+":"+m+":"+s;
    var t = setTimeout(function(){startTime()},500);
}

function checkTime(i) {
    if (i<10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}
</script>	

<script language="javascript">
function buttonAction(func,obj){
//document.getElementById('pro_Menu').style.display = "block";
return func(obj);
}

function hideURL(obj){

return hs.htmlExpand(obj, { objectType: 'iframe', contentId: 'controlbar5' , align: 'center'} );
}
</script>



<script type="text/javascript" src="javascript/highslide-with-html.js"></script>
<link rel="stylesheet" type="text/css" href="javascript/highslide.css" />
<script type="text/javascript">
    hs.graphicsDir = 'javascript/graphics/';
    hs.outlineType = 'rounded-white';
	if(location.hash) {
	var tabid = location.hash.substr(1);
	$('a[href="#'+tabid+'"]').click();
	} 
</script>
        
    </body>
</html>
