<?php
ob_start();
include("autoload.php");
include("check_session_public.php");

$us	=	new	Auth();
$rec= $us->getUserInfo($fuserId);


$obj	=	new Faculty();

$tmp = $obj->fetchRanks(); //fetching Ranks


 

	
// Fecthing Assigned course

$assignC = $obj->fetchallAssignCourse();
$assignCount = count($assignC);


//fetching Instructor 
	
$tmplist	=	$obj->fetchInstructor();	
$size	=	count($tmplist);

// Fetch Faculty

$facultyList = $obj->fetchFacultybyDept();
$facultyCount = count($facultyList);

// Fetch Department

$dep = $obj->getDepartment();
$depCount = count($dep);


$msg	=$_GET["msg"];
$msg1	=$_GET["msg1"];
	
$webpageTitle	=	"WTP Listing";
?>




<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
		
        <?php 
		
		include("top.php"); 
		
		?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
	
<script type="text/javascript">

function add_sorted (parent_node, new_node) {
   var children = parent_node.getElementsByTagName('OPTION');
   var flag=true;
   
   for (var i = 0; i < children.length; i++) {
	 
       if (new_node.text == children[i].text) {
	      flag=false;
       }
       
   }
   
   if( children.length<1)
   parent_node.appendChild(new_node);
   else
   {
	   if(flag)
	   {
	   		parent_node.appendChild(new_node);
		   	}
	   }   
  
}
function move_right () {
   var left = document.getElementById('left');
   var right = document.getElementById('right');

   var Options = left.getElementsByTagName('OPTION');
   for (var i = 0; i < Options.length; i++) {
       if (Options[i].selected) {
     
       var optionsNew= new Option(Options[i].text, Options[i].value, false, Options[i].isSelected);
      
          // optionsNew.setAttribute("group_id", Options[i].parentNode.id);
           add_sorted(right, optionsNew);
         //  i = -1;
       }
   }
}

function move_left () {
   var left = document.getElementById('left');
   var right = document.getElementById('right');

   var Options = right.getElementsByTagName('OPTION');
   for (var i = 0; i < Options.length; i++) {
       if (Options[i].selected) {
          // var group_id = Options[i].getAttribute("group_id");
           //var move_to = left;
           right.remove(i);
           //add_sorted(move_to, Options[i]);
         // i = -1;
       }
   }
}


function Selectall(ff)
{
	for (var i = 0; i < ff.length; i++) { 
    ff.options[i].selected=true;    	
    }
 }
 
 
 
 
 
  function fetchWTP(str){
	//var category = document.getElementById("category").value;
	//document.getElementById("dd").style.display="block";
	var xmlhttp;    
	
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  {// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			var data=xmlhttp.responseText;
	
			 document.getElementById('listAssigned').innerHTML=data;
		}
	  }

	xmlhttp.open("GET","getAssign.php?ins="+str,true);
	xmlhttp.send();
	}
 
 
 
 
 
 
 function fetchSubjects(str){
	var ins = document.getElementById("insId").value;
	//document.getElementById("dd").style.display="block";
	var xmlhttp;    
	
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  {// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			var data=xmlhttp.responseText;
	
			 document.getElementById('listAssigned').innerHTML=data;
		}
	  }

	xmlhttp.open("GET","getAssignsub.php?courseId="+str+"&&ins="+ins,true);
	xmlhttp.send();
	}
	
	
</script> 

		
    </head>
    <body class="skin-blue"  onLoad="startTime()">
	<?php 
	if(!empty($fuserId)){
	include("head.php"); 
	}
	?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <?php 
			if(!empty($fuserId)){
			include("sidemenu.php"); 
			}
			?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
			
			<section class="content-header">
			<div id="txt" class="alert alert-info"></div>
                    <h1><small>List WTP</small></h1>
                   
                </section>
               <!-- Main content -->
                <section class="content">    
				
				
				          

                  <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-success">
					
							<?php
							if($msg)
							{
							?>    
							<div class="alert alert-success alert-dismissable"> <?php echo $msg; ?><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
							<?php
							}
							?>    
							
							<?php
							if($msg1)
							{
							?>    
							<div class="alert alert-danger alert-dismissable"> <?php echo $msg1; ?><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
							<?php
							}
							?>    
                              
                              
                                <!-- form start -->
                                <form name="myform" role="form" method="post" action="addWtp.php" id="myform">
                                    <div class="col-md-12 box-body">
									
										
																		
										<div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Course </label>
											<select id="Name" name="Name" class="form-control" onChange="fetchCourseNo();" >
											<option value="">---Select Course---</option>
											<?php
											$course = $obj->fetchMIP();
											$courseSize = count($course);
											$j=0;
											while($j<$courseSize)
											{
											?>
											<option value="<?php echo $course[$j]["course_id"]; ?>"><?php echo $course[$j]["course_name"]; ?></option>
											<?php
											$j++;
											}
											?>
											</select>
										</div>	
										
										
										<div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Course No</label>
											<input type="text" class="form-control" id="courseNo" name="courseNo" style="text-transform:uppercase;" placeholder="Course No"  readonly="true">
											
										</div>	
										
										
										
										
										<div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Start Date *</label>
                                            <input type="text" class="form-control" id="startdate" name="startdate" required onChange="fetchendDate(this.value);">
                                        </div>
										
										
										<div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">End Date *</label>
                                            <input type="text" class="form-control" id="enddate" name="enddate" required readonly="true">
                                        </div>
										
										 <div class="col-md-6 form-group" style="margin-top:24px;">
										  <label for="exampleInputEmail1"></label>
										 <button type="button" class="btn btn-success" onClick="fetchAlreCourse();">View</button>
										</div>
                                        
                                        <div class="col-md-6 form-group">
							<label for="exampleInputEmail1">Click below to see NIAT Floor
								Plan</label>
							<button class="form-control"
								style="background-color: green; color: #ffffff;"
								onclick="showDialog()">Show me Location</button>
						</div>
                                      </div><!-- /.box-body -->
									
									
                                    <!--<div class="box-footer">
                                        <button type="submit" name="addButton" class="btn btn-success">Save <i class="fa fa-check"></i></button>
                                    </div>-->
                                </form>
                            </div><!-- /.box -->
							
							</div>
							
							
							
							</div>
							
							<div id="listAssigned">
							
							<div class="box" >
                                <div class="box-header">     
							                            
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive" id="ex1">
                                    
                                </div><!-- /.box-body -->
                            </div>
							
							</div>
							
<div id="dialog"></div>
                </section><!-- /.content -->
				
				
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        <script src="js/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
		<!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>
        <!-- Zoomjs -->
	<script src="js/jquery.elevatezoom.js" type="text/javascript"></script>
		
		
	<!-- <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"> -->

	<!-- <script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>	 -->
	<link rel="stylesheet" href="../jquery-ui-1.11.2.custom/jquery-ui.css">

	<script src="../jquery-ui-1.11.2.custom/jquery-ui.js"></script>


	   <!-- <script type="text/javascript">
        CKEDITOR.replace(txtaddress);
        </script> -->
	
		
			<script type="text/javascript">

			 var elevateZoom;
				function showDialog(){
					
					var htmlimg = '<img id="zoom_01" src="img/clsroom.jpg" style="width:400px;height:400px;" data-zoom-image="img/clsroom.jpg"/>';
					$("#dialog").html(htmlimg);
					
				        
				        	$("#zoom_01").elevateZoom();
				        	elevateZoom=$("#zoom_01").data('elevateZoom');
				        	
			$("#dialog").dialog({
				minWidth: 450,
				title: "Class Room Location",
				close: function( event, ui ) {
					$("#dialog").html("");
					elevateZoom.changeState('disable');
					}
				});
				}
			
			$(function() {
    $( "#startdate" ).datepicker({ dateFormat: 'dd-mm-yy' });  
    $( "#enddate" ).datepicker({ dateFormat: 'dd-mm-yy' }); 
	
  });
			
            $(function() {
                $('#example1').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>
		
		
		<script type="text/javascript">
		
		function askDelete(){
			if(confirm("Do you want to delete this item ? click OK to continue, CANCEL to exit")){
				return true;
			}else{
				return false;
			}
		}
		
		
		$(document).keyup(function(){
		  var name = document.getElementById('Name').value;
			$("#Name").autocomplete({
				source:'getCoursesNamesTrainee.php?name='+name,
				minLength:1
			});
		});
		
		
		 $("#enddate").change(function () {
			var startDate = document.getElementById("startdate").value;
			var endDate = document.getElementById("enddate").value;
			
			var startDate = startDate.split("-").reverse().join("-");
			
			var endDate = endDate.split("-").reverse().join("-");
			
		
			startDate = new Date(startDate);

			endDate = new Date(endDate);
			
			if (startDate >= endDate) {
			alert("End date should be greater than Start date");
			document.getElementById("enddate").value = "";
			}
			
		});
		
		$("#startdate").change(function () {
			var startDate = document.getElementById("startdate").value;
			var endDate = document.getElementById("enddate").value;
			//alert(Date.parse(startDate));
			
		 	var startDate = startDate.split("-").reverse().join("-");
			
			var endDate = endDate.split("-").reverse().join("-");
			
		
			startDate = new Date(startDate);

			endDate = new Date(endDate);
			
			
			
			if (startDate >= endDate) {
			alert("End date should be greater than Start date");
			document.getElementById("enddate").value = "";
			}
			
		});	
		
		
		
		
		function fetchendDate(stdate){
		
		
	     	var stdate = stdate.split("-").reverse().join("-");
			stdate = new Date(stdate);
			
			stdate.setDate(stdate.getDate() + 5);
		   
		   eddate = (stdate.getDate() + '-' + (stdate.getMonth() + 1) + '-' +  stdate.getFullYear());	
		   
		   document.getElementById('enddate').value=eddate;
		
		}
		 
		
		
		
		
		
		function fetchAlreCourse(){
		
		 var name = document.getElementById('Name').value;
		 var startdate = document.getElementById('startdate').value;
		 var enddate = document.getElementById('enddate').value;

		 $.ajax(
					{
						url :'ajaxfetchWTP.php?name='+name+'&sdate='+startdate+'&edate='+enddate,
						type: "POST",
						async: false,
						success:function(data) 
						{
						$("#ex1").html(data);							
							
						}
		  
		});
			
		}
		
		
		
		function fetchCourseNo(){
		
		 var name = document.getElementById('Name').value;
		
		 $.ajax(
					{
						url :'ajaxfetchCourseNoTraineeWTP.php?name='+name,
						type: "POST",
						async: false,
						success:function(data) 
						{
						$("#courseNo").val(data);							
							
						}
		  
		});
			
		}
		
		
		

		</script>
		
		
		  
		
<script>
function startTime() {
    var today=new Date();
    var h=today.getHours();
    var m=today.getMinutes();
    var s=today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt').innerHTML = h+":"+m+":"+s;
    var t = setTimeout(function(){startTime()},500);
}

function checkTime(i) {
    if (i<10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}
</script>	
		
        
    </body>
</html>