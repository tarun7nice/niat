<?php
ob_start ();
include ("autoload.php");
include ("check_session_public.php");

$us = new Auth ();
$rec = $us->getUserInfo ( $fuserId );

$obj = new Faculty ();

// fetching Best Instructor

$tmplist = $obj->fetchBestInstructor ();
$size = count ( $tmplist );

$msg = $_GET ["msg"];
$msg1 = $_GET ["msg1"];

$webpageTitle = "Instructor WTP";
?>




<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
        <?php include("top.php"); ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->


</head>
<body class="skin-blue" onLoad="startTime()">
	
	
	<?php if($fuserId!="") { include("head.php"); }  ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
		<!-- Left side column. contains the logo and sidebar -->
            <?php if($fuserId!="") { include("sidemenu.php"); }  ?>

            <!-- Right side column. Contains the navbar and content of the page -->
		<aside class="right-side">

			<section class="content-header">
				<div id="txt" class="alert alert-info"></div>

				<ol class="breadcrumb">
					<li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
					<li><i class="fa fa-bookmark-o"></i> Masters</li>
					<li class="active">Instructor WTP</li>
				</ol>
				<h1>
					<small>Instructor WTP</small>
				</h1>

			</section>
			<!-- Main content -->
			<section class="content">

				<div class="box">
					<div class="box-header">


						<div class="col-md-6 form-group">
							<label for="exampleInputEmail1">Instructor </label> <select
								id="instructor" name="instructor" class="form-control" required
								onChange="fetchInstructorWTP();">
								<option value="">---Select Instructor---</option>
												<?php
												$ins = $obj->fetchInstructor ();
												$insSize = count ( $ins );
												$i = 0;
												while ( $i < $insSize ) {
													$rankId = $ins [$i] ["instructor_rank"];
													$rankName = $obj->fetchRankById ( $rankId );
													?>
												<option value="<?php echo $ins[$i]["instructor_id"]; ?>"><?php echo $ins[$i]["instructor_name"]; ?>-&nbsp;<?php echo $rankName[0]["ranks"];?>-&nbsp;<?php echo $ins[$i]["personalno"];?></option>
												<?php
													$i ++;
												}
												?>
											</select>
						</div>


						<div class="col-md-6 form-group">
							<label for="exampleInputEmail1">Start Date *</label> <input
								type="text" class="form-control" id="startdate" name="startdate">
						</div>


						<div class="col-md-6 form-group">
							<label for="exampleInputEmail1">End Date *</label> <input
								type="text" class="form-control" id="enddate" name="enddate"
								onChange="fetchInstructorWTP();">
						</div>

						<div class="col-md-6 form-group">
							<label for="exampleInputEmail1">Click below to see NIAT Floor
								Plan</label>
							<button class="form-control"
								style="background-color: green; color: #ffffff;"
								onclick="showDialog()">Show me Location</button>
						</div>




					</div>
					<!-- /.box-header -->
					<div class="box-body table-responsive" id="ex1"></div>
					<!-- /.box-body -->
				</div>
				<div id="dialog"></div>








			</section>
			<!-- /.content -->

			<!-- data table--->




			<!--- /.data table -->

		</aside>
		<!-- /.right-side -->
	</div>
	<!-- ./wrapper -->

	<!-- jQuery 2.0.2 -->
	<script src="js/jquery.min.js"></script>
	<!-- jQuery UI 1.10.3 -->
	<script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
	<!-- DATA TABES SCRIPT -->
	<script src="js/plugins/datatables/jquery.dataTables.js"
		type="text/javascript"></script>
	<script src="js/plugins/datatables/dataTables.bootstrap.js"
		type="text/javascript"></script>
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
	<!-- AdminLTE App -->
	<script src="js/AdminLTE/app.js" type="text/javascript"></script>
	<!-- Zoomjs -->
	<script src="js/jquery.elevatezoom.js" type="text/javascript"></script>

			
	<!-- <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css"> -->

	<!-- <script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>	 -->
	<link rel="stylesheet" href="../jquery-ui-1.11.2.custom/jquery-ui.css">

	<script src="../jquery-ui-1.11.2.custom/jquery-ui.js"></script>



	<!-- <script type="text/javascript">
        CKEDITOR.replace(txtaddress);
        </script> -->


	<script type="text/javascript">
            $(function() {
                $('#example1').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>


	<script type="text/javascript">
// 		
 var elevateZoom;
			function showDialog(){
				
				var htmlimg = '<img id="zoom_01" src="img/clsroom.jpg" style="width:400px;height:400px;" data-zoom-image="img/clsroom.jpg"/>';
				$("#dialog").html(htmlimg);
				
			        
			        	$("#zoom_01").elevateZoom();
			        	elevateZoom=$("#zoom_01").data('elevateZoom');
			        	
		$("#dialog").dialog({
			minWidth: 450,
			title: "Class Room Location",
			close: function( event, ui ) {
				$("#dialog").html("");
				elevateZoom.changeState('disable');
				}
			});
			}
		
		
   		 $("#enddate").change(function () {
			var startDate = document.getElementById("startdate").value;
			var endDate = document.getElementById("enddate").value;
			
			var startDate = startDate.split("-").reverse().join("-");
			
			var endDate = endDate.split("-").reverse().join("-");
			
		
			startDate = new Date(startDate);

			endDate = new Date(endDate);
			
			if (startDate >= endDate) {
			alert("End date should be greater than Start date");
			document.getElementById("enddate").value = "";
			}
			
		});
		
		$("#startdate").change(function () {
			var startDate = document.getElementById("startdate").value;
			var endDate = document.getElementById("enddate").value;
			//alert(Date.parse(startDate));
			
		 	var startDate = startDate.split("-").reverse().join("-");
			
			var endDate = endDate.split("-").reverse().join("-");
			
		
			startDate = new Date(startDate);

			endDate = new Date(endDate);
			
			
			
			if (startDate >= endDate) {
			alert("End date should be greater than Start date");
			document.getElementById("enddate").value = "";
			}
			
		});	
		
		
		
		
	   function fetchInstructorWTP(){
		
		  var startDate = document.getElementById("startdate").value;
		  var endDate = document.getElementById("enddate").value;
		  
		  var instructor = document.getElementById("instructor").value; 
			
		
			
		 	var startDate = startDate.split("-").reverse().join("-");
			
			var endDate = endDate.split("-").reverse().join("-");
			
		 $.ajax(
					{
						url :'ajaxfetchInstructorWTP.php?startdate='+startDate+'&enddate='+endDate+'&instructor='+instructor,
						type: "POST",
						async: false,
						success:function(data) 
						{
						
						$("#ex1").html(data);		
						
							
						}
		  
		});
			
		}		
		

  $(function() {
    $( "#startdate" ).datepicker({ dateFormat: 'yy-mm-dd' }); 
	 $( "#enddate" ).datepicker({ dateFormat: 'yy-mm-dd' });  
  });



 
  </script>


	<script>
function startTime() {
    var today=new Date();
    var h=today.getHours();
    var m=today.getMinutes();
    var s=today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('txt').innerHTML = h+":"+m+":"+s;
    var t = setTimeout(function(){startTime()},500);
}

function checkTime(i) {
    if (i<10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}
</script>

</body>
</html>