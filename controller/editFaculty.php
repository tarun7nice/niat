<?php
ob_start();
include("autoload.php");
include("check_session.php");

$us	=	new	Auth();

$rec= $us->getUserInfo($userId);  /* Fetching current user info */


$obj	=	new Faculty();

$tmp = $obj->getDepartment(); // get Department

$id = $_GET["id"];  //staff id

$tmplist = $us->getUserInfo($id); //get staff info using id



	if(isset($_POST["addButton"])){	
	
		if($obj->editFaculty($_POST,$id)){							
			$msg = "Updated Successfully !";
			header("Location:listFaculty.php?msg=$msg");		
		exit;							
		}else{			
			$msg1 = $obj->getError();	
		header("Location:editFaculty.php?id=$id&msg1=$msg1");		
		exit;		
		}
		
				
		
	}


$msg	=$_GET["msg"];
$msg1	=$_GET["msg1"];
	
$webpageTitle	=	"Edit Faculty";
?>




<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php include("top.php"); ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
		
		
    </head>
    <body class="skin-blue">
	<?php include("head.php"); ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <?php include("sidemenu.php"); ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
			
			<section class="content-header">
			
			
			 <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li><i class="fa fa-bookmark-o"></i> Masters</li>
                        <li class="active">Manage Faculty</li>
                    </ol>
                    <h1><small>Edit Faculty</small></h1>
                   
                </section>
               <!-- Main content -->
                <section class="content">    
				
				
				          

                  <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-success">
					
							<?php
							if($msg)
							{
							?>    
							<div class="alert alert-success alert-dismissable"> <?php echo $msg; ?><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
							<?php
							}
							?>    
							
							<?php
							if($msg1)
							{
							?>    
							<div class="alert alert-danger alert-dismissable"> <?php echo $msg1; ?><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
							<?php
							}
							?>    
                              
                              
                                <!-- form start -->
                                <form name="myform" role="form" method="post" action="editFaculty.php?id=<?php echo $id;?>" enctype="multipart/form-data">
                                    <div class="col-md-12 box-body">
										
										 <div class="col-md-6 form-group">
                                            <label for="exampleInputEmail1">Faculty Name *</label>
                                            <input type="text" class="form-control" id="txtName" name="txtName" placeholder="Staff Name" required value="<?php echo $tmplist[0]["staff_name"];?>">
                                        </div>										
									
									
										
										<div class="col-md-6 form-group">
										<label for="inputPassword">Department*</label>
										 <select id="txtDepartment" name="txtDepartment" class="form-control" required>
												<option value="">Select</option>
													<?php for($r=0;$r<count($tmp);$r++){?>
													<option value="<?php echo $tmp[$r]["dep_id"];?>" <?php if($tmp[$r]["dep_id"]==$tmplist[0]["dep_id"]) {?> selected="selected" <?php }?>><?php echo $tmp[$r]["department_code"];?></option>
													<?php }?>
												
											</select>
										</div>
									
										
										
										<div class="col-md-6 form-group">
										<label for="inputPassword">Username*</label>
										 <input class="form-control" type="text" onKeyUp="checkexistingUser(this.value,'<?php echo $id;?>')" placeholder="Username....." name="txtusername" value="<?php echo $tmplist[0]["username"];?>" required><span style="color: red" id='userStatusMsg'></span>
										</div>
										
										<div class="col-md-6 form-group">
										<label for="inputPassword">Password*</label>
											<input class="form-control" type="password" placeholder="Password....." name="txtPass" >
										</div>	
										
									
                                    </div><!-- /.box-body -->
									
									
                                    <div class="box-footer">
                                        <button type="submit" name="addButton" id="addButton" class="btn btn-success">Save <i class="fa fa-check"></i></button>
                                    </div>
                                </form>
                            </div><!-- /.box -->
							
							</div>
							
							
							
							</div>

                </section><!-- /.content -->
				
				<!-- data table--->
				
				
				
				
				<!--- /.data table -->
				
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        <script src="js/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
		<!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>
		
		<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
       
	   <!-- <script type="text/javascript">
        CKEDITOR.replace(txtaddress);
        </script> -->
	
		
			<script type="text/javascript">
            $(function() {
                $('#example1').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>
		
		
		
		 <script>
	
	   function checkexistingUser(userName,id){
			$.post("chkuserexist.php?txtusername="+userName+"&id="+id,function(response){
				if(response){
					var responseObj=eval(response);
					if(responseObj){
						$('#userStatusMsg').html("Faculty Already Exists! Try Another Email");
						document.getElementById("addButton").disabled=true;
					}else{
						$('#userStatusMsg').html("");
						document.getElementById("addButton").disabled=false;
						}	
					}
				});
		   }
   </script>

		
        
    </body>
</html>