<?php
ob_start();
include("autoload.php");
include("check_session.php");

$us	=	new	Auth();

$rec= $us->getUserInfo($userId);  /* Fetching current user info */


$obj	=	new Faculty();


//fetching Faculty 

$tmplist	=	$obj->fetchFaculty();	
$size	=	count($tmplist);


  // to make staff active


  if($_GET["action"]=="deActive"){
		$cid		=	$_GET["cid"];
		$tt	=	$obj->makeActive($cid,1);		
		header("Location:listFaculty.php?msg=Activated successfully");
		exit;
      }
	  
	// to deactivate staff   
	  
  if($_GET["action"]=="Active"){
		$cid		=	$_GET["cid"];
		$tt	=	$obj->makeActive($cid,2);		
		header("Location:listFaculty.php?msg=Deactivated successfully");
		exit;
      }


$msg	=$_GET["msg"];
$msg1	=$_GET["msg1"];
	
$webpageTitle	=	"Add Staff";
?>




<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php include("top.php"); ?>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
		
		
    </head>
    <body class="skin-blue">
	<?php include("head.php"); ?>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <?php include("sidemenu.php"); ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
			
			<section class="content-header">
			
			
			 <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li><li><i class="fa fa-bookmark-o"></i> Masters</li>
                        <li class="active">Manage Faculty</li>
                    </ol>
                    <h1><small>List Faculty</small></h1>
                   
                </section>
               <!-- Main content -->
                <section class="content">    
				
				
				          

                  							
							<div class="box">
                                <div class="box-header">       
								
								
								<?php
								if($msg)
								{
								?>    
								<div class="alert alert-success alert-dismissable"> <?php echo $msg; ?><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
								<?php
								}
								?>    
								
								<?php
								if($msg1)
								{
								?>    
								<div class="alert alert-danger alert-dismissable"> <?php echo $msg1; ?><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>
								<?php
								}
								?>    
							
							                            
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Sl No.</th>
                                                <th>Name</th>
                                                <th>Department</th>
												<th>User Name</th>	
												<th>Status</th>
												<th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										   <?php 
							                $i	=	0;										
							                while($i < $size) {				
							                ?>						    
                                            <tr>
                                                <td><?php echo $i+'1';?></td>
                                                <td><?php echo $tmplist[$i]["staff_name"];?> </td>
												<td><?php echo $depName = $obj->fetchDepById($tmplist[$i]["dep_id"]);?> </td>
												<td><?php echo $tmplist[$i]["username"];?> </td>
												<td>
									
												<?php if($tmplist[$i]["del_status"]==1){?>
												 <a href="listFaculty.php?cid=<?php echo $tmplist[$i]["staff_id"];?>&action=Active">Active</a>
												<?php }else{?>
												 <a href="listFaculty.php?cid=<?php echo $tmplist[$i]["staff_id"];?>&action=deActive">Deactive</a>
												<?php }
												
												 ?>
												
												
												</td>
									 
									 
                                                <td><a href="editFaculty.php?id=<?php echo $tmplist[$i]["staff_id"]; ?>" class="btn btn-info">Edit <i class="fa fa-edit"></i></a>&nbsp;</td>
                                            </tr>
											<?php
											$i++;
											}
											?>
                                            
                                                                                     
                                        </tbody>
                                        
                                    </table>
                                </div><!-- /.box-body -->
                            </div>

                </section><!-- /.content -->
				
				<!-- data table--->
				
				
				
				
				<!--- /.data table -->
				
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        <script src="js/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
		<!-- DATA TABES SCRIPT -->
        <script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="js/AdminLTE/app.js" type="text/javascript"></script>
		
		<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
       
	   <!-- <script type="text/javascript">
        CKEDITOR.replace(txtaddress);
        </script> -->
		
		<script type="text/javascript">
		
		function askDelete(){
			if(confirm("Do you want to delete this item ? click OK to continue, CANCEL to exit")){
				return true;
			}else{
				return false;
			}
		}

		
		</script>
		
			<script type="text/javascript">
            $(function() {
                $('#example1').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>
        
    </body>
</html>