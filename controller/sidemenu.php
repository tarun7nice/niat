<script src="js/jquery.min.js"></script>
<script language="JavaScript">

jQuery(document).ready(function () {

var location_href=location.href.toLowerCase();
if(location.hash){
	location_href=location.href.split("#");
	}

var current = jQuery("body").find("a").filter(function () { return this.href.toLowerCase() == location_href });


if (current.length) {
	sessionStorage.setItem("url",location_href);
	current.parents("ul, li").addClass("active").add(current.next()).show();
	objParent=current.parents("ul, li").parents("li");
	objParent.addClass("active");
	objParent.children("a").children("span").addClass("open");
	//objParent.add( "span" ).addClass( "selected" );
	}
	
	
else{
	var current = jQuery("body").find("a").filter(function () { return this.href.toLowerCase() == sessionStorage.getItem("url"); });
	current.parents("ul, li").addClass("active").add(current.next()).show();
	objParent=current.parents("ul, li").parents("li");
	objParent.addClass("active");
	objParent.children("a").children("span").addClass("open");
	//objParent.add( "span" ).addClass( "selected" );	
}
});

</script>
<aside class="left-side sidebar-offcanvas">                
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
					<!--
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="img/avatar3.png" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Hello, Jane</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                   -->
				   <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li>
                            <a href="dashboard.php">
                                <i class="fa fa-dashboard"></i> <span>DASHBOARD</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-bookmark-o"></i>
                                <span>MASTERS</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
								<li class="treeview"><a href="#"><i class="fa fa-angle-double-right"></i> Manage Faculty</a>
									<ul class="treeview-menu">
										<li><a href="addFaculty.php" style="color: #00BFFF"><i class="fa fa-angle-double-right"></i> Add Faculty</a></li>	
										<li><a href="listFaculty.php" style="color: #00BFFF"><i class="fa fa-angle-double-right"></i>List Faculty</a></li>		
								   </ul>
								</li>								
							<!--	 <li class="treeview"><a href="#"><i class="fa fa-angle-double-right"></i> Manage Pages</a>
									<ul class="treeview-menu">
										<li><a href="addpages.php"><i class="fa fa-angle-double-right"></i> Add Pages</a></li>		
								   </ul>
								</li>
								
								 <li class="treeview"><a href="#"><i class="fa fa-angle-double-right"></i> Manage Store List</a>
									<ul class="treeview-menu">
										<li><a href="addStoreList.php"><i class="fa fa-angle-double-right"></i> Add Store List</a></li>		
								   </ul>
								</li>
								
								 <li class="treeview"><a href="#"><i class="fa fa-angle-double-right"></i> Manage Checkout</a>
									<ul class="treeview-menu">
										<li><a href="addCheckoutParameters.php"><i class="fa fa-angle-double-right"></i> Checkout Parameters</a></li>		
								   </ul>
								</li>
								
								 <li class="treeview"><a href="#"><i class="fa fa-angle-double-right"></i> Manage Combos</a>
									<ul class="treeview-menu">
										<li><a href="addCombos.php"><i class="fa fa-angle-double-right"></i>Add Combos</a></li>		
								   </ul>
								</li>-->
								
								
							<!--	<li><a href="addmaterialunit.php"><i class="fa fa-angle-double-right"></i> Manage Material Unit</a></li>
								<li><a href="addmaterial.php"><i class="fa fa-angle-double-right"></i> Manage Materials</a></li>
								<li><a href="addMaterialSupplier.php"><i class="fa fa-angle-double-right"></i> Manage Materials Supplier</a></li>
								<li><a href="addtask.php"><i class="fa fa-angle-double-right"></i> Manage Task at site</a></li>
								<li><a href="addtransporttype.php"><i class="fa fa-angle-double-right"></i> Manage Transportation</a></li>-->
								<!--<li><a href="#"><i class="fa fa-angle-double-right"></i> Manage Project</a></li> -->
                            </ul>
                        </li>
						
						<!--<li class="treeview">
                            <a href="#">
                                <i class="fa fa-edit"></i>
                                <span>Customers</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="listCustomers.php"><i class="fa fa-angle-double-right"></i> Manage Customers</a></li>                              
                            </ul>
                        </li>
						
						<li class="treeview">
                            <a href="#">
                                <i class="fa fa-edit"></i>
                                <span>Orders</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="listOrders.php"><i class="fa fa-angle-double-right"></i> Manage Orders</a></li>                              
                            </ul>
                        </li>-->
						
						<!--<li>
                            <a href="addproject.php?tab=tab_1">
                                <i class="fa fa-briefcase"></i> <span>PROJECTS</span>
                            </a>
                        </li>-->
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>