<?php 
class Faculty extends Database_MySql
{
	public $errorMessage;
	public $userId;
	private $utilObject;

	function __construct()
	{
		parent::__construct();
		$this->connect();
		$this->userId	=	null;
		$this->utilObject	=	new Utilities();
		
	}


//Rekha 




		//fetching Department 
		
		 function getDepartment()
		{			
			$query="SELECT * FROM `tbl_department`";			
			$records	=	$this->fetchAll($query);	
			return $records;
		}
		
		
		
		//fetching Department by id
		 function fetchDepById($id)
			{			
				$query="SELECT department_code FROM `tbl_department` where dep_id=?";				
				$param	=	array("i",$id);
				$records	=	$this->fetchAll($query,$param);						
				return $records[0]["department_code"];
			}  	
			
		
		
		
		

	// inserting data to Faculty master	
	
	function addFaculty($values)
		{
		
		$result	=	false;
		$util	=	new Utilities();
		$validator	=	new Validator(array(
												$values["txtName"]=>"Name/EMPTY",											
												$values["txtDepartment"]=>"Department/EMPTY",
												$values["txtusername"]=>"Username/EMPTY",
												$values["txtPass"]=>"Password/EMPTY"
												));
		if($validator->validate())
		{
		
		  $date = date("Y-m-d");
			
				if(!$this->userExist($values["txtusername"])){				
					$password	=	md5($values["txtPass"]);				
					$array=array(
							"staff_name"=>$values["txtName"],							
							"dep_id"=>$values["txtDepartment"],						
							"username"=>$values["txtusername"],
							"password"=>$password,						
							"role_id"=>2																
							);							
					$type	=	"sissi";				
					$this->insert($array,"tbl_staff",$type);
					$result	=	true;
				}else{
					$this->setError("Faculty Already Exist")	;
				}
		}
		else
		{
			$this->setError($validator->getMessage())	;
		}
		return $result;
		
		}
		
		
		
	//chekcing user exists with username	
		
	function userExist($username)
		{
			$result=false;
			$qry	=	"SELECT * FROM `tbl_staff`  where username=?";
			$param	=	array("s",$username);
			$records	=	$this->fetchAll($qry,$param);
			if(count($records)>0){
				$result	=	true;
			}			
			return $result;
		}
		
		
		
			//chekcing user exists with username	
		
	   function chkuserExist($username,$id)
		{
			$result=false;
			$qry	=	"SELECT * FROM `tbl_staff`  where username=? and staff_id!=?";
			$param	=	array("si",$username,$id);
			$records	=	$this->fetchAll($qry,$param);
			if(count($records)>0){
				$result	=	true;
			}			
			return $result;
		}
		
		
		
		//fetching Faculty list with role =2 
	
		 function fetchFaculty()
			{			
				$query="SELECT * FROM `tbl_staff` where role_id=2";	
				$records	=	$this->fetchAll($query);			
				$qury="SELECT COUNT(staff_id) FROM `tbl_staff`  where role_id=2";			
				$total	=	$this->fetchAll($qury);			
				$this->tot=	$total[0]["COUNT(staff_id)"];
				return $records;
			}  
			

      /*  function delStaff($id)
			{
				/*$tname	=	"tbl_staff";
				$condition	=	"staff_id=?";
				$param		=	array("i",$id);
				$this->delete($tname,$condition,$param);*/
		/*		
				$val	=	array("del_status"=>2					
							);								  
				$tname	=	"tbl_staff";
				$cond	=	"staff_id=? ";
				$param	=	array($id);
				$ty		=	"ii";		
				$this->update($val,$tname,$cond,$param,$ty);				
				$result=true;
				
				
			}*/
			
	//function to activate and deactivate staff 1 is active and 2 is deactivate		
			
			
     function makeActive($id,$type)
			{
		
				$val	=	array("del_status"=>$type					
							);								  
				$tname	=	"tbl_staff";
				$cond	=	"staff_id=? ";
				$param	=	array($id);
				$ty		=	"ii";		
				$this->update($val,$tname,$cond,$param,$ty);				
				$result=true;
				
				
			}	
			
				
		//updating Faculty details	
			
			
		function editFaculty($values,$id)
			{
			
			$result= false;
			
		    $validator	=	new Validator(array(
												$values["txtName"]=>"Name/EMPTY",												
												$values["txtDepartment"]=>"Department/EMPTY",
												$values["txtusername"]=>"Username/EMPTY"												
												));
											
			if($validator->validate())
			{
			
		
			if($values["txtPass"]!="") {			
				
				$password		=	md5($values["txtPass"]);				
				
								
				$val	=	array("staff_name"=>$values["txtName"],						
							"dep_id"=>$values["txtDepartment"],						
							"username"=>$values["txtusername"],
							"password"=>$password
							);
								  
				$tname	=	"tbl_staff";
				$cond	=	"staff_id=? ";
				$param	=	array($id);
				$ty		=	"sissi";		
				$this->update($val,$tname,$cond,$param,$ty);
				
				$result=true;
				
				}else {
				 $val	=	array("staff_name"=>$values["txtName"],
								"dep_id"=>$values["txtDepartment"],	
							    "username"=>$values["txtusername"]
							);
				$tname	=	"tbl_staff";
				$cond	=	"staff_id=? ";
				$param	=	array($id);
				$ty		=	"sisi";		
				$this->update($val,$tname,$cond,$param,$ty);
				
				$result=true;
				
				}				
		   }else
			{
				$this->setError($validator->getMessage())	;
			}
		 return $result;		
	}
	
	
	
	
		//fetching Ranks 
		
		 function fetchRanks()
		{			
			$query="SELECT * FROM `tbl_instructor_ranks`";			
			$records	=	$this->fetchAll($query);	
			return $records;
		}
		
		
			
		//fetching Rank by id	
			
		function fetchRankById($id)
		{
			
			$query="SELECT * FROM `tbl_instructor_ranks` where id=?";		
			$param	=	array("i",$id);
			$records	=	$this->fetchAll($query,$param);		
			return $records;
		}	
		
	
			
		//inserting instructor
			
			function addInstructor($values)
		{
		
			$result	=	false;
			$validator	=	new Validator(array(												
												$values["Name"]=>"Name/EMPTY"
												));
					
			 if($validator->validate())
			 {
			    $query="SELECT * FROM `tbl_instructor_ranks` where ranks = ? ";		
				$param	=	array("s",$values["rank"]);
				$records	=	$this->fetchAll($query,$param);	
				
				if(count($records)==0)
				{
					$ranks = strtoupper($values["rank"]);
					$array =array(
					"ranks"=>$ranks
					);
					
					
					$type = "s";
					$this->insert($array,"tbl_instructor_ranks",$type);
			
					$rankId = $this->lastinsertId();
					
					$pNo = $values["personalNo"];
					
					$checkPno = $this->checkpersonalNumber($pNo);
					$pNocount = count($checkPno);
					if($pNocount==0)
					{
					
					//$grade = date('Y-m-d', strtotime( $values['grade'] ));
					$instructorname = strtoupper($values["Name"]);
						$array1	=	array(								
								"instructor_name"=>$instructorname,
								"instructor_rank"=>$rankId,
								"grade"=>$values['grade'],
								"personalno"=>$values["personalNo"],
								"faculty"=>$values["faculty"],
								"aircraft"=>$values["aircraft"],
								"address"=>$values["address"],
								"mobile"=>$values["mobile"]
								);
						 $type	=	"sississs";				
					     $this->insert($array1,"tbl_instructor",$type);	
					}		 
				}
				else
				{
				
				$query="SELECT * FROM `tbl_instructor_ranks` where ranks = ? ";		
				$param	=	array("s",$values["rank"]);
				$records	=	$this->fetchAll($query,$param);	
				
					$pNo = $values["personalNo"];
					
					$checkPno = $this->checkpersonalNumber($pNo);
					$pNocount = count($checkPno);
					if($pNocount==0)
					{
				
				//$grade = date('Y-m-d', strtotime( $values['grade'] ));
				
				$instructorname = strtoupper($values["Name"]);
						$array	=	array(								
								"instructor_name"=>$instructorname,
								"instructor_rank"=>$records[0]["id"],
								"grade"=>$values['grade'],
								"personalno"=>$values["personalNo"],
								"faculty"=>$values["faculty"],
								"aircraft"=>$values["aircraft"],
								"address"=>$values["address"],
								"mobile"=>$values["mobile"]
								);
						 $type	=	"sississs";				
					     $this->insert($array,"tbl_instructor",$type);		
						 
					}	 
						 
				}		
						 				
						$result	=	true;
				
			}
			else
			{
				$this->setError($validator->getMessage());
			}
			return $result;
		}
		
		
		//fetching Instructor 
		
		 function fetchInstructor()
		{			
			$query="SELECT * FROM `tbl_instructor`";			
			$records	=	$this->fetchAll($query);	
			return $records;
		}
		
		//deleting Instructor
		function deleteInstructor($id)
			{		
				$tname	=	"tbl_instructor";
				$condition	=	"instructor_id=?";
				$param		=	array("i",$id);
				$this->delete($tname,$condition,$param);
				
				$tname1	=	"tbl_leave_app_form";
				$condition1	=	"instructor_id=?";
				$param1		=	array("i",$id);
				$this->delete($tname1,$condition1,$param1);							
			}
			
			
		//fetching Instructor by id	
			
		function fetchInstructorByID($id)
		{
			
			$query="SELECT * FROM `tbl_instructor` where instructor_id=?";		
			$param	=	array("i",$id);
			$records	=	$this->fetchAll($query,$param);		
			return $records;
		}	
		
		
		//updating Instructor
		
		function editInstructor($values,$id)
		{
			$result	=	false;
			$validator	=	new Validator(array(												
												$values["Name"]=>"Name/EMPTY"		
												));
					
			 if($validator->validate())
			 {
			 
			 	$query="SELECT * FROM `tbl_instructor_ranks` where ranks = ? ";		
				$param	=	array("s",$values["ranks"]);
				$records	=	$this->fetchAll($query,$param);	
				
				//echo count($records);
				//exit;
				
				if(count($records)==0)
				{
					$ranks = strtoupper($values["ranks"]);
					$array =array(
					"ranks"=>$ranks
					);
					$type = "s";
					$this->insert($array,"tbl_instructor_ranks",$type);
					
					$rankId = $this->lastinsertId();
					
					$pNo = $values["personalNo"];
					
					$checkPno = $this->editcheckpersonalNumber($pNo,$id);
					$pNocount = count($checkPno);
					if($pNocount==0)
					{
					
					//$grade = date('Y-m-d', strtotime( $values['grade'] ));
					$instructorname = strtoupper($values["Name"]);						
							$array	=	array(	
								"instructor_name"=>$instructorname,
								"instructor_rank"=>$rankId,
								"grade"=>$values['grade'],
								"personalno"=>$values["personalNo"],
								"faculty"=>$values["faculty"],
								"aircraft"=>$values["aircraft"],
								"address"=>$values["address"],
								"mobile"=>$values["mobile"]		
								);
							$type	=	"sississsi";						
							
				$tname	=	"tbl_instructor";
				$cond	=	"instructor_id=?";
				$param	=	array($id);
				
				$this->update($array,$tname,$cond,$param,$type);
				$result	=	true;
				
				}
				}
				else
				{
				
				$rankId = $records[0]["id"];
				
					$pNo = $values["personalNo"];
					
					$checkPno = $this->editcheckpersonalNumber($pNo,$id);
					$pNocount = count($checkPno);
					if($pNocount==0)
					{
					//$grade = date('Y-m-d', strtotime( $values['grade'] ));
					$instructorname = strtoupper($values["Name"]);						
							$array	=	array(	
								"instructor_name"=>$instructorname,
								"instructor_rank"=>$rankId,
								"grade"=>$values['grade'],
								"personalno"=>$values["personalNo"],
								"faculty"=>$values["faculty"],
								"aircraft"=>$values["aircraft"],
								"address"=>$values["address"],
								"mobile"=>$values["mobile"]			
								);
							$type	=	"sississsi";						
							
				$tname	=	"tbl_instructor";
				$cond	=	"instructor_id=?";
				$param	=	array($id);
				
				$this->update($array,$tname,$cond,$param,$type);
				$result	=	true;
				}
				}
				
			}
			else
			{
				$this->setError($validator->getMessage());
			}
			return $result;			
							
							
							
		}
		
		
		
		//fetching Course OFFicer with ranks 
		
		 function fetchCourseOfficer()
		{			
			$query="SELECT i.*,r.ranks FROM `tbl_instructor`i join tbl_instructor_ranks r on r.id=i.instructor_rank";			
			$records	=	$this->fetchAll($query);	
			return $records;
		}
		
		
		//fetching Course Instrcutor with out  ranks 
		
		 function fetchCourseInstructor()
		{			
			$query="SELECT i.*,r.ranks FROM `tbl_instructor`i join tbl_instructor_ranks r on r.id=i.instructor_rank where r.ranks!='SLT' and r.ranks!='LT' and r.ranks!='LT CDR' and r.ranks!='CDR'";			
			$records	=	$this->fetchAll($query);	
			return $records;
		}
		
		
		
		//fetching Course Names  
		
		 function fetchCourseNames($name)
		{			
			$query="SELECT course_name FROM `tbl_mip` where course_name LIKE '$name%' ";			
			$records	=	$this->fetchAll($query);	
			return $records;
		}
		
		
		
		//fetching Course Names  
		
		 function fetchCourseNO($name)
		{			
			$query="SELECT course_no FROM `tbl_mip` where course_name LIKE '$name%' ";			
			$records	=	$this->fetchAll($query);	
			return $records[0]["course_no"];
		}
		
		
		//fetching Course Names  
		
		 function fetchCourseNOForWTP($id)
		{			
			$query="SELECT course_no FROM `tbl_mip` where course_id=? ";
			$param	=	array("i",$id);
			$records	=	$this->fetchAll($query,$param);
			return $records[0]["course_no"];
		}
		
		
		
		
		//fetching Course details  
		
		 function fetchCourseDetailsForSubStatus($name)
		{			
			$query="SELECT * FROM `tbl_mip` where course_name LIKE '$name%' ";			
			$records	=	$this->fetchAll($query);	
			return $records;
		}
		
		
		
		//fetching subject details for course  
		
		 function fetchSubListFOrCourse($cid)
		{			
			$query="SELECT * FROM `tbl_course_subjects` where course_id=? ";			
			$param	=	array("i",$cid);
			$records	=	$this->fetchAll($query,$param);
			return $records;
		}
		
		
		
		// Fetch Course by Id
		
		function fetchcourseById($id)
		{
				$query="SELECT * FROM `tbl_mip` where course_id =?";		
				$param	=	array("i",$id);
				$records	=	$this->fetchAll($query,$param);
				return $records;
		}
		
		
			// Fetch sessions taken from wtp_per_date
		
		function fetchwtpSessTaken($id)
		{
		
				$query="SELECT count(wtp_per_date_id) as noofsess FROM `tbl_wtp_per_date` where subject_id =? and approval=2 and day!='Saturday' and date<=now() and status=1";		
				$param	=	array("i",$id);
				$records	=	$this->fetchAll($query,$param);
				
				return $records;
		}
		
		
			// Fetch sessions taken from wtp_per_date
		
		function fetchwtpSessTakenDays($id)
		{
	
				$qury="SELECT * FROM `tbl_wtp_per_date` where subject_id =? and approval=2 and day!='Saturday' and date<=now() and status=1 group by day";		
				$parm	=	array("i",$id);
				$recrds	=	$this->fetchAll($qury,$parm);
				
				return $recrds;
		}
		
		
		
			//inserting MIP 
			
			function addMIP($values,$fuserId)
		{
		
			$result	=	false;
			$validator	=	new Validator(array(												
												$values["Name"]=>"Name/EMPTY",
												$values["courseno"]=>"courseno/EMPTY",
												$values["strength"]=>"strength/EMPTY|NUMBER",
												$values["courseofficer"]=>"courseofficer/EMPTY",
												$values["courseInstructor"]=>"courseInstructor/EMPTY",
												$values["faculty"]=>"faculty/EMPTY"											
												));
					
			 if($validator->validate())
			 {
			 
				 
				$query="SELECT * FROM `tbl_mip` where course_name = ? ";		
				$param	=	array("s",$values["Name"]);
				$records	=	$this->fetchAll($query,$param);		
				
				
				
				if(count($records)==0){
			 
			 	$coursename = strtoupper($values["Name"]);
			    $startDate = date('Y-m-d', strtotime( $values['startdate'] ));
				$endDate = date('Y-m-d', strtotime( $values['enddate'] ));
			    //inserting basic info about Course
				
						$array	=	array(								
								"course_name"=>$coursename,
								"course_no"=>$values["courseno"],
								"start_date"=>$startDate,
								"end_date"=>$endDate,
								"strength"=>$values["strength"],
								"faculty_id"=>$values["faculty"],
								"course_officer"=>$values["courseofficer"],
								"course_instructor"=>$values["courseInstructor"]
								);	
						 $type	=	"ssssiiii";				
					     $this->insert($array,"tbl_mip",$type);	
						 
						
						//fetching last insert hotel id
						 
						 $lid = $this->lastInsertId();
						 
						 //counter value of loop for seasons
						 
						 $counter = $values["counter"];
						 
						
						//seasons details inserting 
						 for($t=0;$t<$counter;$t++){
						 
						 
						 if($values["subject".$t]!=""){
						 
						 $sub = strtoupper($values["subject".$t]);
							 $array	=	array(		
							 		"course_id"=>$lid,				
									"subject_name"=>$sub,
									"sessions"=>$values["session".$t],
									"phase"=>$values["phase".$t],
									"stream"=>$values["stream".$t]								
									);	
							 $type	=	"isiss";				
							 $this->insert($array,"tbl_course_subjects",$type);	
							 
						 }
						 
						 }
						 					
						$result	=	true;
						
						
						}else{
						
						
						$coursename = strtoupper($values["Name"]);
						$startDate = date('Y-m-d', strtotime( $values['startdate'] ));
						$endDate = date('Y-m-d', strtotime( $values['enddate'] ));
						
						   //updating basic info about Course
				
						$array	=	array(								
								"course_name"=>$coursename,
								"course_no"=>$values["courseno"],
								"start_date"=>$startDate,
								"end_date"=>$endDate,
								"strength"=>$values["strength"],								
								"course_officer"=>$values["courseofficer"],
								"course_instructor"=>$values["courseInstructor"],
								"faculty_id"=>$values["faculty"]
								);	
						    $type	=	"ssssiiiii";				
					     	$tname	=	"tbl_mip";
							$cond	=	"course_id=?";
							$param	=	array($records[0]["course_id"]);
							
							$this->update($array,$tname,$cond,$param,$type);
							
							
							
					//	$tname	=	"tbl_course_subjects";
					//	$condition	=	"course_id=?";
					//	$param		=	array("i",$records[0]["course_id"]);
					//	$this->delete($tname,$condition,$param);		
							
							
						
						 //counter value of loop for seasons
						 
						 $counter = $values["counter"];
						 
						
						//seasons details inserting 
						 for($t=0;$t<$counter;$t++){
						 
						 
						 if($values["subjectId".$t]==""){
							 if($values["subject".$t]!=""){
							 $sub = strtoupper($values["subject".$t]);
								 $array	=	array(		
										"course_id"=>$records[0]["course_id"],				
										"subject_name"=>$sub,
										"sessions"=>$values["session".$t],
										"phase"=>$values["phase".$t],
										"stream"=>$values["stream".$t]								
										);	
								 $type	=	"isiss";	
								 $this->insert($array,"tbl_course_subjects",$type);	
								 }
						 } else{
						 
						  if($values["subject".$t]!=""){
						   $sub = strtoupper($values["subject".$t]);
							 $array	=	array(		
							 		"course_id"=>$records[0]["course_id"],				
									"subject_name"=>$sub,
									"sessions"=>$values["session".$t],
									"phase"=>$values["phase".$t],
									"stream"=>$values["stream".$t]								
									);	
							 $type	=	"isissi";	
							// $this->insert($array,"tbl_course_subjects",$type);	
							 
							$tname	=	"tbl_course_subjects";
							$cond	=	"sub_id=?";
							$param	=	array($values["subjectId".$t]);
							
							$this->update($array,$tname,$cond,$param,$type);
							 
							} 
						 
						 }
						 
						 }
						 
						 
						 
						 
						 
						
						
						}
						
						$result	=	true;					
						
			}
			else
			{
				$this->setError($validator->getMessage());
			}
			return $result;
		}
		
		
			//fetching Course Info  
		
		 function fetchCourseInfo($name)
		{			
			$query="SELECT * FROM `tbl_mip` where course_name = ? or course_no=?";		
			$param	=	array("ss",$name,$name);
			$records	=	$this->fetchAll($query,$param);		
			return $records;
		}
		
			//fetching Subject Info  
		
		 function fetchSubjectInfo($id)
		{			
			$query="SELECT * FROM `tbl_course_subjects` where course_id = ? ";		
			$param	=	array("i",$id);
			$records	=	$this->fetchAll($query,$param);		
			return $records;
		}
		
			//fetching Subject Info  
		
		 function fetchSubjectInfoEdit($id,$insid)
		{			
			$query="SELECT * FROM `tbl_course_subjects` where course_id = ? and sub_id NOT IN (SELECT subjectId FROM tbl_instructorcourse where courseId=? and instructorId=?)";		
			$param	=	array("iii",$id,$id,$insid);
			$records	=	$this->fetchAll($query,$param);		
			return $records;
		}
		
			//fetching Subject by Id  
		
		 function fetchSubject($id)
		{			
			$query="SELECT * FROM `tbl_course_subjects` where sub_id =?";		
			$param	=	array("i",$id);
			$records	=	$this->fetchAll($query,$param);		
			return $records;
		}
		
	// Delete subject
		
		function deleteSubject($subId)
	{
	            $tname	=	"tbl_course_subjects";
				$condition	=	"sub_id=?";
				$param		=	array("i",$subId);
				$this->delete($tname,$condition,$param);
	}
	
	// Fetch MIP
	
	function fetchMIP()
	{
		    $query="SELECT * FROM `tbl_mip`";	
			$records	=	$this->fetchAll($query);		
			return $records;
	}
	// Fetch MIP Percentage
	
	function fetchMIPPercentage()
	{
		    $query="SELECT * FROM (SELECT *,  round(( SPENT/DUR * 100 ),2) AS PERCENT FROM ( SELECT course_name as COURSE, DATEDIFF(`end_date`, `start_date`) as DUR, DATEDIFF(CURDATE(), `start_date`) as SPENT FROM `tbl_mip`) as course ) as course where PERCENT < 100";	
			$records	=	$this->fetchAll($query);		
			return $records;
	}
	
	function fetchWTPByDate($wtpDate)
	{
// 		echo $wtpDate;
		$query="SELECT wtp.wtp_id,mip.course_name, mip.course_no, 
				DATE_FORMAT(mip.start_date,'%d.%m.%y') as start_date,
				DATE_FORMAT(mip.end_date,'%d.%m.%y') as end_date, mip.strength,
				CEIL(DATEDIFF(mip.end_date, mip.start_date)/7) as full_week,
CEIL(DATEDIFF(wtp.end_date, mip.start_date)/7) as current_week,
class.class_room,
coff.instructor_name as course_off, coff_rank.ranks as course_off_rank, cins.instructor_name as course_ins, cins_rank.ranks as course_ins_rank
FROM tbl_wtp as wtp 
inner join tbl_mip as mip on mip.course_id = wtp.course_id 
left join tbl_class_room as class on wtp.classroomid = class.class_room_id
left join tbl_instructor as coff on coff.instructor_id = mip.course_officer
left join tbl_instructor_ranks as coff_rank on coff.instructor_rank = coff_rank.id
left join tbl_instructor as cins on cins.instructor_id = mip.course_instructor
left join tbl_instructor_ranks as cins_rank on cins.instructor_rank = cins_rank.id
where wtp.start_date <= ? and wtp.end_date >= ?";
		
		$param	=	array("ss",$wtpDate,$wtpDate);
		$records	=	$this->fetchAll($query,$param);
		return $records;
	}
	
	function fetchMIPwithWTP($weekStartDate, $weekEndDate, $depId)
	{
		// 		echo $wtpDate;
		$query="SELECT wtp.wtp_id,mip.course_id,mip.course_name, 
				mip.course_no,mip.faculty_id, dept.department_code,
				mip.start_date,
				mip.end_date,
				CEIL(DATEDIFF(mip.end_date, mip.start_date)/7) as full_week,
CEIL(DATEDIFF(?, mip.start_date)/7) as current_week
FROM tbl_mip as mip
left join tbl_wtp as wtp on mip.course_id = wtp.course_id 
				and wtp.start_date = ? #week start date
				and wtp.end_date = ? #week end date
left join tbl_department as dept on dept.dep_id = mip.faculty_id
where mip.start_date <= ? #week end date
and mip.end_date >= ? #week start date
and dept.dep_id = ?				
				";
	
		$param	=	array("sssssi",$weekEndDate, $weekStartDate, $weekEndDate,$weekEndDate, $weekStartDate,$depId);
		$records	=	$this->fetchAll($query,$param);
		return $records;
	}
	
	function fetchWTPInfoById($wtpId)
	{
		
		$query="SELECT wtp.session, ins_rank.ranks, ins.Instructor_name, sub.subject_name FROM tbl_wtp_per_date as wtp 
left join tbl_instructor as ins on ins.instructor_id = wtp.Instructor_Id
left join tbl_instructor_ranks as ins_rank on ins.instructor_rank = ins_rank.id
left join tbl_course_subjects as sub on sub.sub_id = wtp.subject_id
where wtp_id = ? and day = 'Monday' order by session";
	
		$param	=	array("i",$wtpId);
		$records	=	$this->fetchAll($query,$param);
		return $records;
	}
	
	function fetchLeaveHistorybyId($insId)
	{
	
		$query="SELECT *
FROM `tbl_leave_app_form`
where instructor_id = ?";
	
		$param	=	array("i",$insId);
		$records	=	$this->fetchAll($query,$param);
		return $records;
	}
	
	
	// Fetch faculty by id
	
	function fetchFacultybyId($facId)
	{
			$query="SELECT * FROM `tbl_roles` where role_id = ? ";		
			$param	=	array("i",$facId);
			$records	=	$this->fetchAll($query,$param);		
			return $records; 
	}
	
	// Delete course
		
		function deleteCourse($couid)
	{
	            $tname	=	"tbl_mip";
				$condition	=	"course_id=?";
				$param		=	array("i",$couid);
				$this->delete($tname,$condition,$param);
	}
	
	// Fetch Rank by name
	function fetchRanksbyName($rank)
		{
			$query="SELECT ranks FROM `tbl_instructor_ranks` where ranks LIKE '$rank%' ";			
			$records	=	$this->fetchAll($query);	
			return $records;
		}	
		
		
		function fetchFacultybyDept()
		{
			$query = "SELECT s. * , d. * FROM tbl_staff s JOIN tbl_department d ON s.dep_id = d.dep_id WHERE role_id =2";
			$records	=	$this->fetchAll($query);	
			return $records;
		}
		
		
		
			//inserting Class Room
		
		function addClassRoom($values)
		{
		$result	=	false;
		//$util	=	new Utilities();
		$validator	=	new Validator(array(
												$values["Name"]=>"Name/EMPTY",
												$values["strength"]=>"strength/EMPTY|NUMBER"																	
												));
		if($validator->validate())
		{
		
		$name = strtoupper($values["Name"]);
			
				$array=array(
				            "class_room"=>$name,						
							"strength"=>$values["strength"],						
							"facilities"=>$values["facilities"]	
							);							
					$type	=	"sis";				
					$this->insert($array,"tbl_class_room",$type);
					$result	=	true;
					
					$lid = $this->lastInsertId();
				
		}
		else
		{
			$this->setError($validator->getMessage())	;
		}
		return $result;
		
		}
		
		
			
	    //fetching ClassRoom 
		
		function fetchClassRoom()
		{	
			$query="SELECT * FROM `tbl_class_room`";		
			$records	=	$this->fetchAll($query);		
			return $records;
		}

		
		//deleting ClassRoom 
		
		 function deleteClassRoom($id)
			{		
				
				$tname	=	"tbl_class_room";
				$condition	=	"class_room_id=?";
				$param		=	array("i",$id);
				$this->delete($tname,$condition,$param);
							
			}
			
			
		//fetch ClassRoom by id 
		
			function fetchClassRoomByID($id)
		{			
			$query="SELECT * FROM `tbl_class_room` where class_room_id=?";		
			$param	=	array("i",$id);
			$records	=	$this->fetchAll($query,$param);		
			return $records;
		}	
		
		//updating classroom
		
		function editClassRoom($values,$id)
		{		
			$result	=	false;
			$validator	=	new Validator(array(												
												$values["Name"]=>"Name/EMPTY",
												$values["strength"]=>"strength/EMPTY|NUMBER"												
												));
					
			 if($validator->validate())
			 {
				
				$name = strtoupper($values["Name"]);
				
				$query="SELECT * FROM `tbl_class_room` where class_room_id=?";		
				$param	=	array("i",$id);
				$records	=	$this->fetchAll($query,$param);	
				$alrName = $records[0]["class_room"];
				
				$qry="SELECT * FROM `tbl_class_room` where class_room=? and class_room!=?";		
				$pram	=	array("ss",$name,$alrName);
				$rcrds	=	$this->fetchAll($qry,$pram);
				
				if(count($rcrds)==0)
				{
				
					$array=array(
						    "class_room"=>$name,						
							"strength"=>$values["strength"],						
							"facilities"=>$values["facilities"]													
							);
					
					$type	=	"sisi";									
							
				$tname	=	"tbl_class_room";
				$cond	=	"class_room_id=?";
				$param	=	array($id);
				
				$this->update($array,$tname,$cond,$param,$type);
				$result	=	true;
				
				}
				else
				{
					$this->setError('Class name already in use');
				}
			}
			else
			{
				$this->setError($validator->getMessage());
			}
			return $result;			
							
							
							
		}	
		
		
		//inserting Leave App Form
		
		function addLeaveAppForm($values)
		{
		$result	=	false;
		//$util	=	new Utilities();
		$validator	=	new Validator(array(
												$values["courseInstructor"]=>"courseInstructor/EMPTY"												
												));
		if($validator->validate())
		{
		
		        $startDate = date('Y-m-d', strtotime( $values['startdate'] ));
				$endDate = date('Y-m-d', strtotime( $values['enddate'] ));
			
				$array=array(
				            "instructor_id"=>$values["courseInstructor"],						
							"start_date"=>$startDate,						
							"end_date"=>$endDate,
							"reason"=>$values["reason"]	
							);							
					$type	=	"isss";				
					$this->insert($array,"tbl_leave_app_form",$type);
					$result	=	true;					
				
		}
		else
		{
			$this->setError($validator->getMessage())	;
		}
		return $result;
		
		}
		
		
			
	    //fetching Leave App Form
		
		function fetchLeaveAppForm()
		{	
			$query="SELECT * FROM `tbl_leave_app_form`";		
			$records	=	$this->fetchAll($query);		
			return $records;
		}

		
		//deleting Leave App Form 
		
		 function deleteLeaveAppForm($id)
			{		
				
				$tname	=	"tbl_leave_app_form";
				$condition	=	"app_id=?";
				$param		=	array("i",$id);
				$this->delete($tname,$condition,$param);
							
			}
			
			
		//fetch Leave App Form  by id 
		
			function fetchLeaveAppFormByID($id)
		{			
			$query="SELECT * FROM `tbl_leave_app_form` where app_id=?";		
			$param	=	array("i",$id);
			$records	=	$this->fetchAll($query,$param);		
			return $records;
		}	
		
		//updating Leave App Form 
		
		function editLeaveAppForm($values,$id)
		{		
			$result	=	false;
			$validator	=	new Validator(array(												
												$values["courseInstructor"]=>"courseInstructor/EMPTY"							
												));
					
			 if($validator->validate())
			 {		
			 
			 
			    $startDate = date('Y-m-d', strtotime( $values['startdate'] ));
				$endDate = date('Y-m-d', strtotime( $values['enddate'] ));		
		
					$array=array(
						    "instructor_id"=>$values["courseInstructor"],						
							"start_date"=>$startDate,						
							"end_date"=>$endDate,
							"reason"=>$values["reason"]												
							);
					
					$type	=	"isssi";									
							
				$tname	=	"tbl_leave_app_form";
				$cond	=	"app_id=?";
				$param	=	array($id);
				
				$this->update($array,$tname,$cond,$param,$type);
				$result	=	true;
				
			}
			else
			{
				$this->setError($validator->getMessage());
			}
			return $result;			
							
							
							
		}
		
		
			// Check personal number
	
	function checkpersonalNumber($pNo)
	{
		$query ="SELECT * from `tbl_instructor` where personalno=?";
		$param	=	array("s",$pNo);
		$records	=	$this->fetchAll($query,$param);		
		return $records;
	}
		
	function editcheckpersonalNumber($pNo,$id)				
		{
		$query ="SELECT * from `tbl_instructor` where personalno=? and instructor_id!=?";
		$param	=	array("si",$pNo,$id);
		$records	=	$this->fetchAll($query,$param);		
		return $records;
	}					
	
	// Add WTP
	
	function assignCourse($values)
	{
		
			$result	=	false;
			
			
			$validator	=	new Validator(array(												
			$values["instructor"]=>"Instructor/EMPTY",
			$values["course"]=>"Course/EMPTY"));
			
			if($validator->validate())
			{
			
			$test = $values["right"];
			
			$tname	=	"tbl_instructorcourse";
			$condition	=	"instructorId=?";
			$param		=	array("i",$values["instructor"]);
			$this->delete($tname,$condition,$param);
			
			
			for($i=0;$i<count($values["right"]);$i++){	
			
			$query="SELECT course_id FROM `tbl_course_subjects` where sub_id=?";			
			$param	=	array("i",$test[$i]);
			$records	=	$this->fetchAll($query,$param);		
			
			/*
			$qury="SELECT prj_mark_id FROM `m_project_marketing` where activity_id=? and project_id=? and cate_id=?";			
			$param	=	array("iii",$test[$i],$values["project"],$records[0]["category_id"]);
			$recds	=	$this->fetchAll($qury,$param);	
			*/
						
			$array	=	array(								
			"instructorId"=>$values["instructor"],
			"courseId"=>$records[0]["course_id"],
			"subjectId"=>$test[$i]								
			);	
			$type	=	"iii";				
			$this->insert($array,"tbl_instructorcourse",$type);	
			
			}					
			$result	=	true;
			
			}
			else
			{
			$this->setError($validator->getMessage());
			}
			return $result;
		
	}		
	
	// Fetch Instructor from wtp
	
	function fetchwtpIns($insid)
	{
		$query ="SELECT * from `tbl_instructorcourse` where instructorId=?";
		$param	=	array("i",$insid);
		$records	=	$this->fetchAll($query,$param);		
		return $records;
	}	
	
	function fetchwtpCou($insid,$couid)
	{
		$query ="SELECT * from `tbl_instructorcourse` where instructorId=? and courseId=?";
		$param	=	array("ii",$insid,$couid);
		$records	=	$this->fetchAll($query,$param);		
		return $records;
	}	
	
	
	function fetchwtpCouEdit($insid)
	{
		$query ="SELECT * from `tbl_instructorcourse` where instructorId=?";
		$param	=	array("i",$insid);
		$records	=	$this->fetchAll($query,$param);		
		return $records;
	}	
	
	
	
	// delete wtp
	
	function deleteWTP($vid)
	{
				$tname	=	"tbl_instructorcourse";
				$condition	=	"id=?";
				$param		=	array("i",$vid);
				$this->delete($tname,$condition,$param);
	}	
	
	// Edit assigned subjects
	
	function editassignCourse($values,$insid,$couid)
	{
	
		$result	=	false;
			
			
			$validator	=	new Validator(array(												
			$values["instructor"]=>"Instructor/EMPTY",
			$values["course"]=>"Course/EMPTY"));
			
			if($validator->validate())
			{
			
			$test = $values["right"];
			
			$tname	=	"tbl_instructorcourse";
			$condition	=	"instructorId=?";
			$param		=	array("i",$values["instructor"]);
			$this->delete($tname,$condition,$param);
			
			for($i=0;$i<count($values["right"]);$i++){	
			
			
			$query ="SELECT course_id from `tbl_course_subjects` where sub_id=?";
			$param	=	array("i",$test[$i]);
			$records	=	$this->fetchAll($query,$param);		
		
			
			$array	=	array(								
			"instructorId"=>$values["instructor"],
			"courseId"=>$records[0]["course_id"],
			"subjectId"=>$test[$i]								
			);	
			$type	=	"iii";				
			$this->insert($array,"tbl_instructorcourse",$type);
				
			//$this->insert($array,"tbl_instructorcourse",$type);	
			
			}					
			$result	=	true;
			
			}
			else
			{
			$this->setError($validator->getMessage());
			}
			return $result;

	
	}
	
	//Fetch sub id from list
	
	function fetchsubfromList($subId)
	{
		$query ="SELECT * from `tbl_instructorcourse` where subjectId=?";
		$param	=	array("i",$subId);
		$records	=	$this->fetchAll($query,$param);		
		return $records;
	}	
	
	function checkclassname($name)
	{
		$query ="SELECT * from `tbl_class_room` where class_room=?";
		$param	=	array("s",$name);
		$records	=	$this->fetchAll($query,$param);		
		return $records;
	}
	
	function editcheckclassname($name,$new)
	{
		$query ="SELECT * from `tbl_class_room` where class_room=? and class_room!=?";
		$param	=	array("ss",$name,$new);
		$records	=	$this->fetchAll($query,$param);		
		return $records;
	}
	
	function fetchInsbyac($airc)
	{
		$query="SELECT * FROM `tbl_instructor` where aircraft=?";			
		$param	=	array("s",$airc);
		$records	=	$this->fetchAll($query,$param);
		return $records;
	}
		
		
	function fetchDepid($facultyId)
	{
		$query="SELECT * FROM `tbl_staff` where role_id=?";			
		$param	=	array("i",$facultyId);
		$records	=	$this->fetchAll($query,$param);
		return $records;
	}	
		
	function fetchLeavebyDate($sd,$ed)
	{
		$query="SELECT * FROM `tbl_leave_app_form` where start_date <= '$ed' AND end_date >= '$sd'";            
        //$param    =    array("ss",$sd,$ed);
        $records    =    $this->fetchAll($query);
        return $records;
	}
	
	
	
	
	
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		//inserting Products
		
		function addProducts($values,$imagepath)
		{
		$result	=	false;
		//$util	=	new Utilities();
		$validator	=	new Validator(array(
												$values["Name"]=>"Name/EMPTY",
												$values["Category"]=>"Category/EMPTY",
												$values["price"]=>"price/EMPTY",												
												$values["vegornonveg"]=>"vegornonveg/EMPTY"											
												));
		if($validator->validate())
		{
			
				$array=array(
				            "prod_image"=>$imagepath,						
							"product_name"=>$values["Name"],						
							"cat_id"=>$values["Category"],							
							"product_description"=>$values["description"],
							"price"=>$values["price"],							
							"veg_or_nonveg"=>$values["vegornonveg"]										
							);							
					$type	=	"ssisss";				
					$this->insert($array,"tbl_products",$type);
					$result	=	true;
					
					$lid = $this->lastInsertId();
				
		}
		else
		{
			$this->setError($validator->getMessage())	;
		}
		return $lid;
		
		}
		
		
			
	    //fetching Products 
		
		function fetchProducts()
		{	
			$query="SELECT * FROM `tbl_products`";		
			$records	=	$this->fetchAll($query);		
			return $records;
		}

		
		//deleting Products 
		
		 function deleteProducts($id)
			{		
				$query="SELECT prod_image FROM `tbl_products` where prod_id=?";		
				$param	=	array("i",$id);
				$records	=	$this->fetchAll($query,$param);						
				
				$tname	=	"tbl_products";
				$condition	=	"prod_id=?";
				$param		=	array("i",$id);
				$this->delete($tname,$condition,$param);	
				return $records[0]["prod_image"];						
			}
			
		//fetching previous Products image	
			
			function getPrevimageForProducts($id)
		{
			$query="SELECT prod_image FROM `tbl_products` where prod_id=?";		
			$param	=	array("i",$id);
			$records	=	$this->fetchAll($query,$param);		
			return $records[0]["prod_image"];
		}		
			
			
		//fetch Products by id 
		
			function fetchProductsByID($id)
		{			
			$query="SELECT * FROM `tbl_products` v join `tbl_category` c on v.cat_id = c.cat_id where prod_id=?";		
			$param	=	array("i",$id);
			$records	=	$this->fetchAll($query,$param);		
			return $records;
		}	
		
		//updating Products
		
		function editProducts($values,$id,$imagepath)
		{		
			$result	=	false;
			$validator	=	new Validator(array(												
												$values["Name"]=>"Name/EMPTY",
												$values["Category"]=>"Category/EMPTY",
												$values["price"]=>"price/EMPTY",												
												$values["vegornonveg"]=>"vegornonveg/EMPTY"													
												));
					
			 if($validator->validate())
			 {
				
					$array=array(
							"prod_image"=>$imagepath,						
							"product_name"=>$values["Name"],						
							"cat_id"=>$values["Category"],							
							"product_description"=>$values["description"],
							"price"=>$values["price"],							
							"veg_or_nonveg"=>$values["vegornonveg"]																
							);
					
					$type	=	"ssisssi";									
							
				$tname	=	"tbl_products";
				$cond	=	"prod_id=?";
				$param	=	array($id);
				
				$this->update($array,$tname,$cond,$param,$type);
				$result	=	true;
				
			}
			else
			{
				$this->setError($validator->getMessage());
			}
			return $result;			
							
							
							
		}	
		
		
	//  Insert Pages
	
	function addpages($values)
	{
	   $result	=	false;
			$validator	=	new Validator(array(												
										
												$values["txtpage"]=>"Select Page/EMPTY",
												$values["txtdescription"]=>"Description/EMPTY"
																						
												));
					
			 if($validator->validate())
			 {
			 
						$array	=	array(								
							
								"page_name"=>$values["txtpage"],
								"page_description"=>$values["txtdescription"]		
																								
								);	
						 $type	=	"ss";				
					     $this->insert($array,"tbl_pages",$type);						
						$result	=	true;
						
					
				
			}
			else
			{
				$this->setError($validator->getMessage());
			}
			return $result;
	}	
	
	// Fetch all page List
	
	function getAllPages()
	{
	            $query="SELECT * FROM `tbl_pages`  ";				
				$records	=	$this->fetchAll($query);			
				return $records;
	}
	
	// Delete Pages
	
	function deletePages($pageid)
	{
	            $tname	=	"tbl_pages";
				$condition	=	"page_id=?";
				$param		=	array("i",$pageid);
				$this->delete($tname,$condition,$param);
	}
	
	//  Update Pages
	
	function editpages($values,$pageId)
	{
	    $result	=	false;
			$validator	=	new Validator(array(												
											    $values["txtpage"]=>"Select Page/EMPTY",
												$values["txtdescription"]=>"Description By/EMPTY"							
												));
					
			 if($validator->validate())
			 {
				
						
											
							$array	=	array(	
								"page_name"=>$values["txtpage"],
								"page_description"=>$values["txtdescription"]	
								);
							$type	=	"ssi";						
							
				$tname	=	"tbl_pages";
				$cond	=	"page_id=?";
				$param	=	array($pageId);
				
				$this->update($array,$tname,$cond,$param,$type);
				$result	=	true;
			}
			else
			{
				$this->setError($validator->getMessage());
			}
			return $result;
	}
		
	// Fetch pages for edit
	
	function getPagesForEdit($pageId)
	{
	            $query="SELECT * FROM `tbl_pages` where page_id=?  ";	
				$param	=	array("i",$pageId);			
				$records	=	$this->fetchAll($query,$param);			
				return $records;
	}
	
	// Fetch Page names
	
	function getPageName($pageName)
	{
	            $query="SELECT * FROM `tbl_pages` where page_name=?  ";	
				$param	=	array("s",$pageName);			
				$records	=	$this->fetchAll($query,$param);			
				return $records;
	}	
	
	// Fetch peage names except the selected one
	
	function getEditPageName($pageName,$selName)
	{
	            $query="SELECT * FROM `tbl_pages` where page_name=? and page_name!=? ";	
				$param	=	array("ss",$pageName,$selName);			
				$records	=	$this->fetchAll($query,$param);			
				return $records;
	}
	
	
	
				
		//inserting storelist
			
			function addStoreList($values)
		{
		
			$result	=	false;
			$validator	=	new Validator(array(												
												$values["txtName"]=>"Location/EMPTY",
												$values["address"]=>"Address/EMPTY"
												));
					
			 if($validator->validate())
			 {
				
						$array	=	array(								
								"store_location"=>$values["txtName"],
								"store_address"=>$values["address"]							
								);	
						 $type	=	"ss";				
					     $this->insert($array,"tbl_store_list",$type);						
						$result	=	true;
				
			}
			else
			{
				$this->setError($validator->getMessage());
			}
			return $result;
		}
		
		
		//fetching StoreList 
		
		 function fetchStoreList()
		{			
			$query="SELECT * FROM `tbl_store_list`";			
			$records	=	$this->fetchAll($query);	
			return $records;
		}
		
		//deleting StoreList
		function deleteStoreList($id)
			{		
				$tname	=	"tbl_store_list";
				$condition	=	"store_id=?";
				$param		=	array("i",$id);
				$this->delete($tname,$condition,$param);							
			}
			
			
		//fetching StoreList by id	
			
		function fetchStoreListByID($id)
		{
			
			$query="SELECT * FROM `tbl_store_list` where store_id=?";		
			$param	=	array("i",$id);
			$records	=	$this->fetchAll($query,$param);		
			return $records;
		}	
		
		
		//updating StoreList
		
		function editStoreList($values,$id)
		{
			$result	=	false;
			$validator	=	new Validator(array(												
												$values["txtName"]=>"Location/EMPTY",
												$values["address"]=>"Address/EMPTY"					
												));
					
			 if($validator->validate())
			 {
				
											
							$array	=	array(	
								"store_location"=>$values["txtName"],
								"store_address"=>$values["address"]		
								);
							$type	=	"ssi";						
							
				$tname	=	"tbl_store_list";
				$cond	=	"store_id=?";
				$param	=	array($id);
				
				$this->update($array,$tname,$cond,$param,$type);
				$result	=	true;
				
			}
			else
			{
				$this->setError($validator->getMessage());
			}
			return $result;			
							
							
							
		}	
		
		
	
	//updating CheckoutParameters
	
	function addCheckoutParameters($values)
		{
		
		$result	=	false;
		
		
				$array=array(
							"delivery_charge"=>$values["deliverycharge"],
							"loyality_point"=>$values["loyalitypoint"],
							"loyality_charge"=>$values["loyalitycharge"],
							"redeem_charge"=>$values["redeemcharge"]																					
							);							
						$tname	=	"tbl_checkout_parameters";
						$cond	=	"id=?";
						$param	=	array(1);
						$ty		=	"ssssi";		
						$this->update($array,$tname,$cond,$param,$ty);
						$result	=	true;
					
			
		      return $result;
		
		} 	
		
		
		//fetching CheckoutParameters
		
			 function fetchCheckoutParameters()
			{			
				$query="SELECT * FROM `tbl_checkout_parameters` where id=?";				
				$param	=	array("i",1);
				$records	=	$this->fetchAll($query,$param);				
				return $records;
			}
			
			
			
			// Add Combos
	
	function addCombos($values)	
	{
	    
		$result	=	false;
		//$util	=	new Utilities();
		$validator	=	new Validator(array(
												$values["txtcategory1"]=>"Category 1/EMPTY",
												$values["txtcategory2"]=>"Category 2/EMPTY",
												$values["txtcategory3"]=>"Category 3/EMPTY",
												$values["txtamount"]=>"Amount/EMPTY"											
												));
		if($validator->validate())
		{
			
				$array=array(						
							"cat_id"=>$values["txtcategory1"],
							"sec_cat_id"=>$values["txtcategory2"],	
							"thi_cat_id"=>$values["txtcategory3"],
							"amount_reduce"=>$values["txtamount"]				
							);							
					$type	=	"iiis";				
					$this->insert($array,"tbl_combos",$type);
					$result	=	true;
				
		}
		else
		{
			$this->setError($validator->getMessage())	;
		}
		
		
	}
		
	//fetching Combos 
		
		function fetchCombos()
		{	
			$query="SELECT * FROM `tbl_combos`";		
			$records	=	$this->fetchAll($query);		
			return $records;
		}
		
	//fetch Combos by id 
		
			function fetchCombosByID($id)
		{			
			$query="SELECT * FROM `tbl_combos` where combo_id=?";		
			$param	=	array("i",$id);
			$records	=	$this->fetchAll($query,$param);		
			return $records;
		}	

		
	//deleting Combos 
		
		 function deleteCombos($id)
			{	
				$tname	=	"tbl_combos";
				$condition	=	"combo_id=?";
				$param		=	array("i",$id);
				$this->delete($tname,$condition,$param);				
			}
			
	// Update Combos
	
	function editCombos($values,$id)
		{
			$result	=	false;
			$validator	=	new Validator(array(												
												$values["txtcategory1"]=>"Category 1/EMPTY",
												$values["txtcategory2"]=>"Category 2/EMPTY",
												$values["txtcategory3"]=>"Category 3/EMPTY",
												$values["txtamount"]=>"Amount/EMPTY"				
												));
					
			 if($validator->validate())
			 {
				
											
							$array	=	array(	
							"cat_id"=>$values["txtcategory1"],
							"sec_cat_id"=>$values["txtcategory2"],	
							"thi_cat_id"=>$values["txtcategory3"],
							"amount_reduce"=>$values["txtamount"]		
								);
							$type	=	"iiiii";						
							
				$tname	=	"tbl_combos";
				$cond	=	"combo_id=?";
				$param	=	array($id);
				
				$this->update($array,$tname,$cond,$param,$type);
				$result	=	true;
				
			}
			else
			{
				$this->setError($validator->getMessage());
			}
			return $result;			
							
							
							
		}
		
		
	// Fetch Customers
	
	function fetchCustomers()
	{
	        $query="SELECT * FROM `tbl_customer` order by cust_id DESC";
			$records	=	$this->fetchAll($query);		
			return $records;
	}	
	
 // Fetch Customer Details
 
    function fetchCustDetById($id)
	{  
	        $query="SELECT * FROM `tbl_cust_delivery_address` where cust_id=?";		
			$param	=	array("i",$id);
			$records	=	$this->fetchAll($query,$param);		
			return $records;
	}	
	
	
	
		
	// Fetch Orders
	
	function fetchOrders()
	{
	        $query="SELECT o.*,c.name FROM `tbl_order` o join tbl_customer c on c.cust_id=o.cust_id order by o.order_id DESC";
			$records	=	$this->fetchAll($query);		
			return $records;
	}	
	
	
	 // Fetch Product Details by order id
 
    function fetchProDetById($id)
	{  
	        $query="SELECT o.*,p.product_name FROM `tbl_order_products`o join tbl_products p on p.prod_id=o.product_id where o.order_id=?";		
			$param	=	array("i",$id);
			$records	=	$this->fetchAll($query,$param);		
			return $records;
	}	
	
	
	
	  //fetching courses
		
		function fetchCourses()
		{	
			$query="SELECT * FROM `tbl_mip`";		
			$records	=	$this->fetchAll($query);		
			return $records;
		}
		
		
		  //fetching valid courses
		
		function fetchValidCourses($depId)
		{	
			$query="SELECT * FROM `tbl_mip` where end_date>=now() and faculty_id=?";
			$param	=	array("i",$depId);		
			$records	=	$this->fetchAll($query,$param);		
			return $records;
		}
		
		
		
			  //fetching subjects for courses
		
		function fetchSubjectsForCourse($id)
		{	
			$query="SELECT * FROM `tbl_course_subjects` where course_id=?";		
			$param	=	array("i",$id);
			$records	=	$this->fetchAll($query,$param);		
			return $records;
		}
		
		
		
			  //fetching courses info
		
		function fetchCourseDetailedInfo($id)
		{	
			$query="SELECT * FROM `tbl_mip` where course_id=?";		
			$param	=	array("i",$id);
			$records	=	$this->fetchAll($query,$param);		
			return $records;
		}
		
		
		
		
			  //checking instructor leave information
		
		function chkInstructorOnLeaveDetails($instructor,$startdate,$enddate)
		{
				$startdate = date('Y-m-d', strtotime($startdate));						
				$enddate = date('Y-m-d', strtotime($enddate));
				
			
			$query="SELECT * FROM  tbl_leave_app_form WHERE ((start_date BETWEEN '$startdate' AND '$enddate') or (end_date BETWEEN '$startdate' AND '$enddate')) and instructor_id=?";		
			$param	=	array("i",$instructor);
			$records	=	$this->fetchAll($query,$param);		
			return $records;
		}
		
		
		
		
			// Add WTP
	
	function addWTP($values,$depId)    
    {

        $result    =    false;
        //$util    =    new Utilities();
        $validator    =    new Validator(array(
                                                $values["course"]=>"course/EMPTY",
                                                $values["startdate"]=>"startdate/EMPTY",
                                                $values["enddate"]=>"enddate/EMPTY",
                                                $values["classroom"]=>"ClassRoom/EMPTY"                                                                                    
                                                ));
        if($validator->validate())
        {
        
                $startdate = date('Y-m-d', strtotime($values["startdate"]));                        
                $enddate = date('Y-m-d', strtotime($values["enddate"]));
            
               $date = date('Y-m-d:H:i:s');
            
        /*    $query="SELECT * FROM `tbl_wtp` where start_date=? and end_date=?";        
            $param    =    array("ss",$startdate,$enddate);
            $records    =    $this->fetchAll($query,$param);    */    
            
        /*    $qry="SELECT * FROM `tbl_wtp` where start_date=? and end_date=? and classroomid=?";        
            $par    =    array("ssi",$startdate,$enddate,$values["classroom"]);
            $recds    =    $this->fetchAll($qry,$par);
            
            //if(count($records)==0){
            if(count($recds)==0) {*/
                   $array=array(                        
                            "course_id"=>$values["course"],
                            "start_date"=>$startdate,    
                            "end_date"=>$enddate,
                            "classroomid"=>$values["classroom"],
                            "date"=>$date                                            
                            );                            
                    $type    =    "issis";            
                    

                    $this->insert($array,"tbl_wtp",$type);
                    
                    $lid = $this->lastInsertId();

            //    $startdate = date('Y-m-d', strtotime($startdate));                        
            //    $enddate = date('Y-m-d', strtotime($enddate));
                
                
                
                
                
                 for($j=1;$j<=6;$j++){
                 
                 
                 if($j==1){                 
                 $day ="Monday";
                 $date = $startdate;
                 }else if($j==2)
                 {                 
                 $day ="Tuesday";    
                             
                 $date = date('Y-m-d', strtotime($startdate. " + 1 day"));
                
                 
                 }else if($j==3)
                 {                 
                 $day ="Wednesday";
                 $date = date('Y-m-d', strtotime($startdate. " + 2 days"));
                
                 
                 }else if($j==4)
                 {                 
                 $day ="Thursday";
                 
                 $date = date('Y-m-d', strtotime($startdate. " + 3 days"));
                
                 }else if($j==5)
                 {                 
                 $day ="Friday";
                 $date = date('Y-m-d', strtotime($startdate. " + 4 days"));
                 }else if($j==6)
                 {                 
                 $day ="Saturday";
                 $date = date('Y-m-d', strtotime($startdate. " + 5 days"));
                 }
                 
                 
                 
                    for($i=1;$i<=6;$i++){
                 
                     
                     if($values["subject".$i.$j]!="" && $values["courseInstructor".$i.$j]!=""){
                     
                     
                     $depIdForIn = $this->fetchInstructorByID($values["courseInstructor".$i.$j]);
                     
                     if($depIdForIn[0]["faculty"]==$depId){
                     
                     $approval =2;
                     
                     }else{
                     
                     $approval=1;
                     }
                     
                     
                     
                 
                      $array=array(                        
                            "wtp_id"=>$lid,
                            "date"=>$date,    
                            "day"=>$day,
                            "subject_id"=>$values["subject".$i.$j],
                            "instructor_id"=>$values["courseInstructor".$i.$j],
                            "status"=>$values["status".$i.$j],    
                            "session"=>$i,
                            "approval"=>$approval,
                            "assignBy"=>$depId                                    
                            );                            
                    $type    =    "issiiiiii";                
                    $this->insert($array,"tbl_wtp_per_date",$type);
                 
                  }
                 
                 
                 }            
                
                }
                    
                    $result    =    true;
                    
                    
                /*    }else{
                    
                $this->setError("Course Name / Strength has occupied this class")    ;    
                    
                    }*/
                    
                    
                /*    }else{
                    
                $this->setError("Already Entered For these Dates")    ;    
                    
                    }*/
                    
                    
                    
                    
                    
                
        }
        else
        {
            $this->setError($validator->getMessage())    ;
        }
        return $result;
        
    }
	
	
	
	
	
					// Fetch WTP
			function fetchWTP()
			{
			   $query="SELECT * FROM `tbl_wtp`";	
			$records	=	$this->fetchAll($query);	
			return $records;
			}
			
			
			
			function deletelistedWTP($id)
			{
			$tname	=	"tbl_wtp";
			$condition	=	"wtp_id=?";
			$param	=	array("i",$id);
			$this->delete($tname,$condition,$param);
			$tname	=	"tbl_wtp_per_date";
			$condition	=	"wtp_id=?";
			$param	=	array("i",$id);
			$this->delete($tname,$condition,$param);
			}
			
			
			
			// Fetch WTP
			function fetchWTPDetails($id)
			{
			$query="SELECT * FROM `tbl_wtp` where wtp_id=?";	
			$param	=	array("i",$id);
			$records	=	$this->fetchAll($query,$param);	
			return $records;
			}	
			
			
				
			// Fetch WTP per day
			function fetchWTPDetailsPerDay($id,$session,$date)
			{
            $query="SELECT * FROM `tbl_wtp_per_date` where wtp_id=? and session=? and day=?";	
			$param	=	array("iis",$id,$session,$date);
			$records	=	$this->fetchAll($query,$param);	
			return $records;
			}	
			
			
			
				
			// Add WTP
	
	function editWTP($values,$id,$depId)	
	{
	    
		$result	=	false;
		//$util	=	new Utilities();
		$validator	=	new Validator(array(
												$values["course"]=>"course/EMPTY",
												$values["startdate"]=>"startdate/EMPTY",
												$values["enddate"]=>"enddate/EMPTY",
												$values["classroom"]=>"ClassRoom/EMPTY"																			
												));
		if($validator->validate())
		{
		
		        $startdate = date('Y-m-d', strtotime($values["startdate"]));						
				$enddate = date('Y-m-d', strtotime($values["enddate"]));
			
			
			
			
			
			
		/*	$query="SELECT * FROM `tbl_wtp` where start_date=? and end_date=? and wtp_id!=? and classroomid=?";		
			$param	=	array("ssii",$startdate,$enddate,$id,$values["classroom"]);
			$records	=	$this->fetchAll($query,$param);		
			
			if(count($records)==0){*/
				   $array=array(						
							"course_id"=>$values["course"],
							"start_date"=>$startdate,	
							"end_date"=>$enddate,
							"classroomid"=>$values["classroom"]									
							);							
					$type	=	"issii";				
					//$this->insert($array,"tbl_wtp",$type);
					
					
					
					
			    $tname	=	"tbl_wtp";
				$cond	=	"wtp_id=?";
				$param	=	array($id);
				
				$this->update($array,$tname,$cond,$param,$type);
				$lid = $id;
					
					
					
					
			//	$startdate = date('Y-m-d', strtotime($startdate));						
			//	$enddate = date('Y-m-d', strtotime($enddate));
				
			$tname	=	"tbl_wtp_per_date";
			$condition	=	"wtp_id=?";
			$param	=	array("i",$id);
			$this->delete($tname,$condition,$param);
			
				
				
				
				 for($j=1;$j<=6;$j++){
				 
				 
				 if($j==1){				 
				 $day ="Monday";
				 $date = $startdate;
				 }else if($j==2)
				 {				 
				 $day ="Tuesday";	
				 			
				 $date = date('Y-m-d', strtotime($startdate. " + 1 day"));
				
				 
				 }else if($j==3)
				 {				 
				 $day ="Wednesday";
				 $date = date('Y-m-d', strtotime($startdate. " + 2 days"));
				
				 
				 }else if($j==4)
				 {				 
				 $day ="Thursday";
				 
				 $date = date('Y-m-d', strtotime($startdate. " + 3 days"));
				
				 }else if($j==5)
				 {				 
				 $day ="Friday";
				 $date = date('Y-m-d', strtotime($startdate. " + 4 days"));
				 }else if($j==6)
				 {				 
				 $day ="Saturday";
				 $date = date('Y-m-d', strtotime($startdate. " + 5 days"));
				 }
				 
				 
				 
				    for($i=1;$i<=6;$i++){
				 
					 
					 if($values["subject".$i.$j]!="" && $values["courseInstructor".$i.$j]!=""){
					 
					 $depIdForIn = $this->fetchInstructorByID($values["courseInstructor".$i.$j]);
                     
                     if($depIdForIn[0]["faculty"]==$depId){
                     
                     $approval =2;
                     
                     }else{
                     
                     $approval=1;
                     }
				 
				      $array=array(						
							"wtp_id"=>$lid,
							"date"=>$date,	
							"day"=>$day,
							"subject_id"=>$values["subject".$i.$j],
							"instructor_id"=>$values["courseInstructor".$i.$j],	
							"status"=>$values["status".$i.$j],	
							"session"=>$i,	
							"approval"=>$approval,
							"assignBy"=>$depId								
							);							
					$type	=	"issiiiiii";				
					$this->insert($array,"tbl_wtp_per_date",$type);
				 
				  }
				 
				 
				 }			
				
				}
					
					$result	=	true;
					
					/*}else{
					
				$this->setError("Course Name / Strength has occupied this class")	;	
					
					}*/
					
					
					
					
					
				
		}
		else
		{
			$this->setError($validator->getMessage())	;
		}
		return $result;
		
	}
	
	
	
	
	
	// Fetch Assigned Instructor For Subject
			function fetchAssignedInstructorForSubject($subid)
			{
            $query="SELECT i.*,c.* FROM `tbl_instructorcourse` c join tbl_instructor i on i.instructor_id=c.instructorId where c.subjectId=?";	
			$param	=	array("i",$subid);
			$records	=	$this->fetchAll($query,$param);	
			return $records;
			}	
			
	 // Fetch Assigned Instructor For Subject
			function fetchAssignedInstructorForSubjectWTP($subid,$depid)
			{
            $query="SELECT i.*,c.* FROM `tbl_instructorcourse` c join tbl_instructor i on i.instructor_id=c.instructorId where c.subjectId=? and i.faculty=?";	
			$param	=	array("ii",$subid,$depid);
			$records	=	$this->fetchAll($query,$param);	
			return $records;
			}			
			
		
		// Fetch Best Instructor 
			function fetchBestInstructor()
			{
            $query="SELECT count(wtp_per_date_id) as totSession,c.instructor_id,i.instructor_name,i.instructor_rank FROM `tbl_wtp_per_date` c join tbl_instructor i on i.instructor_id=c.instructor_id group by c.instructor_id order by totSession desc";	
			$records	=	$this->fetchAll($query);	
			return $records;
			}			
					
	
	// Fetch Best Instructor with dates
			function fetchBestInstructorWithDates($startdate,$enddate)
			{
			 $startdate = date('Y-m-d', strtotime($startdate));						
			$enddate = date('Y-m-d', strtotime($enddate));
			
            $query="SELECT count(wtp_per_date_id) as totSession,c.instructor_id,i.instructor_name,i.instructor_rank FROM `tbl_wtp_per_date` c join tbl_instructor i on i.instructor_id=c.instructor_id where date>='$startdate' and date<='$enddate' group by c.instructor_id order by totSession desc";	
			$records	=	$this->fetchAll($query);	
			return $records;
			}
   // Fetch Best Instructor with dates and faculty
			function fetchBestInstructorWithDatesAndFaculty($startdate,$enddate,$faculty)
			{
				$startdate = date('Y-m-d', strtotime($startdate));
				$enddate = date('Y-m-d', strtotime($enddate));
					
				$query="SELECT count(wtp_per_date_id) as totSession,c.instructor_id,i.instructor_name,i.instructor_rank FROM `tbl_wtp_per_date` c join tbl_instructor i on i.instructor_id=c.instructor_id where date>='$startdate' and date<='$enddate' and i.faculty=? group by c.instructor_id order by totSession desc";
				$param	=	array("i",$faculty);
				$records	=	$this->fetchAll($query,$param);
				return $records;
			}
			// Fetch Best Instructor with dates and faculty
			function fetchBestInstructorWithFaculty($faculty)
			{
									
				$query="SELECT count(wtp_per_date_id) as totSession,c.instructor_id,i.instructor_name,i.instructor_rank FROM `tbl_wtp_per_date` c join tbl_instructor i on i.instructor_id=c.instructor_id where i.faculty=? group by c.instructor_id order by totSession desc";
				$param	=	array("i",$faculty);
				$records	=	$this->fetchAll($query,$param);
				return $records;
			}
			
			
	// Fetch Instructor WTP 
			function fetchInstructorWTP($instructorid)
			{
            $query="SELECT c.*,s.subject_name,i.instructor_name,m.course_name FROM `tbl_wtp_per_date` c join tbl_instructor i on i.instructor_id=c.instructor_id join tbl_course_subjects s on s.sub_id=c.subject_id join tbl_mip m on m.course_id=s.course_id where c.instructor_id=? order by c.date,c.session";	
			$param	=	array("i",$instructorid);
			$records	=	$this->fetchAll($query,$param);	
			return $records;
			}	
			
			
			
				
		// Fetch Instructor WTP 
			function fetchInstructorWTPWithDates($instructorid,$startdate,$enddate)
			{
			
			 $startdate = date('Y-m-d', strtotime($startdate));						
			$enddate = date('Y-m-d', strtotime($enddate));
			
			
            $query="SELECT c.*,s.subject_name,i.instructor_name,m.course_name FROM `tbl_wtp_per_date` c join tbl_instructor i on i.instructor_id=c.instructor_id join tbl_course_subjects s on s.sub_id=c.subject_id join tbl_mip m on m.course_id=s.course_id where c.instructor_id=? and (date>='$startdate' and date<='$enddate') order by c.date,c.session";	
	   
	  
			$param	=	array("i",$instructorid);
			$records	=	$this->fetchAll($query,$param);	
			return $records;
			}	
			
			
			
			// Fetch class room for wtp
			function fetchClassRoomByWTPId($id)
			{			
            $query="SELECT w.*,c.class_room FROM `tbl_wtp` w join tbl_class_room c on c.class_room_id=w.classroomid where w.wtp_id=?";	
			$param	=	array("i",$id);
			$records	=	$this->fetchAll($query,$param);	
			return $records[0]["class_room"];
			}			
	
	
	
	
	
	
	
		// Fetch WTP per day
			function fetchWTPDetailsPerDayFORStatus($id,$date)
			{
            $query="SELECT * FROM `tbl_wtp_per_date` where wtp_id=? and day=? and status=2";	
			$param	=	array("is",$id,$date);
			$records	=	$this->fetchAll($query,$param);	
			return $records;
			}		
					

	function fetchallAssignCourse()
	{
			$query="SELECT * FROM `tbl_instructorcourse` ORDER BY id DESC";		
			$records	=	$this->fetchAll($query);		
			return $records;
	}
	
	//function fetchwtpbyCourseId($cid,$startdate,$enddate)
//	{
//			$query="SELECT * FROM `tbl_wtp` where course_id=? and start_date=? and end_date=?";	
//			$param	=	array("iss",$cid,$startdate,$enddate);
//			$records	=	$this->fetchAll($query,$param);	
//			return $records;
//	}
	
	function fetchwtpbyCourseId($cid,$startdate,$enddate)
	{
	
		    $query="SELECT c.*,s.subject_name,i.instructor_name,w.wtp_id FROM `tbl_wtp_per_date` c join tbl_wtp w on w.wtp_id=c.wtp_id join tbl_course_subjects s on s.sub_id=c.subject_id join tbl_instructor i on i.instructor_id=c.instructor_id where w.course_id=? and (w.start_date BETWEEN '$startdate' AND '$enddate') and (w.end_date BETWEEN '$startdate' AND '$enddate') order by c.date,c.session";	
			$param	=	array("i",$cid);
			$records	=	$this->fetchAll($query,$param);	
			return $records;
	}
	
	
	
	function fetchwtpforRequest($depId)
	{
		$query="SELECT i.instructor_id,i.instructor_name,w.* FROM `tbl_instructor` i JOIN `tbl_wtp_per_date` w ON i.instructor_id =w.instructor_id WHERE i.faculty =? ORDER BY approval ASC";
		$param	=	array("i",$depId);
		$records	=	$this->fetchAll($query,$param);	
		return $records;
	}
	
	function updateRequestStat($wtp_per_date_id,$sess)
	{
			$qury="SELECT * FROM `tbl_wtp_per_date` where wtp_per_date_id=?";	
			$parm	=	array("i",$wtp_per_date_id);
			$record	=	$this->fetchAll($qury,$parm);	
			
			$appliedDate = strtotime($record[0]["date"]);
			if(date('l',strtotime($record[0]["date"]))=='Monday') 
			{ 
			$first_day_of_week = $record[0]["date"];
			$last_day_of_week = date('Y-m-d', strtotime('Next Sunday', $appliedDate));
			}
			else
			{
			$first_day_of_week = date('Y-m-d', strtotime('Last Monday', $appliedDate));
			$last_day_of_week = date('Y-m-d', strtotime('Next Sunday', $appliedDate));
			}
					
								
				$val	=	array(
							"approval"=>2
							);
								  
				$tname	=	"tbl_wtp_per_date";
				$cond	=	"session=? AND (date BETWEEN '$first_day_of_week' AND '$last_day_of_week')";
				$param	=	array($sess);
				$ty		=	"ii";		
				$this->update($val,$tname,$cond,$param,$ty);
				
				$result=true;
	}
	
	function updateRequestStatDecline($wtp_per_date_id,$sess)
	{
			$qury="SELECT * FROM `tbl_wtp_per_date` where wtp_per_date_id=?";	
			$parm	=	array("i",$wtp_per_date_id);
			$record	=	$this->fetchAll($qury,$parm);	
			
			$appliedDate = strtotime($record[0]["date"]);
			if(date('l',strtotime($record[0]["date"]))=='Monday') 
			{ 
			$first_day_of_week = $record[0]["date"];
			$last_day_of_week = date('Y-m-d', strtotime('Next Sunday', $appliedDate));
			}
			else
			{
			$first_day_of_week = date('Y-m-d', strtotime('Last Monday', $appliedDate));
			$last_day_of_week = date('Y-m-d', strtotime('Next Sunday', $appliedDate));
			}		
				
								
				$val	=	array(
							"approval"=>3
							);
								  
				$tname	=	"tbl_wtp_per_date";
				$cond	=	"session=? AND (date BETWEEN '$first_day_of_week' AND '$last_day_of_week')";
				$param	=	array($sess);
				$ty		=	"ii";	
				
					
				$this->update($val,$tname,$cond,$param,$ty);
				
				$result=true;
	}
	
	
	
		function chkAssignedForAnotherCourseOnSession($instructor,$startdate,$enddate,$rw,$colum)
	{
	
		 $startdate = date('Y-m-d', strtotime($startdate));						
		 $enddate = date('Y-m-d', strtotime($enddate));
		 
		  $date = date('Y-m-d', strtotime($startdate. " + " .$colum." days")); 
			
	
	
			$query="SELECT wtpdate.*, mip.course_name,mip.strength FROM `tbl_wtp_per_date` as wtpdate
			left join tbl_wtp as wtp on wtp.wtp_id = wtpdate.wtp_id
			left join tbl_mip as mip on wtp.course_id = mip.course_id 
			where wtpdate.instructor_id=? and wtpdate.date='$date' and wtpdate.session=?";	
			$param	=	array("ii",$instructor,$rw);
			$records	=	$this->fetchAll($query,$param);	
			return $records;
	}
	
	
	// 11-11-2014
	
	function fetchWTPDetailsEdit($id)
	{
			$query="SELECT * FROM `tbl_wtp_per_date` where wtp_per_date_id=?";	
			$param	=	array("i",$id);
			$records	=	$this->fetchAll($query,$param);	
			return $records;
	}
	
	function updatewtpDay($values,$sess,$per_id)
	{
	
			$qury="SELECT * FROM `tbl_wtp_per_date` where wtp_per_date_id=?";	
			$parm	=	array("i",$per_id);
			$record	=	$this->fetchAll($qury,$parm);	
		 
			$appliedDate = strtotime($record[0]["date"]);
			if(date('l',strtotime($record[0]["date"]))=='Monday') 
			{ 
			$first_day_of_week = $record[0]["date"];
			$last_day_of_week = date('Y-m-d', strtotime('Next Sunday', $appliedDate));
			}
			else
			{
			$first_day_of_week = date('Y-m-d', strtotime('Last Monday', $appliedDate));
			$last_day_of_week = date('Y-m-d', strtotime('Next Sunday', $appliedDate));
			}		 		 
				 
				      $array=array(	
							"subject_id"=>$values["subject"],
							"instructor_id"=>$values["courseInstructor"],
							"approval"=>4								
							);							
					$type	=	"iiii";	
					$tname	=	"tbl_wtp_per_date";		
					$cond	=	"session=? AND (date BETWEEN '$first_day_of_week' AND '$last_day_of_week')";	
					$param	=	array($sess);
					$this->update($array,$tname,$cond,$param,$type);
					
					$result	=	true;
	}
	
	//13-11-2014
	
	function fetchClassroombyStrength($strength)
	{
			$query="SELECT * FROM `tbl_class_room` where strength >=?";	
			$param	=	array("i",$strength);
			$records	=	$this->fetchAll($query,$param);	
			return $records;
	}
			
	
	function fetchwtpbyCourse($wtpId)
	{
			$query="SELECT * FROM `tbl_wtp` where wtp_id =?";	
			$param	=	array("i",$wtpId);
			$records	=	$this->fetchAll($query,$param);	
			return $records;
	}
	
	function checkClassAlloc($room,$sdate,$edate)
	{
			$startdate = date('Y-m-d', strtotime($sdate));						
		 	$enddate = date('Y-m-d', strtotime($edate));
			
			$query="SELECT * FROM `tbl_wtp` where classroomid =? and start_date=? and end_date=?";	
			$param	=	array("iss",$room,$startdate,$enddate);
			$records	=	$this->fetchAll($query,$param);	
			return $records;
	}
	
	function editcheckClassAlloc($room,$sdate,$edate,$id)
	{
			$startdate = date('Y-m-d', strtotime($sdate));						
		 	$enddate = date('Y-m-d', strtotime($edate));
			
			$query="SELECT * FROM `tbl_wtp` where classroomid =? and start_date=? and end_date=? and wtp_id!=?";	
			$param	=	array("issi",$room,$startdate,$enddate,$id);
			$records	=	$this->fetchAll($query,$param);	
			return $records;
	}
	
		// 13-11-2014
	
	function autoApprovalList()
	{
			$query="SELECT d.*,w.date FROM `tbl_wtp_per_date` d join tbl_wtp w on w.wtp_id=d.wtp_id where approval=1 group by wtp_id";				
			$records	=	$this->fetchAll($query);	
			return $records;
	}
	
	
	function autoApproval($id)
	{
	             $array=array(	
							"approval"=>2
							);							
					$type	=	"ii";	
					$tname	=	"tbl_wtp_per_date";		
					$cond	=	"wtp_id=?";	
					$param	=	array($id);
					$this->update($array,$tname,$cond,$param,$type);
					$result	=	true;

	}
	
	
	
			
			// Fetch WTP per day
			function fetchWTPDetailsPerDayTraineeWtp($cid,$startDate,$endDate,$session,$date)
			{

			
			$query="SELECT c.*,s.subject_name,i.instructor_name,w.wtp_id FROM `tbl_wtp_per_date` c join tbl_wtp w on w.wtp_id=c.wtp_id join tbl_course_subjects s on s.sub_id=c.subject_id join tbl_instructor i on i.instructor_id=c.instructor_id where w.course_id=? and (w.start_date BETWEEN '$startDate' AND '$endDate') and (w.end_date BETWEEN '$startDate' AND '$endDate') and c.session=? and c.day=? order by c.date,c.session";	
		
			$param	=	array("iis",$cid,$session,$date);
			$records	=	$this->fetchAll($query,$param);	
			return $records;
			
			
			
			}	
			
			
			//fetching Course details  
               
                function fetchCourseDetailsForSubStatusById($name)
               {                        
                       $query="SELECT * FROM `tbl_mip` where course_id=?";                        
                       $param        =        array("i",$name);
                       $records        =        $this->fetchAll($query,$param);        
                       return $records;
               }
			
			
			   function fetchwtpCouWithoutInstrId($couid)
    {
        $query ="SELECT * from `tbl_instructorcourse` where courseId=?";
        $param    =    array("i",$couid);
        $records    =    $this->fetchAll($query,$param);        
        return $records;
    }
			
}
?>
