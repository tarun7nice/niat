<?php 
class Auth extends Database_MySql
{
	public $errorMessage;
	public $userId;
	private $utilObject;

	function __construct()
	{
		parent::__construct();
		$this->connect();
		$this->userId	=	null;
		$this->utilObject	=	new Utilities();
		
	}
	

		
	/*
	 * to set user_id for each page
	 * author: Rekha
	 */
	function setUserId($id)
	{
		 $this->userId	=	$id;
	}
	
	
	
	function updateStaffAccount($values,$userId)
	{
		$result	=	false;
		$validator	=	new Validator(array(																			
												$values["txtUsername"]=>"/EMPTY"));
		if($validator->validate())
		{
			
				$val	=	array("staff_name"=>$values["txtUsername"]);
				$tname	=	"tbl_staff";
				$cond	=	"staff_id=?";
				$param	=	array($userId);
				$ty		=	"si";
				$this->update($val,$tname,$cond,$param,$ty);
					
		}
		else
		{
			$this->setError($validator->getMessage())	;
		}
		return $result;
	
	}
	
	function updatePassword($values,$userId)
	{
		$result	=	false;
		$validator	=	new Validator(array(
												$values["txtCurrentPassword"]=>"/EMPTY",												
												$values["txtNewPassword"]=>"/EMPTY"));
		if($validator->validate())
		{
			$oldpassword	=	md5($values["txtCurrentPassword"]);		
			$newpassword	=	md5($values["txtNewPassword"]);					
			
				$val	=	array("password"=>$newpassword);
				$tname	=	"tbl_staff";
				$cond	=	"staff_id=? and password=?";
				$param	=	array($userId,$oldpassword);
				$ty		=	"sis";
				if(!$this->update($val,$tname,$cond,$param,$ty)){
					$this->setError("Old Password doesn't match");
				}
		}
		else
		{
			$this->setError($validator->getMessage())	;
		}
		return $result;
	
	}
	

			
		function getUserInfo($id)
			{
				$qry	=	"SELECT * FROM `tbl_staff` where staff_id=?";		
				$param	=	array("i",$id);
			    $records	=	$this->fetchAll($qry,$param);			
				return $records;
			}
		
			
  // check admin login 	
	
	function checkAdminLogin($values)
	{
	
		$result		=	false;
		$validator	=	new Validator(array($values["txtusername"]=>"/EMPTY",$values["txtpassword"]=>"/EMPTY"));
		if($validator->validate())
		{
			$records		=	"";
			$username		=	$values["txtusername"];
			$password		=	md5($values["txtpassword"]);
			
			
	    	$querry			=	"SELECT staff_id FROM `tbl_staff`  WHERE `username`=?";//check user name exist.
			
			
			$param			=	array("s",$username);
			$records		=	$this->fetchAll($querry,$param);
									
			if(!empty($records))
			{
				$det_rec	=	array();
			    $querry			=	"SELECT * FROM `tbl_staff`  WHERE `staff_id`=? and password=? and role_id=1";//check username and password
				$param			=	array("is",$records[0]["staff_id"],$password);
				$det_rec		=	$this->fetchAll($querry,$param);
							
				if(!empty($det_rec))
				{
					$result		=	true;
					$this->userId	=	$det_rec[0]["staff_id"];
					$this->userName	=	$det_rec[0]["staff_name"];
					$this->userRole	=	$det_rec[0]["role_id"];					
				}					
			}
			if(!$result)
			{
				$this->setError("Invalid username or password ");	
			}
		}
		else
		{
			$this->setError("Enter mandatory fields !");
		}		
		return $result;
	}
	
	
	
	 // check faculty login 	
	
	function checkFacultyLogin($values)
	{
	
		$result		=	false;
		$validator	=	new Validator(array($values["txtusername"]=>"/EMPTY",$values["txtpassword"]=>"/EMPTY"));
		if($validator->validate())
		{
			$records		=	"";
			$username		=	$values["txtusername"];
			$password		=	md5($values["txtpassword"]);
			
			
	    	$querry			=	"SELECT staff_id FROM `tbl_staff`  WHERE `username`=?";//check user name exist.
			
			
			$param			=	array("s",$username);
			$records		=	$this->fetchAll($querry,$param);
									
			if(!empty($records))
			{
				$det_rec	=	array();
			    $querry			=	"SELECT * FROM `tbl_staff`  WHERE `staff_id`=? and password=? and role_id=2";//check username and password
				$param			=	array("is",$records[0]["staff_id"],$password);
				$det_rec		=	$this->fetchAll($querry,$param);
							
				if(!empty($det_rec))
				{
					$result		=	true;
					$this->userId	=	$det_rec[0]["staff_id"];
					$this->userName	=	$det_rec[0]["staff_name"];
					$this->userRole	=	$det_rec[0]["role_id"];					
				}					
			}
			if(!$result)
			{
				$this->setError("Invalid username or password ");	
			}
		}
		else
		{
			$this->setError("Enter mandatory fields !");
		}		
		return $result;
	}
		
}?>